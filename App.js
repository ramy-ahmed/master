
import React, { Component } from 'react';
import { createRootNavigator } from './app/navigation/Router'
import { createTabletRootNavigator } from './app/navigation/RouterTablet';
import { ActivityIndicator, StyleSheet } from 'react-native';
import AsyncStorage from '@react-native-community/async-storage';
import DeviceInfo from 'react-native-device-info'
//store
import { Provider } from 'react-redux';
import createStore from './app/redux';
import Colors from './app/theme/Colors';




  componentWillMount() {
    //await this._bootstrapAsync();
  }
  async componentDidMount() {
    await this._bootstrapAsync();

  }

  componentDidUpdate() {

  }

  _bootstrapAsync = async () => {
    var _isLogin = await AsyncStorage.getItem('isLogin');
    if (_isLogin === null) {
      _isLogin = "false"
    }
    await this.setState({ isLogin: _isLogin, isLoading: false })
  }


  _renderRouter() {
    if (this.state.isLoading) {
      return <ActivityIndicator style={style.activityIndicator} size="large" color={Colors.activityIndicator} />
    } else {
      if (DeviceInfo.isTablet()) {
        const TabletRouter = createTabletRootNavigator(this.state.isLogin);
        return (<TabletRouter />)
      } else {
        const PhoneRouter = createRootNavigator(this.state.isLogin);
        return (<PhoneRouter />)
      }
    }
  }

  render() {
    return (
      <Provider store={store}>
        {this._renderRouter()}
      </Provider>
    )
  }
}

const style = StyleSheet.create({
  activityIndicator: {
    flex: 1,
    alignItems: 'center'
  }
})
