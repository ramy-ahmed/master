import Constants from './Constants';
import fetch from '../helpers/FetchWithTimeout';


const create = () => {
    const getRequestToken = (userName, password, organization) => fetch(organization.almsPlusAuthUrl + Constants.AuthUrl, {
        method: 'POST',
        headers: {
            Accept: 'application/json',
            'Content-Type': 'application/x-www-form-urlencoded',
        },
        body: 'client_id=api&'
            + 'grant_type=password&'
            + 'username=' + userName + '&' + 'password=' + password + '&'
            + 'address=' + organization.apiAddress + '&'
            + 'port=' + Constants.ApiPort,
        // body: JSON.stringify({
        //     "client_id": 'api',
        //     "grant_type": 'password',
        //     "username": userName,
        //     "password": password,
        //     "address": organization.apiAddress,
        //     "port": Constants.ApiPort
        // })
    }).then(response => response.json()).then(json => {
        console.log("api resp:", json);
        return json
    })

    const refreshToken = (body) => fetch(body.almsPlusAuthUrl + Constants.AuthUrl, {
        method: 'POST',
        headers: {
            Accept: 'application/json',
            'Content-Type': 'application/x-www-form-urlencoded',
        },
        body: 'client_id=api&'
            + 'grant_type=refresh_token&'
            + 'username=' + body.userName + '&' + 'password=' + body.password + '&'
            + 'address=' + body.apiAddress + '&'
            + 'port=' + Constants.ApiPort + '&'
            + 'refresh_token=' + body.refreshToken,
    }).then(function (response) {
        if (response.ok) {
            console.log('refreshToken response.ok');
            return response.json()
        } else {
            throw { 'error': true, 'code': response.status, 'text': response.statusText }
        }
    }).then(function (json) {
        console.log('refreshToken result: ', json);
        return json
    }).catch(error => {
        console.log("refreshToken error: ", error);
        if (error.hasOwnProperty("error")) return error
        else return { 'error': true, 'code': 600, 'text': 'connection error' }
    });

    const getUserIdentity = (token, organization) => fetch(organization.almsPlusApiUrl + Constants.UserIdentity, {
        method: 'GET',
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json',
            'Authorization': 'Bearer ' + token,
        }
    }).then(function (response) {
        if (response.ok) {
            return response.json();
        } else {
            throw { 'error': response.status, 'error_text': response.statusText }
        }
    }).then(function (json) {
        console.log("getUserIdentity result", json)
        return json
    }).catch(error => {
        return error
    });

    return {
        getRequestToken,
        getUserIdentity,
        refreshToken
    }
}

export default {
    create
}