import Colors from './Colors';
const AppTheme = {
    colors: {
        primary: Colors.primary
    }
}
export default AppTheme