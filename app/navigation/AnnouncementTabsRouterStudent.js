
import React from 'react';
import { View } from 'react-native';
import { createMaterialTopTabNavigator, createAppContainer, createStackNavigator, createSwitchNavigator, createBottomTabNavigator } from 'react-navigation';
import AllAnnouncement from '../views/tabs/announcement/AllAnnouncement';
import AnnouncementDetail from '../views/tabs/announcement/AnnouncementDetail';
import DeletedAnnouncement from '../views/tabs/announcement/DeletedAnnouncement';
import DraftAnnouncement from '../views/tabs/announcement/DraftAnnouncement';
import InboxAnnouncement from '../views/tabs/announcement/InboxAnnouncement';
import TrashAnnouncement from '../views/tabs/announcement/TrashAnnouncement';
import SentAnnouncement from '../views/tabs/announcement/SentAnnouncement';
import Colors from '../theme/Colors';
import { strings } from '../locales/i18n';



//Tabs Routing for instructor user role
//TODO: Hangi Duyuru tiplerin getirileceği sorulacak.


const TabAnnouncementScreen = createMaterialTopTabNavigator(
    {
        All: {
            screen: AllAnnouncement,
            navigationOptions: ({ navigation, screenProps }) => {
                return {
                    tabBarLabel: screenProps.languageResource.r_announcement_all || strings('r_announcement_all')
                }
            }
        },
        Inbox: {
            screen: InboxAnnouncement,
            navigationOptions: ({ navigation, screenProps }) => {
                return {
                    tabBarLabel: screenProps.languageResource.r_announcement_inbox || strings('r_announcement_inbox')
                }
            }
        },
        Deleted: { screen: DeletedAnnouncement,
            navigationOptions: ({ navigation, screenProps }) => {
                return {
                    tabBarLabel: screenProps.languageResource.r_announcement_deleted || strings('r_announcement_deleted')
                }
            }
        },
    },
    {
        tabBarPosition: 'top',
        swipeEnabled: true,
        animationEnabled: true,
        lazy: true,
        backBehavior: 'none',
        tabBarOptions: {

            activeTintColor: Colors.announcement_tab_active_text_color,
            inactiveTintColor: Colors.announcement_tab_inactive_text_color,
            scrollEnabled: false,
            style: {
                backgroundColor: Colors.announcement_tab_background,
            },
            labelStyle: {
                textAlign: 'center',
            },
            indicatorStyle: {
                borderBottomColor: Colors.primary,
                borderBottomWidth: 2,
            },
        },
    }
);

const AnnouncementTabRouterStudentContainer = createAppContainer(createSwitchNavigator({
    announcementTabs: {
        screen: TabAnnouncementScreen
    },
}));
export default AnnouncementTabRouterStudentContainer

// class AnnouncementTabRouterStudent extends React.Component {
//     constructor(props) {
//         super(props)
//     }
//     static router = AnnouncementTabRouterStudentContainer.router
//     render() {
//         return <AnnouncementTabRouterStudentContainer navigation={this.props.navigation} />
//     }
// }

// export default AnnouncementTabRouterStudent;
