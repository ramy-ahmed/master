import React from 'react';
import { createAppContainer, createBottomTabNavigator, createStackNavigator } from 'react-navigation';
import Ionicons from 'react-native-vector-icons/Ionicons';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
import SimpleLineIcons from 'react-native-vector-icons/SimpleLineIcons';
import Colors from '../theme/Colors';

//tabs
import CourseActivities from '../views/tabs/courses/tab/CourseActivities';
import CourseNotebook from '../views/tabs/courses/tab/CourseNotebook';
import CourseSyllabus from '../views/tabs/courses/tab/CourseSyllabus';
import CourseAnnouncement from '../views/tabs/courses/tab/CourseAnnouncement';
import CourseForum from '../views/tabs/courses/tab/CourseForum';
import CourseFiles from '../views/tabs/courses/tab/CourseFiles';

import { strings } from '../locales/i18n';

//TODO: Hangi tab ların gösterileciğle ilgili çalışma yapılacak
const CourseActivitiesStack = createStackNavigator({
    CourseActivities: {
        screen: CourseActivities,
        navigationOptions: {
            header: null
        }
    },
}, {
    headerMode: 'float', headerLayoutPreset: 'center', navigationOptions: ({ navigation, screenProps }) => {
        return {
            tabBarLabel: screenProps.languageResource.r_course_detail_tab_activities || strings('r_course_detail_tab_activities'),
            tabBarIcon: ({ focused, horizontal, tintColor }) => {
                let IconComponent = Ionicons;
                let iconName = 'logo-buffer';
                return <IconComponent name={iconName} size={25} color={tintColor} />;
            },
        }
    }
})

const CourseSyllabusStack = createStackNavigator({
    CourseSyllabus: {
        screen: CourseSyllabus,
        navigationOptions: {
            header: null
        },
    },
}, {
    headerMode: 'float', headerLayoutPreset: 'center', navigationOptions: {
        tabBarLabel: null,
        tabBarIcon: ({ focused, horizontal, tintColor }) => {
            let IconComponent = Ionicons;
            let iconName = 'md-clipboard';
            return <IconComponent name={iconName} size={25} color={tintColor} />;
        },
    }
})

const CourseNoteBookStack = createStackNavigator({
    CourseNoteBook: {
        screen: CourseNotebook,
        navigationOptions: {
            header: null
        },
    }
}, {
    headerMode: 'float', headerLayoutPreset: 'center', navigationOptions: {
        tabBarLabel: null,
        tabBarIcon: ({ focused, horizontal, tintColor }) => {
            let IconComponent = SimpleLineIcons;
            let iconName = 'notebook';
            return <IconComponent name={iconName} size={25} color={tintColor} />;
        },
    }
})

const CourseAnnouncementStack = createStackNavigator({
    CourseAnnouncement: {
        screen: CourseAnnouncement,
        navigationOptions: {
            header: null
        },
    }
}, {
    headerMode: 'float', headerLayoutPreset: 'center', navigationOptions: ({ navigation, screenProps }) => {
        return {
            tabBarLabel: screenProps.languageResource.r_course_detail_tab_announcements || strings('r_course_detail_tab_announcements'),
            tabBarIcon: ({ focused, horizontal, tintColor }) => {
                let IconComponent = Ionicons;
                let iconName = 'ios-notifications-outline';
                return <IconComponent name={iconName} size={25} color={tintColor} />;
            },
        }

    }
})

const CourseForumStack = createStackNavigator({
    CourseForum: {
        screen: CourseForum,
        navigationOptions: {
            header: null
        },
    },
}, {
    headerMode: 'float', headerLayoutPreset: 'center', navigationOptions: ({ navigation, screenProps }) => {
        return {
            tabBarLabel: screenProps.languageResource.r_course_detail_tab_forum || strings('r_course_detail_tab_forum'),
            tabBarIcon: ({ focused, horizontal, tintColor }) => {
                let IconComponent = MaterialCommunityIcons;
                let iconName = 'message-text-outline';
                return <IconComponent name={iconName} size={25} color={tintColor} />;
            },
        }

    }
})

const CourseFilesStack = createStackNavigator({
    CourseFiles: {
        screen: CourseFiles,
        navigationOptions: {
            header: null
        },
    }
}, {
    headerMode: 'float', headerLayoutPreset: 'center', navigationOptions: ({ navigation, screenProps }) => {
        return {
            tabBarLabel: screenProps.languageResource.r_course_detail_tab_files || strings('r_course_detail_tab_files'),
            tabBarIcon: ({ focused, horizontal, tintColor }) => {
                let IconComponent = Ionicons;
                let iconName = 'ios-attach';
                return <IconComponent name={iconName} size={25} color={tintColor} />;
            },
        }

    }
})

const CourseDetailTab = createBottomTabNavigator(
    {
        CourseActivitiesStack,
        //CourseSyllabusStack,
        //CourseNoteBookStack,
        CourseAnnouncementStack,
        CourseForumStack,
        CourseFilesStack
    }, {
    tabBarOptions: {
        activeTintColor: Colors.course_detail_tab_active_text_color,
        inactiveTintColor: Colors.course_detail_tab_inactive_text_color,
        showLabel: true,
        showIcon: true,
        scrollEnabled: false,
    },
    backBehavior: 'none',
    tabBarPosition: 'bottom'
}
);

const CourseDetailTabMenu = createAppContainer(createStackNavigator({
    CourseDetail: {
        screen: CourseDetailTab,
        navigationOptions: {
            header: null
        }
    },
}));
export default CourseDetailTabMenu;