import React from 'react';
import { createMaterialTopTabNavigator, createAppContainer, createSwitchNavigator } from 'react-navigation';
import AllAnnouncement from '../views/tabs/announcement/AllAnnouncement';
import DeletedAnnouncement from '../views/tabs/announcement/DeletedAnnouncement';
import DraftAnnouncement from '../views/tabs/announcement/DraftAnnouncement';
import InboxAnnouncement from '../views/tabs/announcement/InboxAnnouncement';
import TrashAnnouncement from '../views/tabs/announcement/TrashAnnouncement';
import SentAnnouncement from '../views/tabs/announcement/SentAnnouncement';

import Ionicons from 'react-native-vector-icons/Ionicons';
import Colors from '../theme/Colors';

//Tabs Routing for instructor user role
//TODO: Hangi Duyuru tiplerin getirileceği sorulacak.

const TabAnnouncementScreen = createMaterialTopTabNavigator(
    {
        Inbox: {
            screen: InboxAnnouncement,
            // navigationOptions: {
            //     tabBarIcon: ({ focused, horizontal, tintColor }) => {
            //         let IconComponent = Ionicons;
            //         let iconName = 'logo-buffer';
            //         return <IconComponent name={iconName} size={25} color={tintColor} />;
            //     },
            // }
        },
        Sent: {
            screen: SentAnnouncement,
            // navigationOptions: {
            //     tabBarIcon: ({ focused, horizontal, tintColor }) => {
            //         let IconComponent = Ionicons;
            //         let iconName = 'logo-buffer';
            //         return <IconComponent name={iconName} size={25} color={tintColor} />;
            //     },
            // }
        },
        Deleted: {
            screen: DeletedAnnouncement,
            // navigationOptions: {
            //     tabBarIcon: ({ focused, horizontal, tintColor }) => {
            //         let IconComponent = Ionicons;
            //         let iconName = 'logo-buffer';
            //         return <IconComponent name={iconName} size={25} color={tintColor} />;
            //     },
            // }
        },
        // Draft: {
        //     screen: DraftAnnouncement,
        //     navigationOptions: {
        //         tabBarIcon: ({ focused, horizontal, tintColor }) => {
        //             let IconComponent = Ionicons;
        //             let iconName = 'logo-buffer';
        //             return <IconComponent name={iconName} size={25} color={tintColor} />;
        //         },
        //     }
        // },
        // Sent: {
        //     screen: SentAnnouncement,
        //     navigationOptions: {
        //         tabBarIcon: ({ focused, horizontal, tintColor }) => {
        //             let IconComponent = Ionicons;
        //             let iconName = 'logo-buffer';
        //             return <IconComponent name={iconName} size={25} color={tintColor} />;
        //         },
        //     }
        // },
        // Trash:{screen:TrashAnnouncement}

    },
    {
        tabBarPosition: 'top',
        swipeEnabled: true,
        animationEnabled: true,
        lazy: true,
        backBehavior: 'none',
        tabBarOptions: {

            activeTintColor: '#000000',
            inactiveTintColor: '#808080',
            scrollEnabled: false,
            style: {
                backgroundColor: '#FFF',
            },
            labelStyle: {
                textAlign: 'center',
            },
            indicatorStyle: {
                borderBottomColor: Colors.primary,
                borderBottomWidth: 2,
            },
        },
    }
);
const AnnouncementTabRouterInstructorContainer = createAppContainer(createSwitchNavigator({
    announcementTabs: {
        screen: TabAnnouncementScreen
    },
}));
export default AnnouncementTabRouterInstructorContainer

// class AnnouncementTabsRouterInstructor extends React.Component {
//     constructor(props) {
//         super(props)
//     }
//     static router = AnnouncementTabRouterInstructorContainer.router;
//     render() {
//         return <AnnouncementTabRouterInstructorContainer navigation={this.props.navigation}/>
//     }
// }
// export default AnnouncementTabsRouterInstructor;