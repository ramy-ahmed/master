import { createReducer, createActions } from 'reduxsauce';
import Immutable from 'seamless-immutable';



/* ------------- Types and Action Creators ------------- */
const { Types, Creators } = createActions({
    setCoursesList: [],

    getCoursesRequest: ['body'],
    getCoursesSuccess: ['data'],
    getCoursesFailure: ['error'],

    getEnrollmentProgressRequest: ['body'],
    getEnrollmentProgressSuccess: ['data'],
    getEnrollmentProgressFailure: ['error'],

})

export const CoursesTypes = Types
export default Creators


/* ------------- Initial State ------------- */
export const INITIAL_STATE = Immutable({
    enrolledCoursesData: [],
    fetching: true,
    errorMessage: '',
    error: false,


    enrollmentProgressData: [],
    enrollmentProgressFetching: false,
    enrollmentProgressErrorMessage: '',
    enrollmentProgressError: false,
})


/* ------------- Reducers ------------- */
export const getCoursesRequest = (state, action) => {
    console.log("getCoursesRequest : ",action.body)
    return state.merge({ fetching: true, error: false, errorMessage: '' })
}

export const getCoursesSuccess = (state, action) => {
    return state.merge({ fetching: false, error: false, errorMessage: '', enrolledCoursesData: [...state.enrolledCoursesData, ...action.data] })
}

export const getCoursesFailure = (state, action) => {
    return state.merge({ fetching: false, error: true, errorMessage: action.error })
}


/* ------------------ */
export const getEnrollmentProgressRequest = (state, action) => {
    return state.merge({ enrollmentProgressFetching: true, enrollmentProgressErrorMessage: '', enrollmentProgressError: false })
}
export const getEnrollmentProgressSuccess = (state, action) => {
    return state.merge({ enrollmentProgressData: action.data, enrollmentProgressFetching: false, enrollmentProgressErrorMessage: '', enrollmentProgressError: false })
}

export const getEnrollmentProgressFailure = (state, action) => {
    return state.merge({ enrollmentProgressFetching: false, enrollmentProgressErrorMessage: action.error, enrollmentProgressError: true })
}


export const setCoursesList = (state, action) => {
    return state.merge({ enrolledCoursesData: [], fetching: true, errorMessage: '', error: false, })
}
/* ------------- Connection Reducers To Types ------------- */
export const reducer = createReducer(INITIAL_STATE, {
    [Types.GET_COURSES_REQUEST]: getCoursesRequest,
    [Types.GET_COURSES_SUCCESS]: getCoursesSuccess,
    [Types.GET_COURSES_FAILURE]: getCoursesFailure,

    [Types.GET_ENROLLMENT_PROGRESS_REQUEST]: getEnrollmentProgressRequest,
    [Types.GET_ENROLLMENT_PROGRESS_SUCCESS]: getEnrollmentProgressSuccess,
    [Types.GET_ENROLLMENT_PROGRESS_FAILURE]: getEnrollmentProgressFailure,


    [Types.SET_COURSES_LIST]: setCoursesList,
})