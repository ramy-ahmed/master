import { combineReducers } from 'redux';
import configureStore from './CreateStore';
import rootSaga from '../sagas/';

export default () => {
    /* ------------- Assemble The Reducers ------------- */

    const appReducer = combineReducers({
        organizations: require('./OrganizationRedux').reducer,
        login: require('./LoginRedux').reducer,
        announcement: require('./AnnouncementRedux').reducer,
        courses: require('./CoursesRedux').reducer,
        courseDetail: require('./CourseDetailRedux').reducer,
        schedule: require('./ScheduleRedux').reducer,
        messages: require('./MessagesRedux').reducer,
        menu: require('./MenuRedux').reducer,
        main: require('./MainRedux').reducer,
        addActivity: require('./AddActivityRedux').reducer,
        settings: require('./SettingsRedux').reducer,
        activityInteract: require('./ActivityInteractRedux').reducer,
    })


    const rootReducer = (state, action) => { //Clear state data when user logout 
        if (action.type === 'CLEAR_REDUX')
            state = undefined
        return appReducer(state, action)
    }

    return configureStore(rootReducer, rootSaga)
}