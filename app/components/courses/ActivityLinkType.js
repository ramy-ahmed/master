//react libraries
import React from 'react';
import { View, TouchableOpacity, ScrollView } from 'react-native';
import { Card, Image } from 'react-native-elements';


//3.rd libraries
import Ionicons from 'react-native-vector-icons/Ionicons';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
import Moment from 'moment';
import HTML from 'react-native-render-html';


//Component & styles
import TextView from '../TextView';
import Colors from '../../theme/Colors';
import Constants from '../../services/Constants';
import ActivityInteractActions from '../../redux/ActivityInteractRedux';
import CoursesActions from '../../redux/CoursesRedux';

//settings
import { connect } from 'react-redux';
import { strings } from '../../locales/i18n';
import { template } from '../../locales/StringTemplate';
import Loader from '../Loader';
import styles from '../../theme/Style';

class ActivityLinkType extends React.Component {
    constructor(props) {
        super(props)

        this.state = {
            activity: props.activity,
            downloadPercentage: 0,
            downloaderVisibility: false,
            canceled: false,
            image: { uri: props.activity.cardImgName },
            onBackground: false,
        }

        this.setActivityAsCompletedApiRequestBody = {
            almsPlusApiUrl: this.props.main.selectedOrganization.almsPlusApiUrl,
            accessToken: this.props.main.authData.access_token,
            getActivityType: Constants.ActivityListType.GetActivityBySelectedActivity,
            classId: null,
            courseId: props.activity.courseId,
            activityId: props.activity.activityId,
            termWeekId: null,
            take: 1,
            skip: 0
        }

        this.enrollmentProgressApiRequestBody = {
            almsPlusApiUrl: this.props.main.selectedOrganization.almsPlusApiUrl,
            accessToken: this.props.main.authData.access_token,
        }
    }

    componentWillReceiveProps(props) {
        if (this.props.activityInteract.viewCompletionCriteriaData !== props.activityInteract.viewCompletionCriteriaData
            && this.state.activity.activityId === props.activityInteract.viewCompletionCriteriaData[0].activityId) {
            this.props.getEnrollmentProgress(this.enrollmentProgressApiRequestBody)
        }
    }




    _renderDueDate() {
        if (this.state.activity.taskDeadLine !== null) {
            var curDate = Moment();
            var isAfter = Moment(curDate).isAfter(this.state.activity.taskDeadLine);
            if (isAfter) {
                return (
                    <View style={{ flexDirection: 'row' }}>
                        <TextView weight="bold" style={{ fontSize: 13, color: Colors.activity_due_date_past }}>
                            {this.props.main.languageResource.r_activity_due_date || strings('r_activity_due_date')}
                        </TextView>
                        <TextView weight="regular" style={{ fontSize: 12, color: Colors.activity_due_date_past }}>

                            {this.props.main.languageResource.r_activiy_due_outdate_day !== undefined
                                ? template(this.props.main.languageResource.r_activiy_due_outdate_day, { day: curDate.diff(this.state.activity.taskDeadLine, 'days') })
                                : curDate.diff(this.state.activity.taskDeadLine, 'days') !== 0
                                    ? template(strings('r_activiy_due_outdate_day'), { day: curDate.diff(this.state.activity.taskDeadLine, 'days') })
                                    : this.props.main.languageResource.r_activity_due_date_today || strings('r_activity_due_date_today')
                            }
                        </TextView>
                    </View>
                );
            } else {
                return (
                    <View style={{ flexDirection: 'row' }}>
                        <TextView weight="bold" style={{ fontSize: 12 }}>
                            {this.props.main.languageResource.r_activity_due_date || strings('r_activity_due_date')}
                        </TextView>
                        <TextView weight="regular" style={{ fontSize: 12, marginStart: 5 }}>
                            {Moment(this.state.activity.taskDeadLine).format('D MMMM, hh:mm ')}
                        </TextView>
                    </View>
                );
            }
        }
        else
            return null;
    }
    _renderDescription() {
        if (this.state.activity.description != null) {
            return (
                <View style={{ padding: 10 }}>
                    <HTML html={this.state.activity.description} />
                </View>
            )
        } else return null
    }

    setAsCompleted() {
        this.setActivityAsCompletedApiRequestBody.classId = this.props.courseDetail.course.classId;
        this.props.setAsCompleted(this.setActivityAsCompletedApiRequestBody);
    }

    _renderActionButtons() {
        return (
            <View style={{ flexDirection: 'row', alignContent: 'center' }}>
                <View style={{ flex: 0.5 }} />
                <TouchableOpacity
                    activeOpacity={0.7}
                    onPress={() => { this.setAsCompleted() }}
                    style={{ flexDirection: 'row', padding: 10, flex: 0.5, justifyContent: 'flex-end', alignItems: 'center' }}>
                    <Ionicons name="md-checkmark" size={25} color="black" style={{ marginEnd: 5 }} />
                    <TextView weight="bold" style={{ color: 'black', fontSize: 16 }}>
                        {this.props.main.languageResource.r_activity_completation_button_text || strings('r_activity_completation_button_text')}
                    </TextView>
                </TouchableOpacity>
            </View>
        )
    }

    _renderCardHeader() {
        return (
            <View style={{ flexDirection: 'row', padding: 10 }}>
                <Ionicons style={{ flex: 0.1 }} name="ios-link" size={25} color="black" />
                <View style={{ flex: 0.8, flexDirection: 'column', marginStart: 5 }}>
                    <TextView numberOfLines={2} weight="bold" style={{ color: 'black', fontSize: 18 }}>
                        {this.state.activity.name}
                    </TextView>
                    {this._renderDueDate()}
                </View>
                {this.props.courses.enrollmentProgressData.find(data => data.activityId === this.state.activity.activityId)
                    ? <View style={{ flex: 0.1, alignItems: 'center', justifyContent: 'center', height: styles.marked_completed_activity_icon_size, width: 30, borderRadius: 30 / 2, backgroundColor: Colors.primary }}>
                        <Ionicons name="ios-checkmark" size={styles.marked_completed_activity_icon_size} color="white" />
                    </View>
                    :
                    null
                }
            </View>
        )
    }

    onError(error) {
        this.setState({ image: require('../../assets/images/image_not_found.png') })
    }
    _renderCardContainer() {
        return (
            <View style={{ flexDirection: 'column' }}>
                {this.state.activity.cardImgName !== null
                    ? <Image style={{ aspectRatio: 16 / 9, width: undefined, height: undefined, alignSelf: 'stretch' }} source={this.state.image} onError={this.onError.bind(this)} />
                    : null}
                <View >
                    {this._renderDescription()}

                    {/* Card Link */}
                    <View style={{ padding: 10 }}>
                        <TouchableOpacity onPress={() => {
                            Linking.openURL(this.state.activity.fileEmbed)
                        }}>
                            <TextView weight="bold" style={{ marginTop: 10, color: 'black', fontSize: 15 }}>
                                {this.state.activity.fileEmbed}
                            </TextView>
                        </TouchableOpacity>

                    </View>
                </View>
            </View>
        )
    }


    render() {
        return (
            <View>
                <Loader loading={this.props.activityInteract.viewCompletionCriteriaFetching || this.props.courses.enrollmentProgressFetching} />
                <ScrollView>

                    <Card containerStyle={{ backgroundColor: 'white', flexDirection: 'column', padding: 0, margin: 5 }}>
                        <View>

                            {/* Card Header */}
                            {this._renderCardHeader()}

                            {/* Card Container */}
                            {this._renderCardContainer()}


                            <View style={{ marginTop: 20, height: 1, backgroundColor: Colors.lineColor }} />

                            {/* Card Action Button */}
                            {this._renderActionButtons()}
                        </View>
                    </Card>
                </ScrollView>
            </View >
        );
    }
}
const mapDispatchToProps = (dispatch) => {
    return {
        setAsCompleted: (...args) => dispatch(ActivityInteractActions.activityCompletionViewCriteriaRequest(...args)),
        getEnrollmentProgress: (...args) => dispatch(CoursesActions.getEnrollmentProgressRequest(...args)),
    }
}

const mapStateToProps = (state) => {
    return {
        main: state.main,
        courseDetail: state.courseDetail,
        activityInteract: state.activityInteract,
        courses: state.courses
    }
}
export default connect(mapStateToProps, mapDispatchToProps)(ActivityLinkType)