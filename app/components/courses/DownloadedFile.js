import React from 'react';

import { View, TouchableOpacity, Alert } from 'react-native';
import { Card } from 'react-native-elements';
import TextView from '../TextView';
import Ionicons from 'react-native-vector-icons/Ionicons';
import { bytesToSize } from '../../helpers/Calculate';
import Constants from '../../services/Constants';
import RNFetchBlob from 'rn-fetch-blob'

import { connect } from 'react-redux';
import { strings } from '../../locales/i18n';

import FileViewer from 'react-native-file-viewer';

class DownloadedFile extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            activity: this.props.activity
        }

    }

    _renderIcons() {
        switch (this.state.activity.file.extension) {
            case Constants.DocumentExtenstion.Video:
                return (
                    <Ionicons name="ios-play-circle" size={30} color="black" />
                )
            case Constants.DocumentExtenstion.Pdf:
                return (
                    <Ionicons name="ios-document" size={30} color="black" />
                )
            default:
                return (
                    <Ionicons name="ios-play-circle" size={30} color="black" />
                )
        }
    }
    _itemClicked() {
        switch (this.state.activity.file.extension) {
            case Constants.DocumentExtenstion.Video:
                if (this.state.activity.hasOwnProperty('downloadedVideoPath'))
                    this.props.navigation.navigate('VideoPlayer', { activity: this.state.activity })
                else
                    FileViewer.open(this.state.activity.downloadedFilePaths).then();
                break;

            case Constants.DocumentExtenstion.Pdf:
                this.props.navigation.navigate('PdfViewer', { activity: this.state.activity })
                break;
        }
    }
    _removeFile() {
        Alert.alert(this.props.main.languageResource.r_activity_downloaded_file_delete || strings('r_activity_downloaded_file_delete'),
            this.props.main.languageResource.r_activity_downloaded_file_delete_description || strings('r_activity_downloaded_file_delete_description'), [
            { text: this.props.main.languageResource.r_activity_downloaded_file_delete_action_cancel || strings('r_activity_downloaded_file_delete_action_cancel'), style: 'cancel' },
            { text: this.props.main.languageResource.r_activity_downloaded_file_delete_action_ok || strings('r_activity_downloaded_file_delete_action_ok'), onPress: () => this._removeFileAccepted() }
        ])
    }
    _removeFileAccepted() {
        RNFetchBlob.fs.unlink(this.state.activity.file.filePath).then(() => {
            this.props.onRemove(this.state.activity)
        })
    }
    render() {
        return (
            <Card containerStyle={{ margin: 5, padding: 10 }}>
                <TouchableOpacity activeOpacity={1} onPress={() => this._itemClicked()}>
                    <View style={{ flex: 1, flexDirection: 'row', alignItems: 'center' }}>
                        <View style={{ flex: 0.1, alignItems: 'center' }}>
                            {this._renderIcons()}
                        </View>
                        <View style={{ flex: 0.7, marginStart: 10, marginEnd: 10 }}>
                            <TextView weight="bold" style={{ color: 'black' }}>
                                {this.state.activity.file.fileName}
                            </TextView>
                        </View>
                        <View style={{ flex: 0.2, alignItems: 'flex-end' }}>
                            <TextView style={{ fontSize: 12, color: 'black' }} weight="bold">
                                {bytesToSize(this.state.activity.file.size)}
                            </TextView>
                        </View>
                    </View>
                    <View style={{ flex: 1, flexDirection: 'row', alignContent: 'center' }}>
                        <View style={{ flex: 0.5 }} />

                        <View style={{ flex: 0.5 }}>
                            <TouchableOpacity activeOpacity={0.7} style={{ alignSelf: 'flex-end', flexDirection: 'row', justifyContent: 'flex-end', alignItems: 'center', marginTop: 5 }} onPress={() => this._removeFile()}>
                                <Ionicons name="md-trash" size={20} color="black" style={{ marginEnd: 5 }} />
                                <TextView weight="bold" style={{ color: 'black', fontSize: 14 }}>
                                    {this.props.main.languageResource.r_activity_downloaded_file_delete || strings('r_activity_downloaded_file_delete')}
                                </TextView>
                            </TouchableOpacity>
                        </View>

                    </View>
                </TouchableOpacity>
            </Card>
        );
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
    }
}

const mapStateToProps = (state) => {
    return {
        courseDetail: state.courseDetail,
        main: state.main,
    }
}


export default connect(mapStateToProps, mapDispatchToProps)(DownloadedFile);

