//react libraries
import React from 'react';
import { View, TouchableOpacity, Platform, PermissionsAndroid, ImageBackground, ScrollView, Alert } from 'react-native';
import { Card, Image, Button } from 'react-native-elements';
import AsyncStorage from '@react-native-community/async-storage';

//3.rd libraries
import { showMessage, hideMessage } from 'react-native-flash-message';
import Ionicons from 'react-native-vector-icons/Ionicons';
import MaterialIcons from 'react-native-vector-icons/MaterialIcons'
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons'
import RNFetchBlob from 'rn-fetch-blob'
import HTML from 'react-native-render-html';
import Moment from 'moment';
import PercentageCircle from 'react-native-percentage-circle';
import NetInfo from '@react-native-community/netinfo';


//Components & styles
import TextView from '../TextView';
import DownloadAlert from '../../components/DownloadAlert';
import ActivityInteractActions from '../../redux/ActivityInteractRedux';
import CoursesActions from '../../redux/CoursesRedux';

//settings

import { template } from '../../locales/StringTemplate';
import { strings } from '../../locales/i18n';
import { connect } from 'react-redux';
import LocalStorageConstants from '../../local/LocalStorageConstants';
import Colors from '../../theme/Colors';
import Constants from '../../services/Constants';
import styles from '../../theme/Style';
import Loader from '../Loader';


//TODO: tek aktivite açıldıktan sonra onlayout methodunun implement etmen gerek,
class ActivityVideoType extends React.Component {
    constructor(props) {
        super(props)

        this.state = {
            downloadButtonVisibility: false,
            image: props.activity.cardImgName !== null ? { uri: props.activity.cardImgName } : require('../../assets/images/image_not_found.png'),
            activity: props.activity,
            downloadPercentage: 0,
            downloaderVisibility: false,
            canceled: false,
            onBackground: false,
        }

        this.setActivityAsCompletedApiRequestBody = {
            almsPlusApiUrl: this.props.main.selectedOrganization.almsPlusApiUrl,
            accessToken: this.props.main.authData.access_token,
            getActivityType: Constants.ActivityListType.GetActivityBySelectedActivity,
            classId: null,
            courseId: props.activity.courseId,
            activityId: props.activity.activityId,
            termWeekId: null,
            take: 1,
            skip: 0
        }
        this.enrollmentProgressApiRequestBody = {
            almsPlusApiUrl: this.props.main.selectedOrganization.almsPlusApiUrl,
            accessToken: this.props.main.authData.access_token,
        }
    }

    async componentDidMount() {
        const paths = await AsyncStorage.getItem(LocalStorageConstants.DownloadedFilePaths)
        if (paths !== null) {
            let downloadedData = JSON.parse(paths);
            var isFound = false
            for (let i = 0; i < downloadedData.length; i++) {
                if (this.state.activity.activityId === downloadedData[i].activityId)
                    isFound = true
            }
            if (!isFound)
                this.setState({ downloadButtonVisibility: true })
        } else {
            this.setState({ downloadButtonVisibility: true })
        }

    }

    componentWillReceiveProps(props) {
        if (this.props.activityInteract.viewCompletionCriteriaData !== props.activityInteract.viewCompletionCriteriaData
            && this.state.activity.activityId === props.activityInteract.viewCompletionCriteriaData[0].activityId) {
            this.props.getEnrollmentProgress(this.enrollmentProgressApiRequestBody)
        }
    }


    _videoPlayer() {
        this.props.navigation.navigate('VideoPlayer', { activity: this.state.activity })
    }


    downloadOnBackground() {
        this.setState({ onBackground: true, downloaderVisibility: false, downloadButtonVisibility: false })
    }
    downloadOnCancel() {
        this.setState({ downloaderVisibility: false, canceled: true, downloadPercentage: 0 })
        this.task.cancel()
    }



    requestStoragePermission() {
        try {
            PermissionsAndroid.requestMultiple([PermissionsAndroid.PERMISSIONS.READ_EXTERNAL_STORAGE, PermissionsAndroid.PERMISSIONS.WRITE_EXTERNAL_STORAGE]).then((result) => {
                if (result['android.permission.READ_EXTERNAL_STORAGE'] && result['android.permission.WRITE_EXTERNAL_STORAGE'] === 'granted') {
                    this.downloadVideoApi();
                } else {
                    console.log('Permissions denied');
                }
            })
        } catch (error) {
        }
    }

    _downloadVideo() {

        NetInfo.fetch().then(statu => {
            if (statu.isConnected) {
                if (Platform.OS === 'android') {
                    this.requestStoragePermission()
                } else {
                    this.downloadVideoApi()
                }
            }
            else {
                Alert.alert('', this.props.main.languageResource.r_activity_connection_error || strings('r_activity_connection_error'))
            }
        })
    }

    downloadVideoApi() {
        this.setState({
            downloaderVisibility: true,
            canceled: false,
        })

        let dirs = RNFetchBlob.fs.dirs
        let options = null;
        if (Platform.OS === 'ios')
            options = {
                fileCache: true,
                path: dirs.DocumentDir + '/' + this.state.activity.file.fileId + this.state.activity.file.extension,
                // addAndroidDownloads: {
                //     useDownloadManager: true, //uses the device's native download manager.
                //     notification: true,
                //     mediaScannable: true,
                //     // mime: 'text/plain',
                //     title: "Notification Title", // Title of download notification.
                //path: DownloadDir + this.state.activity.file.fileId + this.state.activity.file.extension, // this is the path where your download file will be in
                //     description: 'Downloading file.'
                // }
            }
        else
            options = {
                fileCache: true
            }

        this.task = RNFetchBlob.config(options).fetch('GET', this.state.activity.file.filePath);

        this.task.progress((received, total) => {
            console.log('a progress', received / total)

            this.setState({
                downloadPercentage: ((received / total) * 100).toFixed(1)
            })
        }).then((res) => {
            if (!this.state.canceled) {
                this.setState({
                    downloadButtonVisibility: false,
                    downloaderVisibility: false,
                    onBackground: false
                })
                AsyncStorage.getItem(LocalStorageConstants.DownloadedFilePaths).then(downloadedVideoPaths => {
                    let activity = Object.assign({}, this.state.activity);
                    activity.downloadedVideoPath = res.path();
                    console.log("activity with path: ", activity);
                    if (downloadedVideoPaths === null) {
                        downloadedVideoPaths = [];
                        downloadedVideoPaths.push(activity)
                        AsyncStorage.setItem(LocalStorageConstants.DownloadedFilePaths, JSON.stringify(downloadedVideoPaths));
                    } else {
                        let downloadedVideos = JSON.parse(downloadedVideoPaths);
                        downloadedVideos.push(activity);
                        AsyncStorage.setItem(LocalStorageConstants.DownloadedFilePaths, JSON.stringify(downloadedVideos));
                    }

                })
                // showMessage({
                //     message: 'İndirme tamamlandı!',
                //     description: this.state.activity.name + ' aktiviteye ait dosya indirildi.',
                //     type: 'success',
                //     duration: 3000,
                // })
                console.log("Success");
                console.log('file path:', res.path());
                let response = { 'path': res.path() }
                return response
            }
        }).catch((err) => {
            console.log("Error", err);
            this.setState({
                downloadPercentage: 0,
                downloaderVisibility: false,
                onBackground: false,
                downloadButtonVisibility: true

            })
            return { 'error': true, 'code': 600, 'text': 'connection error' }
        }).finally()

    }

    _renderDownloadButtons() {
        if (this.state.downloadButtonVisibility && this.state.activity.allowDownload) {
            return (
                <TouchableOpacity activeOpacity={0.7} style={{ flexDirection: 'row', padding: 10, flex: 0.5, justifyContent: 'flex-start', alignItems: 'center' }}
                    onPress={() => this._downloadVideo()}>
                    <MaterialCommunityIcons name="download" size={25} color="black" style={{ marginEnd: 5 }} />
                    <TextView weight="bold" style={{ color: 'black', fontSize: 16 }}>
                        {this.props.main.languageResource.r_activity_video_download_button_Text || strings('r_activity_video_download_button_Text')}
                    </TextView>
                </TouchableOpacity>

            )
        } else if (this.state.onBackground) {
            return (
                <View style={{ flex: 0.5, justifyContent: 'center', marginStart: 5 }}>
                    <PercentageCircle
                        radius={17}
                        borderWidth={3}
                        percent={this.state.downloadPercentage}
                        color={Colors.primary}>
                        <TextView weight="medium" style={{ fontSize: 11 }}>
                            {this.state.downloadPercentage}
                        </TextView>
                    </PercentageCircle>
                </View>
            )
        }
        else {
            return <View style={{ flex: 0.5 }} />
        }
    }

    _renderDueDate() {
        if (this.state.activity.taskDeadLine !== null) {
            var curDate = Moment();
            var isAfter = Moment(curDate).isAfter(this.state.activity.taskDeadLine);

            if (isAfter) {
                return (
                    <View style={{ flexDirection: 'row' }}>
                        <TextView weight="bold" style={{ fontSize: 13, color: Colors.activity_due_date_past }}>
                            {this.props.main.languageResource.r_activity_due_date || strings('r_activity_due_date')}
                        </TextView>
                        <TextView weight="regular" style={{ fontSize: 12, marginStart: 5, color: Colors.activity_due_date_past }}>
                            {this.props.main.languageResource.r_activiy_due_outdate_day !== undefined
                                ? template(this.props.main.languageResource.r_activiy_due_outdate_day, { day: curDate.diff(this.state.activity.taskDeadLine, 'days') })
                                : curDate.diff(this.state.activity.taskDeadLine, 'days') !== 0
                                    ? template(strings('r_activiy_due_outdate_day'), { day: curDate.diff(this.state.activity.taskDeadLine, 'days') })
                                    : this.props.main.languageResource.r_activity_due_date_today || strings('r_activity_due_date_today')
                            }
                        </TextView>
                    </View>
                );
            } else {
                return (
                    <View style={{ flexDirection: 'row' }}>
                        <TextView weight="bold" style={{ fontSize: 12 }}>
                            {this.props.main.languageResource.r_activity_due_date || strings('r_activity_due_date')}
                        </TextView>
                        <TextView weight="regular" style={{ fontSize: 12, marginStart: 5 }}>
                            {Moment(this.state.activity.taskDeadLine).format('D MMMM, hh:mm ')}
                        </TextView>
                    </View>
                );
            }
        }
        else
            return null;
    }

    _renderDescription() {
        if (this.state.activity.description != null && this.state.activity.description.length !== 0) {
            return (
                <View style={{ padding: 10 }}>
                    <HTML html={this.state.activity.description} />
                </View>
            )
        } else return null
    }
    _renderCompletionIcon() {
        const enrollmentData = this.props.courses.enrollmentProgressData.find(data => data.activityId === this.state.activity.activityId)
        if (enrollmentData)
            switch (enrollmentData.completionType) {
                case Constants.ActivityCompletionTypes.View:
                    if (enrollmentData.status === Constants.ActivityCompletedStatus.Completed)
                        return (
                            <View style={{ flex: 0.1, alignItems: 'center', justifyContent: 'center', height: styles.marked_completed_activity_icon_size, width: 30, borderRadius: 30 / 2, backgroundColor: Colors.primary }}>
                                <Ionicons name="ios-checkmark" size={styles.marked_completed_activity_icon_size} color="white" />
                            </View>
                        )
                    else return null
                case Constants.ActivityCompletionTypes.Progress:
                    if (enrollmentData.status === Constants.ActivityCompletedStatus.UnCompleted)
                        return (<View style={{ flex: 0.1, alignItems: 'center', justifyContent: 'center', height: styles.marked_completed_activity_icon_size, width: 30, borderRadius: 30 / 2, backgroundColor: Colors.primary }}>
                            <PercentageCircle
                                radius={17}
                                borderWidth={3}
                                percent={enrollmentData.progress}
                                color={Colors.primary}>
                                <TextView weight="medium" style={{ fontSize: 11 }}>
                                    %{enrollmentData.progress}
                                </TextView>
                            </PercentageCircle>
                        </View>
                        )
                    else if (enrollmentData.status === Constants.ActivityCompletedStatus.Completed) {
                        return (
                            <View style={{ flex: 0.1, alignItems: 'center', justifyContent: 'center', height: styles.marked_completed_activity_icon_size, width: 30, borderRadius: 30 / 2, backgroundColor: Colors.primary }}>
                                <Ionicons name="ios-checkmark" size={styles.marked_completed_activity_icon_size} color="white" />
                            </View>
                        )
                    }
                case Constants.ActivityCompletionTypes.Grade:
                    if (enrollmentData.status === Constants.ActivityCompletedStatus.Completed) {
                        return (
                            <View style={{ flex: 0.1, alignItems: 'center', justifyContent: 'center', height: styles.marked_completed_activity_icon_size, width: 30, borderRadius: 30 / 2, backgroundColor: Colors.primary }}>
                                <Ionicons name="ios-checkmark" size={styles.marked_completed_activity_icon_size} color="white" />
                            </View>
                        )
                    }

            }

        else return null
    }

    _renderCardHeader() {
        return (
            <View style={{ flexDirection: 'row', padding: 10 }}>
                <MaterialIcons style={{ flex: 0.1 }} name="play-circle-outline" size={25} />
                <View style={{ flex: 0.9, flexDirection: 'column', marginStart: 5 }}>
                    <TextView numberOfLines={2} weight="bold" style={{ color: 'black', fontSize: 18 }}>
                        {this.state.activity.name}
                    </TextView>
                    {this._renderDueDate()}
                </View>
                {this._renderCompletionIcon()}
            </View>
        )
    }

    onError(error) {
        console.log("video type image error")
        this.setState({ image: require('../../assets/images/image_not_found.png') })
    }
    _renderCardContainer() {
        return (
            <View style={{ marginTop: 10, flexDirection: 'column' }}>
                <View style={{ flex: 1, justifyContent: 'center' }}>
                    <TouchableOpacity activeOpacity={0.8} onPress={() => this._videoPlayer()}>
                        <ImageBackground style={{ aspectRatio: 16 / 9 }} source={this.state.image} onError={this.onError.bind(this)}>
                            <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
                                <Ionicons name="ios-play-circle" size={60} color="white" />
                            </View>
                        </ImageBackground>

                    </TouchableOpacity>

                </View>

                {this._renderDescription()}

            </View>
        )
    }

    setAsCompleted() {
        this.setActivityAsCompletedApiRequestBody.classId = this.props.courseDetail.course.classId;
        this.props.setAsCompleted(this.setActivityAsCompletedApiRequestBody);
    }

    _renderCompleteButton() {
        return (
            <TouchableOpacity
                activeOpacity={0.7}
                onPress={() => this.setAsCompleted()}
                style={{ flexDirection: 'row', padding: 10, flex: 0.5, justifyContent: 'flex-end', alignItems: 'center' }} >
                <Ionicons name="md-checkmark" size={25} color="black" style={{ marginEnd: 5 }} />
                <TextView weight="bold" style={{ color: 'black', fontSize: 16 }}>
                    {this.props.main.languageResource.r_activity_completation_button_text || strings('r_activity_completation_button_text')}
                </TextView>
            </TouchableOpacity>
        )
    }
    _renderActionButtons() {
        const enrollmentData = this.props.courses.enrollmentProgressData.find(data => data.activityId === this.state.activity.activityId)
        return (
            <View style={{ flexDirection: 'row', alignContent: 'center' }}>
                {this._renderDownloadButtons()}
                {this.state.activity.completionType === Constants.ActivityCompletionTypes.View
                    ? enrollmentData
                        ? enrollmentData.status === Constants.ActivityCompletedStatus.UnCompleted
                            ? this._renderCompleteButton()
                            : <View style={{ flex: 0.5 }} />
                        : this._renderCompleteButton()
                    : <View style={{ flex: 0.5 }} />}
            </View>
        )
    }

    render() {
        return (
            <View>
                <Loader loading={this.props.activityInteract.viewCompletionCriteriaFetching || this.props.courses.enrollmentProgressFetching} />
                <ScrollView>
                    <DownloadAlert downloading={this.state.downloaderVisibility} percent={this.state.downloadPercentage} onBackground={() => this.downloadOnBackground()} onCancel={() => this.downloadOnCancel()} />
                    <Card containerStyle={{ backgroundColor: 'white', flexDirection: 'column', padding: 0, margin: 5 }}>
                        <View>
                            {/* Card Header */}
                            {this._renderCardHeader()}

                            {/* Card Container */}
                            {this._renderCardContainer()}
                            <View style={{ marginTop: 20, height: 1, backgroundColor: Colors.lineColor }} />

                            {/* Card Action Button */}
                            {this._renderActionButtons()}
                        </View>
                    </Card>
                </ScrollView>
            </View>
        );
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        setAsCompleted: (...args) => dispatch(ActivityInteractActions.activityCompletionViewCriteriaRequest(...args)),
        getEnrollmentProgress: (...args) => dispatch(CoursesActions.getEnrollmentProgressRequest(...args)),
    }
}

const mapStateToProps = (state) => {
    return {
        main: state.main,
        courseDetail: state.courseDetail,
        activityInteract: state.activityInteract,
        courses: state.courses
    }
}
export default connect(mapStateToProps, mapDispatchToProps)(ActivityVideoType)