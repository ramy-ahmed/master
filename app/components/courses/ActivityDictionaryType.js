import React from 'react';
import { View, TouchableOpacity, ScrollView, StyleSheet } from 'react-native';
import { Card, Image, Button } from 'react-native-elements';
import Ionicons from 'react-native-vector-icons/Ionicons';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
import TextView from '../TextView';
import HTML from 'react-native-render-html';
import Colors from '../../theme/Colors';

import Moment from 'moment'
import { connect } from 'react-redux';
import { strings } from '../../locales/i18n';
import { template } from '../../locales/StringTemplate';
import Constants from '../../services/Constants';

class ActivityDictionaryType extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            activity: props.activity,
            dictionary: props.activity.dictionary.slice(0, Constants.ActivityDictionaryWordQuantity)
        }
    }

    _renderDueDate() {
        if (this.state.activity.taskDeadLine !== null) {
            var curDate = Moment();
            var isAfter = Moment(curDate).isAfter(this.state.activity.taskDeadLine);
            if (isAfter) {
                return (
                    <View style={{ flexDirection: 'row' }}>
                        <TextView weight="bold" style={{ fontSize: 13, color: Colors.activity_due_date_past }}>
                            {this.props.main.languageResource.r_activity_due_date || strings('r_activity_due_date')}

                        </TextView>
                        <TextView weight="regular" style={{ fontSize: 12, color: Colors.activity_due_date_past }}>
                            {this.props.main.languageResource.r_activiy_due_outdate_day !== undefined
                                ? template(this.props.main.languageResource.r_activiy_due_outdate_day, { day: curDate.diff(this.state.activity.taskDeadLine, 'days') })
                                : curDate.diff(this.state.activity.taskDeadLine, 'days') !== 0
                                    ? template(strings('r_activiy_due_outdate_day'), { day: curDate.diff(this.state.activity.taskDeadLine, 'days') })
                                    : this.props.main.languageResource.r_activity_due_date_today || strings('r_activity_due_date_today')
                            }
                        </TextView>
                    </View>
                );
            } else {
                return (
                    <View style={{ flexDirection: 'row' }}>
                        <TextView weight="bold" style={{ fontSize: 12 }}>
                            {this.props.main.languageResource.r_activity_due_date || strings('r_activity_due_date')}
                        </TextView>
                        <TextView weight="regular" style={{ fontSize: 12 }}>
                            {Moment(this.state.activity.taskDeadLine).format('D MMMM,hh:mm ')}
                        </TextView>
                    </View>
                );
            }
        }
        else
            return null;
    }

    _renderDictionaries() {
        return (this.state.dictionary.map((dictionary, index) => {
            return (
                <View style={{ flexDirection: 'row', marginTop: 5 }} key={index}>
                    <View style={{ borderColor: '#b0afac', borderWidth: 2 }} />
                    <View style={{ flexDirection: 'column', marginStart: 10, marginTop: 5, marginBottom: 5 }}>
                        <TextView style={{ fontSize: 15, color: 'black' }} weight="bold">
                            {dictionary.keyword}
                        </TextView>
                        <TextView style={{ fontSize: 14, color: 'black' }} weight="regular">
                            {dictionary.description}
                        </TextView>
                    </View>
                </View>
            );
        }))
    }

    _renderMoreDictionaries() {
        if (this.state.activity.dictionary.length > Constants.ActivityDictionaryWordQuantity && this.state.dictionary.length != this.state.activity.dictionary.length) {
            return (
                <TouchableOpacity onPress={() => {
                    this.setState({
                        dictionary: this.state.activity.dictionary
                    })
                }} activeOpacity={0.7} style={style.cardContainerMoreDictionary} >
                    <TextView weight="bold" style={{ padding: 10, alignItems: 'center', flex: 1, fontSize: 13, color: 'black' }}>
                        {this.props.main.languageResource.r_activity_dictionary_show_all_keywords !== undefined
                            ? template(this.props.main.languageResource.r_activity_dictionary_show_all_keywords, { keywordSize: this.state.activity.dictionary.length - Constants.ActivityDictionaryWordQuantity })
                            : template(strings('r_activity_dictionary_show_all_keywords'), { keywordSize: this.state.activity.dictionary.length - Constants.ActivityDictionaryWordQuantity })}
                    </TextView>
                </TouchableOpacity>
            )
        } else return null

    }
    render() {
        return (
            <View>
                <ScrollView>
                    <Card containerStyle={style.cardContainer}>
                        {/* Card Header */}
                        <View>
                            <View style={{ flexDirection: 'row', padding: 10 }}>
                                <MaterialCommunityIcons style={{ flex: 0.1 }} name="format-list-bulleted" size={25} color="black" />
                                <View style={{ flex: 0.8, flexDirection: 'column', marginStart: 5 }}>
                                    <TextView numberOfLines={2} weight="bold" style={{ color: 'black', fontSize: 18 }}>
                                        {this.state.activity.name}
                                    </TextView>
                                    {this._renderDueDate()}
                                </View>
                            </View>

                            {/* Card Container */}
                            <View style={{ padding: 10, flexDirection: 'column' }}>
                                <HTML html={this.state.activity.description} />
                                {
                                    this._renderDictionaries()
                                }
                                {
                                    this._renderMoreDictionaries()
                                }
                            </View>
                        </View>
                    </Card>
                </ScrollView>
            </View>
        );
    }
}

const style = StyleSheet.create({
    cardContainer: {
        backgroundColor: 'white',
        flexDirection: 'column',
        padding: 0,
        margin: 5
    },
    cardContainerMoreDictionary: {
        marginTop: 20,
        backgroundColor: Colors.background,
        alignItems: 'center',
        borderRadius: 5
    }
})

const mapDispatchToProps = (dispatch) => {
    return {}
}

const mapStateToProps = (state) => {
    return {
        main: state.main,
    }
}
export default connect(mapStateToProps, mapDispatchToProps)(ActivityDictionaryType)