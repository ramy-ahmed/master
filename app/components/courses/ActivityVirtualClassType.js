//react libraries
import React from 'react';
import { View, TouchableOpacity, Platform, PermissionsAndroid, Alert, ScrollView, Linking } from 'react-native';
import { Card, Image, Button } from 'react-native-elements';
import AsyncStorage from '@react-native-community/async-storage';
import FileViewer from 'react-native-file-viewer';
import NetInfo from '@react-native-community/netinfo';

//3.rd libraries
import Ionicons from 'react-native-vector-icons/Ionicons';
import MaterialIcons from 'react-native-vector-icons/MaterialIcons';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
import RNFetchBlob from 'rn-fetch-blob'
import Moment from 'moment';
import HTML from 'react-native-render-html';
import PercentageCircle from 'react-native-percentage-circle';

//Component & styles
import TextView from '../TextView';
import Colors from '../../theme/Colors';
import Constants from '../../services/Constants';
import ActivityInteractActions from '../../redux/ActivityInteractRedux';
import CoursesActions from '../../redux/CoursesRedux';
import LocalStorageConstants from '../../local/LocalStorageConstants';
import DownloadAlert from '../DownloadAlert';
import Loader from '../Loader';
import styles from '../../theme/Style';

//settings
import { connect } from 'react-redux';
import { strings } from '../../locales/i18n';
import { template } from '../../locales/StringTemplate';
import { bytesToSize } from '../../helpers/Calculate';


class ActivityVirtualClassType extends React.Component {
    constructor(props) {
        super(props)

        this.state = {
            activity: props.activity,
            downloadPercentage: 0,
            downloaderVisibility: false,
            canceled: false,
            image: { uri: props.activity.cardImgName },
            onBackground: false,
        }

        this.setActivityAsCompletedApiRequestBody = {
            almsPlusApiUrl: this.props.main.selectedOrganization.almsPlusApiUrl,
            accessToken: this.props.main.authData.access_token,
            getActivityType: Constants.ActivityListType.GetActivityBySelectedActivity,
            classId: null,
            courseId: props.activity.courseId,
            activityId: props.activity.activityId,
            termWeekId: null,
            take: 1,
            skip: 0
        }

        this.enrollmentProgressApiRequestBody = {
            almsPlusApiUrl: this.props.main.selectedOrganization.almsPlusApiUrl,
            accessToken: this.props.main.authData.access_token,
        }
    }

    clickedAttacthment() {
        if (Platform.OS === 'android') {
            this.requestStoragePermission()
        } else {
            this.downloadFile()
        }
    }

    componentWillReceiveProps(props) {
        if (this.props.activityInteract.viewCompletionCriteriaData !== props.activityInteract.viewCompletionCriteriaData
            && this.state.activity.activityId === props.activityInteract.viewCompletionCriteriaData[0].activityId) {
            this.props.getEnrollmentProgress(this.enrollmentProgressApiRequestBody)
        }
    }


    requestStoragePermission() {
        try {
            PermissionsAndroid.requestMultiple([PermissionsAndroid.PERMISSIONS.READ_EXTERNAL_STORAGE, PermissionsAndroid.PERMISSIONS.WRITE_EXTERNAL_STORAGE]).then((result) => {
                if (result['android.permission.READ_EXTERNAL_STORAGE'] && result['android.permission.WRITE_EXTERNAL_STORAGE'] === 'granted') {
                    this.downloadFile();
                } else {
                    console.log('Permissions denied');
                }
            })
        } catch (error) {
        }
    }
    async checkDocumentIsExist() {
        const paths = await AsyncStorage.getItem(LocalStorageConstants.DownloadedFilePaths)
        if (paths !== null) {
            let downloadedData = JSON.parse(paths);
            var exist = false
            for (let i = 0; i < downloadedData.length; i++) {
                if (downloadedData[i].file.fileId === this.props.activity.file.fileId)
                    exist = true
            }
            return exist
        } else {
            return false
        }
    }
    async getAlreadyDownloadedFile() {
        const paths = await AsyncStorage.getItem(LocalStorageConstants.DownloadedFilePaths);
        if (paths !== null) {
            let downloadedData = JSON.parse(paths);
            for (var i = 0; i < downloadedData.length; i++) {
                if (this.state.activity.activityId === downloadedData[i].activityId) {
                    return downloadedData[i].downloadedFilePaths;
                }
            }
        }
    }
    async saveDownloadedFile(res) {
        var objActivity = Object.assign({}, this.state.activity);
        objActivity.downloadedFilePaths = res.path();
        await this.setState({ activity: objActivity })

        var downloadedFilePaths = await AsyncStorage.getItem(LocalStorageConstants.DownloadedFilePaths);
        if (downloadedFilePaths === null) {
            downloadedFilePaths = [];
            downloadedFilePaths.push(objActivity)
            return await AsyncStorage.setItem(LocalStorageConstants.DownloadedFilePaths, JSON.stringify(downloadedFilePaths)).then(result => {
                console.log("file path saved")
                return true;
            })
        } else {
            let downloadedFiles = JSON.parse(downloadedFilePaths);
            downloadedFiles.push(objActivity);
            return await AsyncStorage.setItem(LocalStorageConstants.DownloadedFilePaths, JSON.stringify(downloadedFiles)).then(result => {
                console.log("file path saved")
                return true;
            })
        }
    }
    downloadFile() {
        this.checkDocumentIsExist().then(exist => {
            console.log("document exist? : ", exist);
            if (!exist) {
                NetInfo.fetch().then(statu => {
                    if (statu.isConnected) {
                        this.setState({
                            downloaderVisibility: true,
                            canceled: false,
                        })
                        let dirs = RNFetchBlob.fs.dirs
                        let options = {
                            fileCache: true,
                            path: dirs.DocumentDir + '/' + this.state.activity.file.fileId + this.state.activity.file.extension,
                            appendExt: this.state.activity.file.extension,
                        }
                        this.task = RNFetchBlob.config(options).fetch('GET', this.state.activity.file.filePath);
                        this.task.progress((received, total) => {
                            console.log('a progress', received / total)
                            this.setState({
                                downloadPercentage: ((received / total) * 100).toFixed(1)
                            })
                        }).then((res) => {
                            if (!this.state.canceled) {
                                console.log('file path:', res.path());
                                this.saveDownloadedFile(res).then(result => {
                                    this.setState({
                                        downloaderVisibility: false,
                                        onBackground: false
                                    })
                                    if (this.state.activity.file.extension == '.pdf') {
                                        this.props.navigation.navigate('PdfViewer', { activity: this.state.activity })
                                    } else {
                                        FileViewer.open(res.path()).then().catch(error => {
                                            Alert.alert('', this.props.main.languageResource.r_activity_attach_file_open_error || strings('r_activity_attach_file_open_error'))
                                        })
                                    }
                                })
                            }
                        }).catch((err) => {
                            console.log("Error", err);
                            this.setState({
                                downloaderVisibility: false,
                                onBackground: false
                            })
                            //TODO: Error alert doğru mu? 
                            // ErrorAlert(600, null, "Dosyayı açarken problem oluştu!")
                            return { 'error': true, 'code': 600, 'text': 'connection error' }
                        })
                    } else {
                        Alert.alert('', this.props.main.languageResource.r_activity_attach_file_download_connection_error || strings('r_activity_attach_file_download_connection_error'))
                    }
                })
            } else {
                if (this.state.activity.file.extension == '.pdf') {
                    this.props.navigation.navigate('PdfViewer', { activity: this.state.activity })
                } else {
                    this.getAlreadyDownloadedFile().then(filePath => {
                        FileViewer.open(filePath).then().catch(error => {
                            Alert.alert('', this.props.main.languageResource.r_activity_attach_file_open_error || strings('r_activity_attach_file_open_error'))
                        })
                    })

                }
            }
        })

    }
    _renderDueDate() {
        if (this.state.activity.taskDeadLine !== null) {
            var curDate = Moment();
            var isAfter = Moment(curDate).isAfter(this.state.activity.taskDeadLine);
            if (isAfter) {
                return (
                    <View style={{ flexDirection: 'row' }}>
                        <TextView weight="bold" style={{ fontSize: 13, color: Colors.activity_due_date_past }}>
                            {this.props.main.languageResource.r_activity_due_date || strings('r_activity_due_date')}
                        </TextView>
                        <TextView weight="regular" style={{ fontSize: 12, marginStart: 5, color: Colors.activity_due_date_past }}>

                            {this.props.main.languageResource.r_activiy_due_outdate_day !== undefined
                                ? template(this.props.main.languageResource.r_activiy_due_outdate_day, { day: curDate.diff(this.state.activity.taskDeadLine, 'days') })
                                : curDate.diff(this.state.activity.taskDeadLine, 'days') !== 0
                                    ? template(strings('r_activiy_due_outdate_day'), { day: curDate.diff(this.state.activity.taskDeadLine, 'days') })
                                    : this.props.main.languageResource.r_activity_due_date_today || strings('r_activity_due_date_today')
                            }
                        </TextView>
                    </View>
                );
            } else {
                return (
                    <View style={{ flexDirection: 'row' }}>
                        <TextView weight="bold" style={{ fontSize: 12 }}>
                            {this.props.main.languageResource.r_activity_due_date || strings('r_activity_due_date')}
                        </TextView>
                        <TextView weight="regular" style={{ fontSize: 12, marginStart: 5 }}>
                            {Moment(this.state.activity.taskDeadLine).format('D MMMM, hh:mm ')}
                        </TextView>
                    </View>
                );
            }
        }
        else
            return null;
    }

    _renderFileAttachments() {
        if (this.state.activity.file.fileId !== null) {
            return (
                <View>
                    <TextView weight="bold" style={{ fontSize: 16, color: 'black', padding: 10 }}>
                        {this.props.main.languageResource.r_activity_attached_file_title || strings('r_activity_attached_file_title')}
                    </TextView>
                    <TouchableOpacity activeOpacity={0.7} onPress={() => this.clickedAttacthment()} style={{ borderRadius: 5, borderWidth: 0.3, margin: 10, flex: 1, flexDirection: 'row', paddingTop: 5, paddingBottom: 5, paddingStart: 10, paddingEnd: 10, alignItems: 'center' }}>
                        <Ionicons style={{ flex: 0.05 }} name="ios-attach" size={20} />
                        <TextView style={{ flex: 0.75, marginStart: 5 }} weight="regular" numberOfLines={1} ellipsizeMode={'middle'}>{this.state.activity.file.fileName}</TextView>
                        <View style={{ flex: 0.2, alignItems: 'flex-end' }}>
                            <TextView style={{ fontSize: 12, color: 'black' }} weight="bold">
                                {bytesToSize(this.state.activity.file.size)}
                            </TextView>
                        </View>
                    </TouchableOpacity>
                </View>
            );
        } else return null;
    }


    _renderDescription() {
        if (this.state.activity.description != null) {
            return (
                <View style={{ padding: 10 }}>
                    <HTML html={this.state.activity.description} />
                </View>
            )
        } else return null
    }

    setAsCompleted() {
        this.setActivityAsCompletedApiRequestBody.classId = this.props.courseDetail.course.classId;
        this.props.setAsCompleted(this.setActivityAsCompletedApiRequestBody);
    }

    openVirtualClassDetail() {
        //Linking.openURL(this.props.main.selectedOrganization.almsPlusApiUrl + this.state.activity.activityDetailUrl)
        this.props.navigation.navigate('ActivityDetailWebView', { activity: this.state.activity })
    }

    _renderActionButtons() {
        return (
            <View style={{ flexDirection: 'row', alignContent: 'center' }}>
                {this.state.onBackground
                    ? <View style={{ flex: 0.5, justifyContent: 'center', margin: 5 }}>
                        <PercentageCircle
                            radius={17}
                            borderWidth={3}
                            percent={this.state.downloadPercentage}
                            color={Colors.primary}>
                            <TextView weight="medium" style={{ fontSize: 11 }}>
                                {this.state.downloadPercentage}
                            </TextView>
                        </PercentageCircle>
                    </View>
                    : <View style={{ flex: 0.5 }} />
                }
                <View style={{ flex: 0.5, justifyContent: 'center', margin: 5 }}>
                    <Button
                        icon={<Ionicons name="ios-eye" size={25} color="white" />}
                        onPress={() => this.openVirtualClassDetail()}
                        title={this.props.main.languageResource.r_activity_show_btn_text || strings('r_activity_show_btn_text')}
                        titleStyle={{ marginStart: 5 }}
                        buttonStyle={{ backgroundColor: Colors.primary }} />
                </View>
            </View>
        )
    }
    downloadOnBackground() {
        console.log("downloadOnBackground")
        this.setState({ onBackground: true, downloaderVisibility: false })
    }
    downloadOnCancel() {
        console.log("downloadOnCancel2")
        this.setState({ downloaderVisibility: false, canceled: true, downloadPercentage: 0 })
        this.task.cancel()

    }
    _renderCardHeader() {
        return (
            <View style={{ flexDirection: 'row', padding: 10 }}>
                <MaterialIcons style={{ flex: 0.1 }} name="computer" size={25} color="black" />
                <View style={{ flex: 0.8, flexDirection: 'column', marginStart: 5 }}>
                    <TextView numberOfLines={2} weight="bold" style={{ color: 'black', fontSize: 18 }}>
                        {this.state.activity.name}
                    </TextView>
                    {this._renderDueDate()}
                </View>
                {this.props.courses.enrollmentProgressData.find(data => data.activityId === this.state.activity.activityId)
                    ? <View style={{ flex: 0.1, alignItems: 'center', justifyContent: 'center', height: styles.marked_completed_activity_icon_size, width: 30, borderRadius: 30 / 2, backgroundColor: Colors.primary }}>
                        <Ionicons name="ios-checkmark" size={styles.marked_completed_activity_icon_size} color="white" />
                    </View>
                    :
                    null
                }
            </View>
        )
    }

    onError(error) {
        this.setState({ image: require('../../assets/images/image_not_found.png') })
    }
    _renderCardContainer() {
        return (
            <View style={{ marginTop: 10, flexDirection: 'column' }}>
                {this.state.activity.cardImgName !== null
                    ? <Image style={{ aspectRatio: 16 / 9, width: undefined, height: undefined, alignSelf: 'stretch' }} source={this.state.image} onError={this.onError.bind(this)} />
                    : null}
                {this._renderDescription()}
            </View>
        )
    }


    render() {
        return (
            <View>
                <Loader loading={this.props.activityInteract.viewCompletionCriteriaFetching || this.props.courses.enrollmentProgressFetching} />
                <ScrollView>
                    <DownloadAlert
                        downloading={this.state.downloaderVisibility}
                        percent={this.state.downloadPercentage}
                        onBackground={() => this.downloadOnBackground()}
                        onCancel={() => this.downloadOnCancel()} />

                    <Card containerStyle={{ backgroundColor: 'white', flexDirection: 'column', padding: 0, margin: 5 }}>
                        <View>

                            {/* Card Header */}
                            {this._renderCardHeader()}

                            {/* Card Container */}
                            {this._renderCardContainer()}

                            {/* Card Attachments */}
                            {this._renderFileAttachments()}
                            <View style={{ marginTop: 20, height: 1, backgroundColor: Colors.lineColor }} />

                            {/* Card Action Button */}
                            {this._renderActionButtons()}
                        </View>
                    </Card>
                </ScrollView>
            </View >
        );
    }
}
const mapDispatchToProps = (dispatch) => {
    return {
        setAsCompleted: (...args) => dispatch(ActivityInteractActions.activityCompletionViewCriteriaRequest(...args)),
        getEnrollmentProgress: (...args) => dispatch(CoursesActions.getEnrollmentProgressRequest(...args)),
    }
}

const mapStateToProps = (state) => {
    return {
        main: state.main,
        courseDetail: state.courseDetail,
        activityInteract: state.activityInteract,
        courses: state.courses
    }
}
export default connect(mapStateToProps, mapDispatchToProps)(ActivityVirtualClassType)