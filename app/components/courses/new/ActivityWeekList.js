import React from 'react';
import { View, FlatList, TouchableOpacity, StyleSheet } from 'react-native';
import TextView from '../../TextView';
import Colors from '../../../theme/Colors';

import { CheckBox } from 'react-native-elements';

import { connect } from 'react-redux';
import { template } from '../../../locales/StringTemplate';
import { strings } from '../../../locales/i18n';
class ActivityWeekList extends React.Component {

    constructor(props) {
        super(props)

        this.state = {
            weeks: [],
            selectedWeeks: [],
        }

    }
    componentDidMount() {
        let weeksSorted = Object.assign([], this.props.weeks);
        weeksSorted.sort((a, b) => a.week - b.week)
        this.setState({
            weeks: weeksSorted,
        })

    }

    keyExtractor = (item, index) => item.termWeekId



    render() {
        return (
            <FlatList
                data={this.state.weeks}
                extraData={this.state}
                numColumns={3}
                keyExtractor={this.keyExtractor}
                renderItem={({ item }) => (
                    <TouchableOpacity style={style.activityweekListContainer}
                        onPress={() => { //buradaki değişiklikleri checkbox onpress'de de yapmalısın.
                        }}>
                        <CheckBox
                            onPress={() => {
                                if (this.state.selectedWeeks.indexOf(item) > -1) {
                                    let filteredSelectedWeeks = this.state.selectedWeeks.filter(week => week !== item)
                                    this.props.selectedWeeks(filteredSelectedWeeks)
                                    this.setState({ selectedWeeks: filteredSelectedWeeks })
                                } else {
                                    this.setState({
                                        selectedWeeks: [...this.state.selectedWeeks, item]
                                    })
                                    this.props.selectedWeeks([...this.state.selectedWeeks, item])
                                }
                            }}
                            checked={this.state.selectedWeeks.indexOf(item) > -1}
                            title={this.props.main.languageResource.r_activity_add_content_week_text !== undefined
                                ? template(this.props.main.languageResource.r_activity_add_content_week_text, { weekName: item.week })
                                : template(strings('r_activity_add_content_week_text'), { weekName: item.week })}
                            containerStyle={style.activityWeekListItem}
                            checkedColor={Colors.primary} />
                    </TouchableOpacity>
                )}
            />
        )
    }
}

const style = StyleSheet.create({
    activityweekListContainer: {
        flex: 1,
        margin: 2,
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: Colors.background
    },
    activityWeekListItem: {
        backgroundColor: 'transparent',
        padding: 5,
        margin: 0
    }
})

const mapDispatchToProps = (dispatch) => {
    return {}
}

const mapStateToProps = (state) => {
    return {
        main: state.main,
    }
}


export default connect(mapStateToProps, mapDispatchToProps)(ActivityWeekList)