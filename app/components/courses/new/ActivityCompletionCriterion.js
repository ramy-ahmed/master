import React from 'react';
import { View, TouchableOpacity, FlatList, StyleSheet } from 'react-native';
import { CheckBox } from 'react-native-elements';

import Colors from '../../../theme/Colors';
import Constants from '../../../services/Constants';
import TextView from '../../TextView';
import { connect } from 'react-redux';
import { strings } from '../../../locales/i18n';


class ActivityCompletionCriterion extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            ActivityCompletionCriterion: 0,
            completionTypes: [],
            selectedFile: null
        }
    }

    componentWillReceiveProps(props) {
        if (props.selectedFile !== null) {
            switch (this.props.activityType) {
                case Constants.ActivityType.Assignment:
                    this.setState({
                        completionTypes: [
                            { type: Constants.ActivityCompletionTypes.View, text: this.props.main.languageResource.r_activity_completion_type_view || strings('r_activity_completion_type_view') },
                            { type: Constants.ActivityCompletionTypes.Upload, text: this.props.main.languageResource.r_activity_completion_type_upload_file || strings('r_activity_completion_type_upload_file') },
                            { type: Constants.ActivityCompletionTypes.Grade, text: this.props.main.languageResource.r_activity_completion_type_grading || strings('r_activity_completion_type_grading') },
                            { type: Constants.ActivityCompletionTypes.DownloadAttachment, text: this.props.main.languageResource.r_activity_completion_type_download_attachment || strings('r_activity_completion_type_download_attachment') }
                        ]
                    })
                    break
                case Constants.ActivityType.Document:
                    this.setState({
                        completionTypes: [
                            { type: Constants.ActivityCompletionTypes.View, text: this.props.main.languageResource.r_activity_completion_type_view || strings('r_activity_completion_type_view') },
                            { type: Constants.ActivityCompletionTypes.DownloadAttachment, text: this.props.main.languageResource.r_activity_completion_type_download_attachment || strings('r_activity_completion_type_download_attachment') },
                        ]
                    })
            }
        } else {
            switch (this.props.activityType) {
                case Constants.ActivityType.Assignment:
                    this.setState({
                        completionTypes: [
                            { type: Constants.ActivityCompletionTypes.View, text: this.props.main.languageResource.r_activity_completion_type_view || strings('r_activity_completion_type_view') },
                            { type: Constants.ActivityCompletionTypes.Upload, text: this.props.main.languageResource.r_activity_completion_type_upload_file || strings('r_activity_completion_type_upload_file') },
                            { type: Constants.ActivityCompletionTypes.Grade, text: this.props.main.languageResource.r_activity_completion_type_grading || strings('r_activity_completion_type_grading') },
                        ]
                    })
                    break
                case Constants.ActivityType.Document:
                    this.setState({
                        completionTypes: [
                            { type: Constants.ActivityCompletionTypes.View, text: this.props.main.languageResource.r_activity_completion_type_view || strings('r_activity_completion_type_view') },
                        ]
                    })
            }
        }
        this.setState({ selectedFile: props.selectedFile });

    }
    componentDidMount() {
        switch (this.props.activityType) {
            case Constants.ActivityType.Assignment:
                this.setState({
                    completionTypes: [
                        { type: Constants.ActivityCompletionTypes.View, text: this.props.main.languageResource.r_activity_completion_type_view || strings('r_activity_completion_type_view') },
                        { type: Constants.ActivityCompletionTypes.Upload, text: this.props.main.languageResource.r_activity_completion_type_upload_file || strings('r_activity_completion_type_upload_file') },
                        { type: Constants.ActivityCompletionTypes.Grade, text: this.props.main.languageResource.r_activity_completion_type_grading || strings('r_activity_completion_type_grading') },
                    ]
                })
                break
            case Constants.ActivityType.Document:
                this.setState({
                    completionTypes: [
                        { type: Constants.ActivityCompletionTypes.View, text: this.props.main.languageResource.r_activity_completion_type_view || strings('r_activity_completion_type_view') },
                    ]
                })
                break
            case Constants.ActivityType.Video:
                this.setState({
                    completionTypes: [
                        { type: Constants.ActivityCompletionTypes.View, text: this.props.main.languageResource.r_activity_completion_type_view || strings('r_activity_completion_type_view') },
                        { type: Constants.ActivityCompletionTypes.Progress, text: this.props.main.languageResource.r_activity_completion_type_progress || strings('r_activity_completion_type_progress') },
                        { type: Constants.ActivityCompletionTypes.Grade, text: this.props.main.languageResource.r_activity_completion_type_grading || strings('r_activity_completion_type_grading') },
                    ]
                })
                break
            case Constants.ActivityType.LinkActivity:
                this.setState({
                    completionTypes: [
                        { type: Constants.ActivityCompletionTypes.View, text: this.props.main.languageResource.r_activity_completion_type_view || strings('r_activity_completion_type_view') },
                    ]
                })

        }
    }

    keyExtractor = (item, index) => index.toString
    render() {
        return (
            <FlatList
                data={this.state.completionTypes}
                extraData={this.state}
                numColumns={3}
                style={{ marginTop: 5 }}
                keyExtractor={this.keyExtractor}
                renderItem={({ item }) => (
                    <TouchableOpacity style={style.criterionContainer}
                        onPress={() => { //buradaki değişiklikleri checkbox onpress'de de yapmalısın.
                            this.props.onChange(item.type)
                            this.setState({ ActivityCompletionCriterion: item.type })
                        }}>
                        <CheckBox
                            checkedIcon='dot-circle-o'
                            uncheckedIcon='circle-o'
                            onPress={() => {
                                this.props.onChange(item.type)
                                this.setState({ ActivityCompletionCriterion: item.type })
                            }}
                            checked={this.state.ActivityCompletionCriterion === item.type}
                            containerStyle={{ backgroundColor: 'transparent', padding: 0, margin: 0 }}
                            checkedColor={Colors.primary} />
                        <TextView weight="bold" style={style.criterionText}>{item.text}</TextView>
                    </TouchableOpacity>
                )}
            />
        )
    }
}

const style = StyleSheet.create({
    criterionContainer: {
        flex: 1,
        margin: 2,
        flexDirection: 'column',
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: Colors.background
    },
    criterionText: {
        color: 'black',
        alignSelf: 'center',
        textAlign: 'center'
    }
})
const mapDispatchToProps = (dispatch) => {
    return {}
}

const mapStateToProps = (state) => {
    return {
        main: state.main,
    }
}
export default connect(mapStateToProps, mapDispatchToProps)(ActivityCompletionCriterion);