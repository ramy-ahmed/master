import React from 'react';
import { View, TouchableOpacity, Platform, TextInput, ScrollView, Linking } from 'react-native';
import { Card, Image, Button, ButtonGroup, CheckBox } from 'react-native-elements';
import Ionicons from 'react-native-vector-icons/Ionicons';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons'

import TextView from '../TextView';
import HTML from 'react-native-render-html';
import Colors from '../../theme/Colors';

import Moment from 'moment';
import Constants from '../../services/Constants';
import ActivityInteractActions from '../../redux/ActivityInteractRedux';
import CoursesActions from '../../redux/CoursesRedux';
import { connect } from 'react-redux';
import { strings } from '../../locales/i18n';
import { template } from '../../locales/StringTemplate';
import styles from '../../theme/Style';

class ActivitySurveyType extends React.Component {
    constructor(props) {
        super(props)

        this.state = {
            activity: props.activity,
            image: { uri: props.activity.cardImgName },
            oneSelection: 0,
            singleChoiseAnswer: null,
            multiChoiseAnswer: [],
            moreThanOneQuestion: false,
            multiLineQuestionAnswer: "",
            singleLineQuestionAnswer: "",

        }

        this.setActivityAsCompletedApiRequestBody = {
            almsPlusApiUrl: this.props.main.selectedOrganization.almsPlusApiUrl,
            accessToken: this.props.main.authData.access_token,
            getActivityType: Constants.ActivityListType.GetActivityBySelectedActivity,
            classId: null,
            courseId: props.activity.courseId,
            activityId: props.activity.activityId,
            termWeekId: null,
            take: 1,
            skip: 0
        }

        this.enrollmentProgressApiRequestBody = {
            almsPlusApiUrl: this.props.main.selectedOrganization.almsPlusApiUrl,
            accessToken: this.props.main.authData.access_token,
        }
    }
    componentDidMount() {
        const reSortedList = [].concat(this.state.activity.surveyList.questionList)
            .filter(question => question.questionType !== Constants.ActivitySurveyQuestionTypes.SurveyTitle)
            .filter(question => question.questionType !== Constants.ActivitySurveyQuestionTypes.SurveyParagraph);
        if (reSortedList.length > 1)
            this.setState({ moreThanOneQuestion: true })
    }

    componentWillReceiveProps(props) {
        if (this.props.activityInteract.viewCompletionCriteriaData !== props.activityInteract.viewCompletionCriteriaData
            && this.state.activity.activityId === props.activityInteract.viewCompletionCriteriaData[0].activityId) {
            this.props.getEnrollmentProgress(this.enrollmentProgressApiRequestBody)
        }
    }

    openSurveyDetail() {
        // Linking.openURL(this.props.main.selectedOrganization.almsPlusApiUrl + this.state.activity.activityDetailUrl)
        this.props.navigation.navigate('ActivityDetailWebView', { activity: this.state.activity })

    }

    onError(error) {
        this.setState({ image: require('../../assets/images/empty-image-card.png') })
    }
    _renderDueDate() {
        if (this.state.activity.taskDeadLine !== null) {
            var curDate = Moment();
            var isAfter = Moment(curDate).isAfter(this.state.activity.taskDeadLine);
            if (isAfter) {
                return (
                    <View style={{ flexDirection: 'row' }}>
                        <TextView weight="bold" style={{ fontSize: 13, color: Colors.activity_due_date_past }}>
                            {this.props.main.languageResource.r_activity_due_date || strings('r_activity_due_date')}
                        </TextView>
                        <TextView weight="regular" style={{ fontSize: 12, marginStart: 5, color: Colors.activity_due_date_past }}>
                            {this.props.main.languageResource.r_activiy_due_outdate_day !== undefined
                                ? template(this.props.main.languageResource.r_activiy_due_outdate_day, { day: curDate.diff(this.state.activity.taskDeadLine, 'days') })
                                : curDate.diff(this.state.activity.taskDeadLine, 'days') !== 0
                                    ? template(strings('r_activiy_due_outdate_day'), { day: curDate.diff(this.state.activity.taskDeadLine, 'days') })
                                    : this.props.main.languageResource.r_activity_due_date_today || strings('r_activity_due_date_today')
                            }
                        </TextView>
                    </View>
                );
            } else {
                return (
                    <View style={{ flexDirection: 'row' }}>
                        <TextView weight="bold" style={{ fontSize: 12 }}>
                            {this.props.main.languageResource.r_activity_due_date || strings('r_activity_due_date')}
                        </TextView>
                        <TextView weight="regular" style={{ fontSize: 12, marginStart: 5 }}>
                            {Moment(this.state.activity.taskDeadLine).format('D MMMM, hh:mm ')}
                        </TextView>
                    </View>
                );
            }
        }
        else
            return null;
    }

    _renderCardImage() {
        if (this.state.activity.cardImgName !== null) {
            return (<Image style={{ aspectRatio: 16 / 9, width: undefined, height: undefined, alignSelf: 'stretch' }} source={this.state.image} onError={this.onError.bind(this)} />)
        } else
            return null
    }

    _renderDescription() {
        if (this.state.activity.description !== null && this.state.activity.description.length !== 0) {
            return (
                <HTML html={this.state.activity.description} />
            );
        } else return null;
    }

    _renderQuestions() {
        //uygulamada sadece 1 sorusu olan anketler gözekecek
        return this.state.moreThanOneQuestion
            ? null
            : <View style={{ marginTop: 20 }}>
                {
                    [].concat(this.state.activity.surveyList.questionList)
                        .sort((a, b) => a.displayOrder - b.displayOrder)
                        .map((question, index) => {
                            switch (question.questionType) {
                                case Constants.ActivitySurveyQuestionTypes.SurveyTitle:
                                    return this._renderSurveyTitle(question, index)
                                case Constants.ActivitySurveyQuestionTypes.SurveyParagraph:
                                    return this._renderSurveyParagraph(question, index);
                                case Constants.ActivitySurveyQuestionTypes.SingleChoise: //soru tipi
                                    return this._renderSinglChoiseQuestion(question, index);
                                case Constants.ActivitySurveyQuestionTypes.MultiChoise: //Soru tipi
                                    return this._renderMultiChoiseQuestion(question, index);
                                case Constants.ActivitySurveyQuestionTypes.MultiLineQuestion: //Soru tipi
                                    return this._renderMultiLineQuestion(question, index);
                                case Constants.ActivitySurveyQuestionTypes.OneLineQuestion: //Soru tipi
                                    return this._renderOneLineQuestion(question, index);
                                case Constants.ActivitySurveyQuestionTypes.Matris://soru tipi
                                    return null;
                            }
                        })
                }
            </View>
    }

    _renderOneLineQuestion(question, index) {
        return (
            <View style={{ flex: 1, flexDirection: 'column' }} key={index}>
                <View style={{ marginTop: 20, marginBottom: 20, height: 1, backgroundColor: Colors.lineColor }} />

                <TextView weight="bold" style={{ color: 'black', fontSize: styles.survey_desciption_size }}>
                    {question.description}
                </TextView>

                <TextInput
                    style={{ fontSize: 16, borderColor: Colors.background, textAlignVertical: 'top', borderWidth: 1, marginTop: 5 }}
                    onChangeText={(text) => this.setState({ singleLineQuestionAnswer: text })}>{this.state.singleLineQuestionAnswer}</TextInput>

            </View>
        );
    }

    _renderMultiLineQuestion(question, index) {
        return (
            <View style={{ flex: 1, flexDirection: 'column' }} key={index}>
                <View style={{ marginTop: 20, marginBottom: 20, height: 1, backgroundColor: Colors.lineColor }} />
                <TextView weight="bold" style={{ color: 'black', fontSize: styles.survey_desciption_size }}>
                    {question.description}
                </TextView>
                <TextInput
                    multiline={true}
                    numberOfLines={Platform.OS === 'ios' ? null : 5}
                    minHeight={Platform.OS === 'ios' ? (20 * 5) : null}
                    style={{ fontSize: 16, borderColor: Colors.background, textAlignVertical: 'top', borderWidth: 1, marginTop: 5 }}
                    onChangeText={(text) => this.setState({ multiLineQuestionAnswer: text })}>{this.state.multiLineQuestionAnswer}</TextInput>
            </View>
        );
    }

    _renderSinglChoiseQuestion(question, index) {
        return (
            <View style={{ flex: 1, flexDirection: 'column' }} key={index}>
                <View style={{ marginTop: 20, marginBottom: 20, height: 1, backgroundColor: Colors.lineColor }} />

                <TextView weight="bold" style={{ color: 'black', fontSize: styles.survey_desciption_size }}>{question.description}</TextView>
                {question.questionChoiceList.map((q, i) => {
                    return <TouchableOpacity style={{ flexDirection: 'row', padding: 5 }} key={i}
                        onPress={() => { //buradaki değişiklikleri checkbox onpress'de de yapmalısın.
                            this.setState({ singleChoiseAnswer: q })
                        }}>
                        <CheckBox
                            checkedIcon='dot-circle-o'
                            uncheckedIcon='circle-o'
                            onPress={() => {
                                this.setState({ singleChoiseAnswer: q })
                            }}
                            checked={this.state.singleChoiseAnswer === q}
                            containerStyle={{ backgroundColor: 'transparent', padding: 0, margin: 0 }}
                            checkedColor={Colors.primary} />
                        <TextView weight="bold" style={{ color: 'black', alignSelf: 'center', textAlign: 'center' }}>{q.text}</TextView>
                    </TouchableOpacity>
                })}
            </View>
        )
    }

    _renderMultiChoiseQuestion(question, index) {
        return (
            <View style={{ flex: 1, flexDirection: 'column' }} key={index}>
                <View style={{ marginTop: 20, marginBottom: 20, height: 1, backgroundColor: Colors.lineColor }} />

                <TextView weight="bold" style={{ color: 'black', fontSize: styles.survey_desciption_size }}>{question.description}</TextView>
                {question.questionChoiceList.map((q, i) => {
                    return <TouchableOpacity style={{ flexDirection: 'row', padding: 5 }} key={i}
                        onPress={() => { //buradaki değişiklikleri checkbox onpress'de de yapmalısın.
                            if (this.state.multiChoiseAnswer.indexOf(q) > -1) {
                                let filteredAnswer = this.state.multiChoiseAnswer.filter(answer => answer !== q)
                                this.setState({ multiChoiseAnswer: filteredAnswer })
                            } else {
                                this.setState({
                                    multiChoiseAnswer: [...this.state.multiChoiseAnswer, q]
                                })
                            }
                        }}>
                        <CheckBox
                            onPress={() => {
                                if (this.state.multiChoiseAnswer.indexOf(q) > -1) {
                                    let filteredAnswer = this.state.multiChoiseAnswer.filter(answer => answer !== q)
                                    this.setState({ multiChoiseAnswer: filteredAnswer })
                                } else {
                                    this.setState({
                                        multiChoiseAnswer: [...this.state.multiChoiseAnswer, q]
                                    })
                                }

                            }}
                            checked={this.state.multiChoiseAnswer.indexOf(q) > -1}
                            containerStyle={{ backgroundColor: 'transparent', padding: 0, margin: 0 }}
                            checkedColor={Colors.primary} />
                        <TextView weight="bold" style={{ color: 'black', alignSelf: 'center', textAlign: 'center' }}>{q.text}</TextView>
                    </TouchableOpacity>
                })}
            </View>
        )
    }

    _renderSurveyTitle(question, index) {
        return (
            <View key={index}>
                <TextView weight="bold" style={{ fontSize: styles.survet_title_size, color: 'black' }}>{question.description}</TextView>
            </View>
        );
    }
    _renderSurveyParagraph(question, index) {
        return (
            <View style={{ marginTop: 5 }} key={index}>
                <TextView weight="regular" style={{ fontSize: styles.survey_paragraph_size, color: 'black' }}>{question.description}</TextView>
            </View>
        )
    }

    _renderCardHeader() {
        return (
            <View style={{ flexDirection: 'row', padding: 10 }}>
                <MaterialCommunityIcons style={{ flex: 0.1 }} name="format-list-bulleted" size={25} color="black" />
                <View style={{ flex: 0.8, flexDirection: 'column', marginStart: 5 }}>
                    <TextView numberOfLines={2} weight="bold" style={{ color: 'black', fontSize: 18 }}>{this.state.activity.name}</TextView>
                    {this._renderDueDate()}
                </View>
                {this.props.courses.enrollmentProgressData.find(data => data.activityId === this.state.activity.activityId)
                    ? <View style={{ flex: 0.1, alignItems: 'center', justifyContent: 'center', height: styles.marked_completed_activity_icon_size, width: 30, borderRadius: 30 / 2, backgroundColor: Colors.primary }}>
                        <Ionicons name="ios-checkmark" size={styles.marked_completed_activity_icon_size} color="white" />
                    </View>
                    :
                    null
                }
            </View>

        )
    }

    _renderCardContainer() {
        return (
            <View style={{ marginTop: 10, flexDirection: 'column' }}>
                {this._renderCardImage()}
                <View style={{ padding: 10 }}>
                    {this._renderDescription()}
                    {this._renderQuestions()}
                </View>
            </View>
        )
    }
    setAsCompleted() {
        this.setActivityAsCompletedApiRequestBody.classId = this.props.courseDetail.course.classId;
        this.props.setAsCompleted(this.setActivityAsCompletedApiRequestBody);
    }

    _renderActionButtons() {
        if (this.state.activity.completionType === Constants.ActivityCompletionTypes.View)
            return this.props.courses.enrollmentProgressData.find(data => data.activityId === this.state.activity.activityId) ? null : (
                <View style={{ flexDirection: 'row', alignContent: 'center' }}>
                    <View style={{ flex: 0.5 }} />
                    <TouchableOpacity
                        activeOpacity={0.7}
                        onPress={() => { this.setAsCompleted() }}
                        style={{ flexDirection: 'row', padding: 10, flex: 0.5, justifyContent: 'flex-end', alignItems: 'center' }} >
                        <Ionicons name="md-checkmark" size={25} color="black" style={{ marginEnd: 5 }} />
                        <TextView weight="bold" style={{ color: 'black', fontSize: 16 }}>
                            {this.props.main.languageResource.r_activity_completation_button_text || strings('r_activity_completation_button_text')}
                        </TextView>
                    </TouchableOpacity>
                </View>
            )
        else if (this.state.activity.completionType === Constants.ActivityCompletionTypes.Complete) {
            return this.props.courses.enrollmentProgressData.find(data => data.activityId === this.state.activity.activityId) ? null :
                this.state.moreThanOneQuestion
                    ? <View style={{ flexDirection: 'row', alignContent: 'center' }}>
                        <View style={{ flex: 0.5 }} />
                        <View style={{ flex: 0.5, padding: 5 }}>
                            <Button
                                icon={<Ionicons name="ios-eye" size={25} color="white" />}
                                onPress={() => this.openSurveyDetail()}
                                title={this.props.main.languageResource.r_activity_show_btn_text || strings('r_activity_show_btn_text')}
                                titleStyle={{ marginStart: 5 }}
                                buttonStyle={{ backgroundColor: Colors.primary }} />
                        </View>

                    </View>
                    : <View style={{ flexDirection: 'row', alignContent: 'center' }}>
                        <View style={{ flex: 0.5 }} />
                        <TouchableOpacity
                            activeOpacity={0.7}
                            onPress={() => { this.openSurveyDetail() }} //FIXME: anket cevaplama apis geldiğinde burasının değişmesi gerek!
                            style={{ flexDirection: 'row', padding: 10, flex: 0.5, justifyContent: 'flex-end', alignItems: 'center' }} >
                            <Ionicons name="md-checkmark" size={25} color="black" style={{ marginEnd: 5 }} />
                            <TextView weight="bold" style={{ color: 'black', fontSize: 16 }}>
                                {this.props.main.languageResource.r_survey_anwer_btn_text || strings('r_survey_anwer_btn_text')}
                            </TextView>
                        </TouchableOpacity>
                    </View>

        } else return null
    }
    render() {
        return (
            <View>
                <ScrollView>
                    <Card containerStyle={{ backgroundColor: 'white', flexDirection: 'column', padding: 0, margin: 5 }}>
                        <View>
                            {/* Card Header */}
                            {this._renderCardHeader()}
                            {/* Card Container */}
                            {this._renderCardContainer()}

                            <View style={{ marginTop: 20, height: 1, backgroundColor: Colors.lineColor }} />
                            {/* Card Action Button */}
                            {this._renderActionButtons()}
                        </View>
                    </Card>
                </ScrollView>
            </View>
        );
    }
}


const mapDispatchToProps = (dispatch) => {
    return {
        setAsCompleted: (...args) => dispatch(ActivityInteractActions.activityCompletionViewCriteriaRequest(...args)),
        getEnrollmentProgress: (...args) => dispatch(CoursesActions.getEnrollmentProgressRequest(...args)),

    }
}

const mapStateToProps = (state) => {
    return {
        main: state.main,
        courseDetail: state.courseDetail,
        activityInteract: state.activityInteract,
        courses: state.courses
    }
}
export default connect(mapStateToProps, mapDispatchToProps)(ActivitySurveyType)