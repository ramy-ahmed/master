import React from 'react';
import { View, StyleSheet } from 'react-native';
import TextView from '../TextView';
import HTML from 'react-native-render-html';
import HTMLView from 'react-native-htmlview';
import Colors from '../../theme/Colors';
import FontSize from '../../theme/FontSize';

export default class AnnouncementListItem extends React.Component {
    constructor(props) {
        super(props)

        this.state = ({
            announcement: props.announcement
        });
    }

    render() {
        return (
            <View style={style.announcementMain}>
                <TextView weight="bold" style={style.announcementSubject}>{this.state.announcement.subject}</TextView>
                <View style={{ flexDirection: 'column', flex: 1 }}>
                    <TextView weight="bold" style={style.announcementDisplayName}>{this.state.announcement.fromDisplayName + ": "} </TextView>
                    <HTMLView value={this.state.announcement.body} textComponentProps={{ numberOfLines: 1 }} addLineBreaks={false} />
                </View>
            </View>
        );
    }

}

const style = StyleSheet.create({
    announcementMain: {
        marginTop: 2,
        paddingLeft: 10,
        paddingRight: 10,
        paddingTop: 15,
        paddingBottom: 15,
        flexDirection: 'column',
        backgroundColor: Colors.annouceement_item_background
    },
    announcementSubject: {
        color: Colors.announcement_item_title,
        marginTop: 5,
        marginBottom: 5,
        fontSize: FontSize.announcement_item_title
    },
    announcementDisplayName: {
        color: Colors.announcement_display_name,
        fontSize: FontSize.announcement_display_name
    }
})