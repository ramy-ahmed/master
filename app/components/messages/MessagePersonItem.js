import React from 'react';
import { View, Image } from 'react-native';
import TextView from '../TextView';
import Ionicons from 'react-native-vector-icons/Ionicons';
import HTML from 'react-native-render-html'
import Colors from '../../theme/Colors';
import Constants from '../../services/Constants';

export default class MessagePersonItem extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            // image: { uri: this.props.message.profileUrl }
            image: require('../../assets/images/avatar.png')
        }

    }

    onErrorImageLoad() {
        this.setState({
            image: require('../../assets/images/avatar.png')
        })
    }

    render() {
        return (
            <View style={{ flexDirection: 'column' }}>
                <View style={{ flexDirection: 'row', margin: 5 }}>
                    <View style={{ flex: 0.2, alignItems: 'center' }}>
                        <Image source={this.state.image} onError={this.onErrorImageLoad.bind(this)} style={{ height: 40, width: 40, resizeMode: 'contain' }} />
                    </View>

                    <View style={{ flex: 0.7, justifyContent: 'center', marginStart: 5 }}>
                        <TextView style={{ color: 'black', fontSize: 17 }} weight="bold">{this.props.message.groupName}</TextView>
                        <HTML html={this.props.message.lastMessage} />
                    </View>
                    {this.props.message.status === Constants.MessagesStatusType.UnRead
                        ? <View style={{ flex: 0.1, justifyContent: 'center', alignItems: 'center' }}>
                            <View style={{
                                width: 7,
                                height: 7,
                                borderRadius: 7 / 2,
                                backgroundColor: Colors.primary
                            }} />
                        </View>
                        : null
                    }

                </View>
                <View style={{ height: 1, backgroundColor: Colors.background }} />
            </View>
        )
    }
}