import React from 'react';
import { View, Image } from 'react-native';
import TextView from '../TextView';

import Ionicons from 'react-native-vector-icons/Ionicons';
import HTML from 'react-native-render-html';

import FontSize from '../../theme/FontSize';
import Colors from '../../theme/Colors';
import styles from '../../theme/Style';
import Moment from 'moment';
export default class MessageSenderMe extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            // image: { uri: this.props.message.profileUrl }
            image: require('../../assets/images/avatar.png')
        }
    }

    onErrorImageLoad() {
        this.setState({
            image: require('../../assets/images/avatar.png')
        })
    }
    render() {
        return (
            <View style={{ margin: 5 }}>
                <View style={{ flexDirection: 'row', alignSelf: 'flex-end', margin: 5, alignItems: 'center' }}>
                    <Image source={this.state.image} onError={this.onErrorImageLoad.bind(this)} style={{ height: 30, width: 30, resizeMode: 'contain' }} />
                    <TextView style={{ color: Colors.message_item_text_color, margin: 5, fontSize: FontSize.message_sender_text }} weight="bold">
                        {this.props.message.senderName}
                    </TextView>
                    <TextView style={{ margin: 5, fontSize: 12 }}>
                        {Moment(this.props.message.date).format('hh:mm')}
                    </TextView>
                </View>

                <View style={{ backgroundColor: Colors.messages_item_me_background_color, padding: 10, alignSelf: 'flex-end', borderBottomLeftRadius: styles.message_item_border_radius, borderBottomRightRadius: styles.message_item_border_radius, borderTopLeftRadius: styles.message_item_border_radius }}>
                    <HTML html={this.props.message.message} />
                </View>
            </View>
        )
    }
}