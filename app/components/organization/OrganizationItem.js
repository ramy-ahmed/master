import React from 'react';
import { View, Text } from 'react-native';
export default class OrganizationItem extends React.Component {
    constructor(props) {
        super(props);
        this.state = ({
            organization: props.organization
        })
    }
    render() {
        return (
            <View style={{ padding: 10 }}>
                <Text>
                    {this.state.organization.name}
                </Text>
            </View>
        );

    }

}