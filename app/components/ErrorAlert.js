
import { Alert } from 'react-native';
import { strings } from '../locales/i18n';
import AsyncStorage from '@react-native-community/async-storage';
import { NavigationActions } from 'react-navigation';


export const ErrorAlert = (error, navigation, spesificMessage = "") => {

    switch (error.code) {
        case 700:
            Alert.alert('', "Hata kodu: " + error.code + " " + error.text,
                [
                    {
                        text: 'Geri', onPress: () => {
                            //TODO: bir önceki ekrana dönmenin yolunu bul
                            //                     const backActions = NavigationActions.back({
                            //                         key: null
                            //                     });
                            //                     navigation.dispatch(backActions)

                        }
                    },
                ], {
                    cancelable: false
                }
            )
            break;

        case 600:
            Alert.alert(strings('error.error_title'), "Hata Kodu: " + error.code + "" + spesificMessage, [
                { text: strings('common.ok') },
            ]);
            break;
        case 401:
            Alert.alert('Uyarı', strings('error.session_failed'), [
                {
                    text: strings('common.ok'), onPress: () => {
                        AsyncStorage.removeItem('auth');
                        AsyncStorage.setItem('isLogin', 'false').then(() => {
                            navigation.navigate('SignedOut')
                        })

                    }
                },
            ], {
                    cancelable: false
                });
            break;
        default:
            Alert.alert(strings('error.error_title'), 'Unknown error', [
                { text: strings('common.ok') },
            ]);
    }
}
