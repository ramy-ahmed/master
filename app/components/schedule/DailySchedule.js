import React from 'react';
import { View, Text, FlatList, TouchableOpacity } from 'react-native';
import { Card } from 'react-native-elements';
import TextView from '../TextView';
import Moment from 'moment';
import AsyncStorage from '@react-native-community/async-storage';
import LocalStorageConstants from '../../local/LocalStorageConstants';
import Colors from '../../theme/Colors';
import FontSize from '../../theme/FontSize';


class DailySchedule extends React.Component {
    constructor(props) {
        super(props)

        this.state = ({
            schedule: props.schedule,
            dailyCalenderData: props.dailyCalenderData,
            languageCode: '',
        })
    }

    async componentDidMount() {
        const lang = await AsyncStorage.getItem(LocalStorageConstants.Language)
        this.setState({ languageCode: lang })

    }

    keyExtractor = (item, index) => index.toString()

    _onItemClick(item) {

    }
    _renderTime() {
        if (this.state.languageCode === LocalStorageConstants.LanguageTrResource) {
            return (
                <TextView style={{ fontSize: 13 }} weight="regular">{Moment(this.state.schedule.startDate).format('HH:mm') + " - " + Moment(this.state.schedule.endDate).format('HH:mm')}</TextView>
            )
        } else {
            return (
                <TextView style={{ fontSize: 13 }} weight="regular">{Moment(this.state.schedule.startDate).format('hh:mm') + " - " + Moment(this.state.schedule.endDate).format('hh:mm')}</TextView>
            )
        }

    }
    render() {
        return (
            <View style={{ flexDirection: 'row' }}>
                <View style={{ borderColor: '#b0afac', borderWidth: 2 }} />
                <View style={{ flexDirection: 'column', marginStart: 10, marginTop: 5, marginBottom: 5, padding: 5 }}>
                    {this._renderTime()}
                    <TextView style={{ fontSize: 17, color: '#323231' }} weight="bold">
                        {this.state.schedule.courseName}
                    </TextView>
                    <View style={{ flexDirection: 'row' }}>
                        <TextView numberOfLines={1} style={{ fontSize: FontSize.course_daily_item_title, color: Colors.course_daily_activity_text }} weight="regular" ellipsizeMode="clip" >{this.state.schedule.activityName}</TextView>
                    </View>
                </View>
            </View>
        );
    }
}
export default DailySchedule