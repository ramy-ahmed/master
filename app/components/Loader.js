import React, { Component } from 'react';
import { StyleSheet, View, Text, Modal, ActivityIndicator } from 'react-native';

class Loader extends React.Component {
    constructor(props) {
        super(props)
    }
    render() {
        const progressText = () => {
            if (this.props.progress !== undefined) {
                return (<Text>% {this.props.progress}</Text>)
            } else { return null }
        }
        return (
            <Modal
                transparent={true}
                animationType={'none'}
                visible={this.props.loading}
                onRequestClose={() => { }}>
                <View style={styles.modalBackground}>
                    <View style={styles.activityIndicatorWrapper}>
                        <ActivityIndicator
                            animating={this.props.loading} />
                        {progressText()}
                    </View>
                </View>
            </Modal>
        );
    }
}
// const Loader = props => {
//     const {
//         loading,
//         progress,
//         ...attributes
//     } = props;

//     const progressText = () => {
//         if (progress !== undefined) {
//             return (<Text>% {progress}</Text>)
//         } else { return null }
//     }

//     return (
//         <Modal
//             transparent={true}
//             animationType={'none'}
//             visible={loading}
//             onRequestClose={() => { }}>
//             <View style={styles.modalBackground}>
//                 <View style={styles.activityIndicatorWrapper}>
//                     <ActivityIndicator
//                         animating={loading} />
//                     {progressText()}
//                 </View>
//             </View>
//         </Modal>
//     )
// }


const styles = StyleSheet.create({
    modalBackground: {
        flex: 1,
        alignItems: 'center',
        flexDirection: 'column',
        justifyContent: 'space-around',
        backgroundColor: '#00000040'
    },
    activityIndicatorWrapper: {
        backgroundColor: '#FFFFFF',
        height: 100,
        width: 100,
        borderRadius: 10,
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'space-around'
    }
});

export default Loader;