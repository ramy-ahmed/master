import React, { Component } from 'react';
import {
    StyleSheet,
    View,
    Modal,
    Dimensions,
    TouchableOpacity
} from 'react-native';
import TextView from './TextView';

const { width, height } = Dimensions.get('screen');
import { connect } from 'react-redux';
import { strings } from '../locales/i18n';

class DownloadAlert extends React.Component {
    constructor(props) {
        super(props)
    }

    onCancel = () => {
        this.props.onCancel()
    }
    onBackground = () => {
        this.props.onBackground()
    }
    render() {
        return (
            <Modal
                transparent={true}
                animationType={'none'}
                visible={this.props.downloading}
                onRequestClose={() => { }}>
                <View style={styles.modalBackground}>
                    <View style={{ backgroundColor: 'white', width: width * 0.6, borderRadius: 5, padding: 10 }}>
                        <TextView weight="bold" style={{ color: 'black', fontSize: 18 }}>
                            {this.props.main.languageResource.r_activity_video_downloading || strings('r_activity_video_downloading')}
                        </TextView>
                        <TextView weight="regular" style={{ color: 'black', fontSize: 17, marginTop: 5 }}>
                            %{this.props.percent}
                        </TextView>

                        <View style={{ marginTop: 10, flexDirection: 'row', justifyContent: 'space-between' }}>
                            <TouchableOpacity onPress={this.onCancel}>
                                <TextView weight="bold" style={{ color: 'black', fontSize: 16, padding: 10 }}>
                                    {this.props.main.languageResource.r_activity_video_downloading_cancel || strings('r_activity_video_downloading_cancel')}
                                </TextView>
                            </TouchableOpacity>
                            <TouchableOpacity onPress={this.onBackground}>
                                <TextView weight="bold" style={{ color: 'black', fontSize: 16, padding: 10 }}>
                                    {this.props.main.languageResource.r_activity_video_downloading_on_backgorund || strings('r_activity_video_downloading_on_backgorund')}
                                </TextView>
                            </TouchableOpacity>
                        </View>

                    </View>
                </View>
            </Modal>
        )
    }
}


const styles = StyleSheet.create({
    modalBackground: {
        flex: 1,
        alignItems: 'center',
        flexDirection: 'column',
        justifyContent: 'space-around',
        backgroundColor: '#00000040'
    },
});

const mapDispatchToProps = (dispatch) => {
    return {}
}

const mapStateToProps = (state) => {
    return {
        main: state.main,
    }
}
export default connect(mapStateToProps, mapDispatchToProps)(DownloadAlert)