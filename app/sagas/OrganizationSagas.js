import { call, put } from 'redux-saga/effects';
import OrganizationActions from '../redux/OrganizationRedux';
//FIXME: dont use i18n
import { strings } from '../locales/i18n';

export function* getOrganizations(api, action) {
    try {
        const response = yield call(api.getOrganizations)
        if (response.success) {
            yield put(OrganizationActions.getOrganizationSuccess(response.data))
        } else {
            yield put(OrganizationActions.getOrganizationFailure(strings('error.api_error')))
        }

    } catch (error) {
        yield put(OrganizationActions.getOrganizationFailure(strings('error.connection_error') + " " + error.message))
    }
}