import React from 'react';
import { View, Text, TouchableOpacity, StyleSheet, SafeAreaView } from 'react-native';
import AsyncStorage from '@react-native-community/async-storage';
import Colors from '../../theme/Colors';
import DeviceInfo from 'react-native-device-info';
import TextView from '../../components/TextView';

var PJson = require('../../../package.json')
import { connect } from 'react-redux';
import MainActions from '../../redux/MainRedux';
import LocalStorageConstants from '../../local/LocalStorageConstants';
import Ionicons from 'react-native-vector-icons/Ionicons';
import FontAwesome from 'react-native-vector-icons/FontAwesome';
import ActionSheet from 'react-native-actionsheet';
import Constants from '../../services/Constants';
import { strings } from '../../locales/i18n';
import FontSize from '../../theme/FontSize';

import I18n from 'react-native-i18n';


class DrawerMenu extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            menuItems: props.menuItems,
            nav: props.nav,
            arrayStack: [],
            languageOptions: [Constants.SupportedLanguage.TR, Constants.SupportedLanguage.EN, this.props.main.languageResource.r_menu_change_language_close || strings('r_menu_change_language_close')],
            languageIndex: null,
            selecteLanguageIndex: null,
        }

        if (DeviceInfo.isTablet()) {
            for (let i = 4; i < this.state.menuItems.length; i++) {
                this.state.arrayStack.push(this.state.menuItems[i]);
            }
        } else {
            for (let i = 4; i < this.state.menuItems.length; i++) {
                this.state.arrayStack.push(this.state.menuItems[i]);
            }
        }

        this.languageResourceApiRequestBody = {
            remote: true,
            version: LocalStorageConstants.LanguageVersion,
            resourceType: null,
            almsPlusApiUrl: this.props.main.selectedOrganization.almsPlusApiUrl,
        }


    }

    async componentDidMount() {
        const langIndex = await AsyncStorage.getItem(LocalStorageConstants.Language);
        this.setState({ languageIndex: langIndex })
    }

    async _logout() {
        await AsyncStorage.setItem("isLogin", "false");
        const saveUserName = await AsyncStorage.getItem(LocalStorageConstants.SavedUserName);
        await AsyncStorage.clear();
        await AsyncStorage.setItem(LocalStorageConstants.SavedUserName, saveUserName);
        this.props.userLogout()
        //this.props.navigation.navigate('SignedOut');
        this.state.nav('SignedOut')
    }

    menuClicked(item) {
        switch (item.name) {
            case 'r_menu_my_files':
                this.props.navigation.navigate('files')
                this.props.navigation.closeDrawer();
                break;
            case 'r_menu_profile':
                this.props.navigation.navigate('profile')
                this.props.navigation.closeDrawer();
                break;

            case 'r_menu_change_language':
                this.showWeekActionSheet();
                this.props.navigation.closeDrawer();
                break;
        }

    }

    _renderDrawerMenuItem(item) {
        switch (item.name) {
            case 'r_menu_my_files':
                return (
                    <View style={{ flexDirection: 'row', alignItems: 'center', padding: 15 }}>
                        <View style={{ height: 30, width: 30, alignItems: 'center', justifyContent: 'center' }}>
                            <Ionicons size={25} name="ios-attach" color={Colors.drawer_menu_item_icon_color} />
                        </View>

                        <TextView style={{ marginStart: 10, color: Colors.drawer_menu_item_color, fontSize: FontSize.drawer_menu_item_text }} weight="bold">
                            {this.props.main.languageResource.r_menu_my_files || strings('r_menu_my_files')}
                        </TextView>
                    </View>
                )
            case 'r_menu_profile':
                return (
                    <View style={{ flexDirection: 'row', alignItems: 'center', padding: 15, }}>
                        <View style={{ height: 30, width: 30, alignItems: 'center', justifyContent: 'center' }}>
                            <Ionicons size={25} name="ios-person" color={Colors.drawer_menu_item_icon_color} />
                        </View>

                        <TextView style={{ marginStart: 10, color: Colors.drawer_menu_item_color, fontSize: FontSize.drawer_menu_item_text }} weight="bold">
                            {this.props.main.languageResource.r_menu_profile || strings('r_menu_profile')}
                        </TextView>
                    </View>
                )
            case 'r_menu_change_language':
                return (
                    <View style={{ flexDirection: 'row', alignItems: 'center', padding: 15 }}>
                        <View style={{ height: 30, width: 30, alignItems: 'center', justifyContent: 'center' }}>
                            <FontAwesome size={25} name="language" color={Colors.drawer_menu_item_icon_color} />
                        </View>

                        <TextView style={{ marginStart: 10, color: Colors.drawer_menu_item_color, fontSize: FontSize.drawer_menu_item_text }} weight="bold">
                            {this.props.main.languageResource.r_menu_change_language || strings('r_menu_change_language')}
                        </TextView>
                    </View>
                )
            default:
                return null
        }
    }
    showWeekActionSheet = () => {
        this.ActionSheet.show()
    }


    render() {
        return (
            <SafeAreaView style={{ flex: 1 }}>
                <View style={styles.containerMain}>
                    <View>
                        {
                            this.state.arrayStack.map(item => (
                                <TouchableOpacity
                                    onPress={() => this.menuClicked(item)}
                                    key={item.name}
                                >
                                    {this._renderDrawerMenuItem(item)}
                                </TouchableOpacity>
                            ))
                        }
                    </View>

                    <View style={styles.bottomView}>
                        <TouchableOpacity onPress={() => this._logout()} style={{ marginBottom: 15, backgroundColor: '#fff', borderColor: Colors.primary, borderWidth: 1, borderRadius: 30, paddingEnd: 50, paddingStart: 50, paddingTop: 7, paddingBottom: 7 }}>
                            <Text style={{ color: Colors.drawer_menu_item_color, fontSize: 16 }}>{this.props.main.languageResource.r_fileUpload_logOut}</Text>
                        </TouchableOpacity>

                        <TextView weight="regular" style={{ fontSize: 11 }}>{this.props.main.languageResource.r_app_version || strings('r_app_version')} {PJson.version}</TextView>
                    </View>

                    <ActionSheet ref={o => this.ActionSheet = o}
                        options={this.state.languageOptions}
                        cancelButtonIndex={this.state.languageOptions.length - 1}
                        onPress={(index) => {
                            if (this.state.languageOptions[index] === Constants.SupportedLanguage.EN) {
                                if (this.state.languageIndex !== LocalStorageConstants.LanguageEnResource) {
                                    console.log("call api again")
                                    this.languageResourceApiRequestBody.resourceType = LocalStorageConstants.LanguageEnResource
                                    this.props.getLanguageResourceRequest(this.languageResourceApiRequestBody)
                                    this.setState({ languageIndex: LocalStorageConstants.LanguageEnResource })
                                    AsyncStorage.setItem(LocalStorageConstants.Language, LocalStorageConstants.LanguageEnResource)
                                    I18n.locale = "en"
                                }
                            }
                            else if (this.state.languageOptions[index] === Constants.SupportedLanguage.TR) {
                                if (this.state.languageIndex !== LocalStorageConstants.LanguageTrResource) {
                                    console.log("call api again")
                                    this.languageResourceApiRequestBody.resourceType = LocalStorageConstants.LanguageTrResource
                                    this.props.getLanguageResourceRequest(this.languageResourceApiRequestBody)
                                    this.setState({ languageIndex: LocalStorageConstants.LanguageTrResource })
                                    AsyncStorage.setItem(LocalStorageConstants.Language, LocalStorageConstants.LanguageTrResource)
                                    I18n.locale = "tr"
                                }
                            } else {

                            }

                        }}
                    />
                </View>
            </SafeAreaView>
        );
    }
}
const mapDispatchToProps = (dispatch) => {
    return {
        userLogout: () => dispatch(MainActions.clearRedux()),
        getLanguageResourceRequest: (...args) => dispatch(MainActions.getLanguageResourceRequest(...args)),

    }
}

const mapStateToProps = (state) => {
    return {
        main: state.main
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(DrawerMenu)
const styles = StyleSheet.create({
    containerMain: {
        flex: 1,
    },
    bottomView: {
        width: '100%',
        height: 100,
        justifyContent: 'center',
        alignItems: 'center',
        position: 'absolute',
        bottom: 0,
    },
});