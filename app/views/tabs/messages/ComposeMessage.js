
import React from 'react';
import { View, Text, SafeAreaView, TextInput, FlatList, TouchableOpacity, StyleSheet, Alert, Platform, KeyboardAvoidingView, ActivityIndicator, Image } from 'react-native';
import Colors from '../../../theme/Colors';
import Ionicons from 'react-native-vector-icons/Ionicons';
import { NavigationActions } from 'react-navigation';


import { connect } from 'react-redux';
import { strings } from '../../../locales/i18n';

import MessagesActions from '../../../redux/MessagesRedux';
import Constants from '../../../services/Constants';
import TextView from '../../../components/TextView';

import MessageSenderMe from '../../../components/messages/MessageSenderMe';
import MessageSenderSomeone from '../../../components/messages/MessageSenderSomeone';
import { createGuid } from '../../../helpers/Calculate';
import Loader from '../../../components/Loader';
import styles from '../../../theme/Style';

import NetInfo from '@react-native-community/netinfo';


class ComposeMessages extends React.Component {

    static navigationOptions = ({ navigation }) => {
        const { params = {} } = navigation.state;

        return {
            title: params.languageResource !== undefined ? params.languageResource.r_message_create_new_message || strings('r_message_create_new_message') : strings('r_message_create_new_message'),
            headerTitleStyle: {
                alignSelf: 'center'
            },
            headerLeft: (
                <View style={{ flex: 1, flexDirection: 'row', alignItems: 'center' }}>
                    <Ionicons style={{ paddingLeft: 20, paddingRight: 20, color: 'black' }} name="ios-arrow-back" size={25} onPress={() => {
                        const backActions = NavigationActions.back({
                            key: null
                        });
                        navigation.dispatch(backActions)
                    }} />
                </View>
            ),
        }
    }

    constructor(props) {
        super(props)

        this.state = {
            searchText: '',
            selectedTarget: null,
            messageBody: '',
            scrolled: false,
            firstMessageSent: false,
            isNewMessage: false,
        }

        this.myClassMatesApiRequestBody = {
            almsPlusApiUrl: this.props.main.selectedOrganization.almsPlusApiUrl,
            accessToken: this.props.main.authData.access_token,
            courseId: null,
            skip: 0,
            take: Constants.ApiResponseQuantity,
        }

        this.searchApiReqeustBody = {
            almsPlusApiUrl: this.props.main.selectedOrganization.almsPlusApiUrl,
            accessToken: this.props.main.authData.access_token,
            skip: 0,
            take: Constants.ApiResponseQuantity,
            searchKey: null,
            getTypes: 'Class,Course,User,Program,Group'
        }

        this.sendMessageApiRequestBody = {
            almsPlusApiUrl: this.props.main.selectedOrganization.almsPlusApiUrl,
            accessToken: this.props.main.authData.access_token,
            body: null,
            subject: null,
            originMessageId: null,
            parentMessageId: null,
            contextList: {
                targetType: Constants.MessagesSendTargetTypes.User,
                contextId: this.state.firstItemUserId,
            },
            fileUploadId: createGuid(),
        }

        this.messageDetailApiRequestBody = {
            remote: true,
            almsPlusApiUrl: this.props.main.selectedOrganization.almsPlusApiUrl,
            accessToken: this.props.main.authData.access_token,
            messageId: null,
            take: Constants.MessageApiResponseQuantity,
            skip: 0,
        }

        this.getMessagesGroupApiRequestBody = {
            remote: true,
            accessToken: this.props.main.authData.access_token,
            almsPlusApiUrl: this.props.main.selectedOrganization.almsPlusApiUrl,
            take: Constants.ApiResponseQuantity,
            skip: 0
        }
    }


    componentDidMount() {
        this.props.navigation.setParams({
            languageResource: this.props.main.languageResource
        })

        this.classMatesApiCall();

    }

    componentWillReceiveProps(props) {
        if (this.props.messages.sendMessageData !== props.messages.sendMessageData) {
            if (this.messageDetailApiRequestBody.messageId === null) {
                this.messageDetailApiRequestBody.messageId = props.messages.sendMessageData
                this.sendMessageApiRequestBody.originMessageId = props.messages.sendMessageData
            }

            this.props.getMessageDetail(this.messageDetailApiRequestBody)
            this.setState({ firstMessageSent: true, messageBody: '', isNewMessage: true })
        }

        if (this.props.messages.sendMessageError !== props.messages.sendMessageError && props.messages.sendMessageError) {
            Alert.alert('', this.props.main.languageResource.r_message_send_error || strings('r_message_send_error'))
        }
    }
    messageDetailApiCall() {
        this.props.getMessageDetail(this.messageDetailApiRequestBody)
    }
    classMatesApiCall() {
        this.setState({ scrolled: false })
        if (this.props.main.userIdentity.userType === Constants.UserTypes.Student)
            this.props.getMyClassMates(this.myClassMatesApiRequestBody)
    }

    componentWillUnmount() {
        this.props.clearSearch();
        this.props.clearClassMates();
        this.props.clearMessageDetail();
        if (this.state.isNewMessage) {
            this.messageGroupApiCall();
        }
    }

    messageGroupApiCall() {
        this.setState({ scrolled: false })
        NetInfo.fetch().then(statu => {
            if (statu.isConnected) this.getMessagesGroupApiRequestBody.remote = true; else this.getMessagesGroupApiRequestBody.remote = false
            this.props.clearMessageGroups()
            this.props.getMessagesGroups(this.getMessagesGroupApiRequestBody);
        })
    }

    searchText(text) {
        this.setState({ searchText: text })
        this.searchApiReqeustBody.searchKey = text
        if (text.length > 2) {
            this.props.searchRequest(this.searchApiReqeustBody)
        } else {
            this.props.clearSearch();
        }
    }
    _renderMessageTo() {
        return (
            <TextInput
                style={{ fontSize: 16, height: 60, borderColor: Colors.background, borderWidth: 1, marginTop: 5, paddingLeft: 10, paddingRight: 10 }}
                placeholder={this.props.main.languageResource.r_search_input_text || strings('r_search_input_text')}
                placeholderTextColor={Colors.tintColors}
                onChangeText={(text) => {
                    this.searchText(text)
                }} />
        )
    }

    onReceiverClick(item) {
        this.setState({ selectedTarget: item })
        this.sendMessageApiRequestBody.contextList.contextId = item.id
        switch (item.type) {
            case Constants.MessageReceiverTypes.User:
                this.sendMessageApiRequestBody.contextList.targetType = Constants.MessagesSendTargetTypes.User
                break
            case Constants.MessageReceiverTypes.Class:
                this.sendMessageApiRequestBody.contextList.targetType = Constants.MessagesSendTargetTypes.Class
                break
            case Constants.MessageReceiverTypes.Program:
                this.sendMessageApiRequestBody.contextList.targetType = Constants.MessagesSendTargetTypes.Program
                break
        }
        this.props.clearSearch();
    }
    onStudentMessageReceiverClick(item) {
        this.setState({ selectedTarget: item })
        this.sendMessageApiRequestBody.contextList.contextId = item.userId;
        this.sendMessageApiRequestBody.contextList.targetType = Constants.MessagesSendTargetTypes.User
    }
    keyExtractor = (item, index) => item.id
    _renderMessageToFlatList() {
        return (
            <FlatList
                keyboardShouldPersistTaps="handled"
                keyExtractor={this.keyExtractor}
                data={this.props.messages.searchData}
                renderItem={({ item }) => (
                    <TouchableOpacity activeOpacity={0.7} style={{ flexDirection: 'column' }}
                        onPress={() => this.onReceiverClick(item)}>
                        <View>
                            <TextView style={{ padding: 10, color: 'black', fontSize: 15 }} weight="bold">
                                {item.value}
                            </TextView>
                        </View>
                    </TouchableOpacity>
                )} />
        )
    }

    _onMessageItemClick(item) {

    }
    keyExtractorMessageList = (item, index) => item.messageId
    _renderMessageList() {
        return (
            <FlatList
                keyExtractor={this.keyExtractorMessageList}
                ref={ref => (this.flatListRef = ref)}
                contentContainerStyle={{ flex: 1, flexDirection: 'column',  justifyContent: 'flex-end' }}
                data={this.props.messages.getMessageDetailData}
                renderItem={({ item }) => {
                    return (
                        <TouchableOpacity activeOpacity={0.7}
                            onPress={() => this._onMessageItemClick(item)}>
                            {item.userId === this.props.main.userIdentity.userId
                                ? <MessageSenderMe message={item} />
                                : <MessageSenderSomeone message={item} />}
                        </TouchableOpacity>

                    )
                }}

            />
        )
    }
    sendMessage() {
        console.log("sending data: ", this.sendMessageApiRequestBody);
        this.props.sendMessageData(this.sendMessageApiRequestBody);
    }
    _renderTypeContent() {
        return (
            <View style={{ flexDirection: 'row' }}>
                <TextInput multiline={true} placeholder={this.props.main.languageResource.r_message_send_message_input || strings('r_message_send_message_input')}
                    style={{ flex: 0.8, paddingLeft: 10 }}
                    onChangeText={(text) => {
                        this.sendMessageApiRequestBody.body = text
                        this.setState({ messageBody: text })
                    }}>
                    {this.state.messageBody}
                </TextInput>
                <TouchableOpacity onPress={() => this.sendMessage()}
                    style={{ flex: 0.2, alignItems: 'center', justifyContent: 'center', height: '100%' }} >
                    {this.props.messages.sendMessageSending
                        ? <View style={style.activityIndicatorWrapper}><ActivityIndicator animating={this.props.messages.sendMessageSending} /></View>
                        : <Ionicons name="md-send" size={25} />}
                </TouchableOpacity>

            </View>
        )
    }


    renderMessageInputs() {
        return (
            <View style={{ flex: 1 }}>
                {!this.state.firstMessageSent
                    ? <View>
                        <View style={{ flexDirection: 'row', paddingTop: 20, paddingBottom: 20, paddingStart: 10, paddingEnd: 10 }}>
                            <TextView style={{ flex: 0.9, color: 'black', fontSize: 17 }} weight="bold">
                                {this.state.selectedTarget.value || this.state.selectedTarget.nameSurname}
                            </TextView>
                            <TouchableOpacity style={{ justifyContent: 'center', alignItems: 'center', flex: 0.1 }} onPress={() => this.setState({ selectedTarget: null })}>
                                <Ionicons size={25} name="ios-close" color="black" />
                            </TouchableOpacity>
                        </View>

                        <View style={{ height: 1, backgroundColor: Colors.background }} />
                    </View>
                    : null}
                <View style={{ flex: 1 }}>
                    <View style={{ flex: 1, marginBottom: styles.typeBoxHeight }}>
                        {this._renderMessageList()}
                    </View>
                    <View style={style.bottomView}>
                        {this._renderTypeContent()}
                    </View>
                </View>
            </View>
        )
    }
    _renderMoreMates() {
        console.log("_renderMoreMates");
        if (this.state.scrolled && (this.props.messages.myClassMatesData.length % Constants.ApiResponseQuantity === 0)) {
            console.log("Load more item")
            this.myClassMatesApiRequestBody.skip = this.myClassMatesApiRequestBody.skip + Constants.ApiResponseQuantity;
            this.classMatesApiCall()
        }
    }

    _renderClassMates() {
        return (
            <View>
                <TextView style={{ margin: 10, color: 'black', fontSize: 17 }} weight="bold">
                    {this.props.main.languageResource.r_send_message_class_mates_title || strings('r_send_message_class_mates_title')}
                </TextView>
                <View style={{ height: 1, backgroundColor: Colors.lineColor, marginStart: 10, marginEnd: 10 }} />
            </View>
        )
    }
    keyExtractorClassMates = (item, index) => item.userId

    //TODO: uri
    _renderClassMatesList() {
        return (
            <View style={{ flex: 1 }}>
                <TextView style={{ margin: 10, color: 'black', fontSize: 17 }} weight="bold">
                    {this.props.main.languageResource.r_send_message_class_mates_title || strings('r_send_message_class_mates_title')}
                </TextView>
                <View style={{ height: 1, backgroundColor: Colors.lineColor, marginStart: 10, marginEnd: 10 }} />
                <FlatList
                    keyExtractor={this.keyExtractorClassMates}
                    ref={ref => (this.flatListRef = ref)}
                    data={this.props.messages.myClassMatesData}
                    onEndReached={this._renderMoreMates()}
                    onEndReachedThreshold={0.3}
                    initialNumToRender={Constants.ApiResponseQuantity}
                    style={{ flex: 0 }}
                    renderItem={({ item }) => {
                        return (
                            <TouchableOpacity style={{ margin: 10 }} onPress={() => this.onStudentMessageReceiverClick(item)}>
                                <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                                    <Image source={{ uri: 'https://icon-library.net/images/avatar-icon-images/avatar-icon-images-4.jpg' }}
                                        style={{ width: 40, height: 40, borderRadius: 40 / 2 }} />
                                    <TextView weight="bold" style={{ color: 'black', fontSize: 16, marginStart: 10 }}>
                                        {item.nameSurname}
                                    </TextView>
                                </View>
                            </TouchableOpacity>
                        )
                    }}
                    onMomentumScrollBegin={() => {
                        this.setState({ scrolled: true })
                    }}

                />
            </View>

        )
    }
    _renderContent() {
        if (this.state.selectedTarget === null) {
            return (
                this.props.main.userIdentity.userType === Constants.UserTypes.Student
                    ? this._renderClassMatesList()
                    : <View style={{ flex: 1 }}>
                        {this._renderMessageTo()}
                        {this._renderMessageToFlatList()}
                    </View>
            )
        } else {
            return (
                this.renderMessageInputs()
            )
        }
    }
    render() {
        return (
            <KeyboardAvoidingView style={{ flex: 1 }} behavior={Platform.OS === 'ios' ? 'padding' : null} keyboardVerticalOffset={Platform.OS === 'ios' ? 90 : 0}>
                <SafeAreaView style={{ flex: 1 }}>
                    <Loader loading={this.props.messages.myClassMatesFetching} />
                    {this._renderContent()}
                </SafeAreaView>
            </KeyboardAvoidingView>
        );
    }
}

const style = StyleSheet.create({
    bottomView: {
        width: '100%',
        height: styles.typeBoxHeight,
        // position: 'absolute',
        // bottom: 0,
        backgroundColor: Colors.background,
    },
    activityIndicatorWrapper: {
        backgroundColor: Colors.background,
        height: 25,
        width: 25,
        borderRadius: 10,
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'space-around'
    }
});


const mapDispatchToProps = (dispatch) => {
    return {
        getMyClassMates: (...args) => dispatch(MessagesActions.getMyClassMatesRequest(...args)),
        sendMessageData: (...args) => dispatch(MessagesActions.sendMessageRequest(...args)),
        searchRequest: (...args) => dispatch(MessagesActions.searchRequest(...args)),
        clearSearch: (...args) => dispatch(MessagesActions.clearSearch(...args)),
        getMessageDetail: (...args) => dispatch(MessagesActions.getMessageDetailRequest(...args)),
        clearClassMates: () => dispatch(MessagesActions.clearClassMates()),
        clearMessageDetail: () => dispatch(MessagesActions.clearMessageDetail()),
        clearMessageGroups: (...args) => dispatch(MessagesActions.clearMessageGroups(...args)),
        getMessagesGroups: (...args) => dispatch(MessagesActions.getMessagesGroupsRequest(...args)),
    }
}

const mapStateToProps = (state) => {
    return {
        messages: state.messages,
        main: state.main
    }
}
export default connect(mapStateToProps, mapDispatchToProps)(ComposeMessages)