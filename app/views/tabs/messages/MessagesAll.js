import React from 'react';
import { View, Text, SafeAreaView, FlatList, TouchableOpacity, RefreshControl } from 'react-native';
import PageEmpty from '../../../components/courses/PageEmpty'

import { connect } from 'react-redux';
import { strings } from '../../../locales/i18n';
import MessagesActions from '../../../redux/MessagesRedux';
import Loader from '../../../components/Loader';
import Constants from '../../../services/Constants';

import MessageGroupItem from '../../../components/messages/MessageGroupItem';
import MessagePersonItem from '../../../components/messages/MessagePersonItem';

import NetInfo from '@react-native-community/netinfo';


class MessagesAll extends React.Component {
    constructor(props) {
        super(props)

        this.state = {
            refreshing: false,
            scrolled: false,
        }

        this.getMessagesGroupApiRequestBody = {
            remote: true,
            accessToken: this.props.main.authData.access_token,
            almsPlusApiUrl: this.props.main.selectedOrganization.almsPlusApiUrl,
            take: Constants.ApiResponseQuantity,
            skip: 0
        }
    }

    componentDidMount() {
        this.messageGroupApiCall();
    }

    messageGroupApiCall() {
        this.setState({ scrolled: false })
        NetInfo.fetch().then(statu => {
            if (statu.isConnected) this.getMessagesGroupApiRequestBody.remote = true; else this.getMessagesGroupApiRequestBody.remote = false
            this.props.getMessagesGroups(this.getMessagesGroupApiRequestBody);
        })
    }

    componentWillReceiveProps(props) {
        if (this.props.messages.messagesGroups !== props.messages.messagesGroups) {
            this.setState({ refreshing: false })
        }
    }


    _onMessageItemClick(message) {
        this.props.navigation.navigate('MessageDetail', { message: message })
    }

    _onRefresh() {
        this.setState({ refreshing: true })
        this.getMessagesGroupApiRequestBody.skip = 0;
        this.messageGroupApiCall();
        this.props.clearMessageGroups()
    }

    _renderMoreItem() {
        console.log("_renderMoreItem");
        if (this.state.scrolled && (this.props.messages.messagesGroups.length % Constants.ApiResponseQuantity === 0)) {
            console.log("Load more item")
            this.getMessagesGroupApiRequestBody.skip = this.getMessagesGroupApiRequestBody.skip + Constants.ApiResponseQuantity;
            this.messageGroupApiCall()
        }
    }

    keyExtractor = (item, index) => item.messageId
    _renderMainContent() {
        if (this.props.messages.messagesGroups.length !== 0) {
            return (
                <FlatList
                    keyExtractor={this.keyExtractor}
                    data={this.props.messages.messagesGroups}
                    onEndReached={this._renderMoreItem()}
                    onEndReachedThreshold={0.3}
                    initialNumToRender={Constants.ApiResponseQuantity}
                    refreshControl={<RefreshControl refreshing={this.state.refreshing} onRefresh={() => this._onRefresh()} />}
                    renderItem={({ item }) => (
                        <TouchableOpacity activeOpacity={0.7} style={{ flexDirection: 'column' }}
                            onPress={() => this._onMessageItemClick(item)}>
                            {item.targetType === Constants.MessagesTargetTypes.ClassGroup
                                ? <MessageGroupItem message={item} />
                                : <MessagePersonItem message={item} />}
                        </TouchableOpacity>
                    )}
                    onMomentumScrollBegin={() => {
                        console.log("scrolled");
                        this.setState({
                            scrolled: true
                        })
                    }}
                />
            )
        } else if (this.props.messages.messagesGroups.length === 0 && !this.props.messages.messagesGroupsFetching) {
            return (<PageEmpty text={this.props.main.languageResource.r_messages_empty_all || strings('r_messages_empty_all')} />)
        } else return null
    }
    render() {
        return (
            <SafeAreaView style={{ flex: 1 }}>
                <Loader loading={this.props.messages.messagesGroupsFetching && !this.state.refreshing} />
                <View style={{ flex: 1 }}>
                    {this._renderMainContent()}
                </View>

            </SafeAreaView>
        );
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        getMessagesGroups: (...args) => dispatch(MessagesActions.getMessagesGroupsRequest(...args)),
        clearMessageGroups: (...args) => dispatch(MessagesActions.clearMessageGroups(...args)),
    }
}

const mapStateToProps = (state) => {
    return {
        messages: state.messages,
        main: state.main
    }
}
export default connect(mapStateToProps, mapDispatchToProps)(MessagesAll)