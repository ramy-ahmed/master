import React from 'react';
import { View, Text, SafeAreaView } from 'react-native';
// import MessagesRouter from '../../../navigation/MessagesRouter'
import { createAppContainer, createSwitchNavigator, createMaterialTopTabNavigator } from 'react-navigation';


import MessagesAll from '../messages/MessagesAll';
import MessagesPersonally from '../messages/MessagesPersonally';
import MessagesGroup from '../messages/MessagesGroup';
import { connect } from 'react-redux';
import Colors from '../../../theme/Colors';
import ActionButton from 'react-native-action-button';
import { strings } from '../../../locales/i18n';


const MessagesTabRouter = createMaterialTopTabNavigator({
    All: {
        screen: MessagesAll,
        navigationOptions: ({ navigation, screenProps }) => {
            return {
                tabBarLabel: screenProps.languageResource.r_messages_all || strings('r_messages_all')
            }

        }
    },
    Group: {
        screen: MessagesGroup,
        navigationOptions: ({ navigation, screenProps }) => {
            return {
                tabBarLabel: screenProps.languageResource.r_messages_group || strings('r_messages_group')
            }

        }
    },
    Personal: {
        screen: MessagesPersonally,
        navigationOptions: ({ navigation, screenProps }) => {
            return {
                tabBarLabel: screenProps.languageResource.r_messages_personally || strings('r_messages_personally')
            }

        }
    }
}, {
    tabBarPosition: 'top',
    swipeEnabled: true,
    animationEnabled: true,
    lazy: true,
    backBehavior: 'none',
    tabBarOptions: {

        activeTintColor: Colors.messages_tab_active_text_color,
        inactiveTintColor: Colors.messages_tab_inactive_text_color,
        scrollEnabled: false,
        style: {
            backgroundColor: Colors.messages_tab_background,
        },
        labelStyle: {
            textAlign: 'center',
        },
        indicatorStyle: {
            borderBottomColor: Colors.primary,
            borderBottomWidth: 2,
        },
    },
});

export const MessagesRouter = createAppContainer(createSwitchNavigator({
    messagesTabs: {
        screen: MessagesTabRouter
    }
}))

class MessagesMain extends React.Component {
    constructor(props) {
        super(props)
    }

    _composeNewMessage() {
        this.props.navigation.navigate('composeNewMessage')
    }
    _renderAddButton() {
        return (
            <ActionButton
                buttonColor={Colors.primary}
                offsetX={20}
                offsetY={20}
                onPress={() => this._composeNewMessage()} />
        )
    }
    static router = MessagesRouter.router;
    _renderTabs() {
        return <MessagesRouter navigation={this.props.navigation} screenProps={{ languageResource: this.props.main.languageResource }} />
    }

    render() {
        return (
            <SafeAreaView style={{ flex: 1 }}>
                <View style={{ flex: 1 }}>
                    {this._renderTabs()}
                    {this._renderAddButton()}
                </View>
            </SafeAreaView>
        );
    }
}

const mapDispatchToProps = (dispatch) => {
    return {}
}

const mapStateToProps = (state) => {
    return {
        main: state.main,
    }
}
export default connect(mapStateToProps, mapDispatchToProps)(MessagesMain)