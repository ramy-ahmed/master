import React from 'react';
import { View, Text, StyleSheet, TextInput, TouchableOpacity, FlatList, RefreshControl, ActivityIndicator, Platform, KeyboardAvoidingView, SafeAreaView, Dimensions } from 'react-native';
import { NavigationActions } from 'react-navigation';
import Ionicons from 'react-native-vector-icons/Ionicons';
import { connect } from 'react-redux';
import { strings } from '../../../locales/i18n';
import MessagesActions from '../../../redux/MessagesRedux';
import Constants from '../../../services/Constants';
import TextView from '../../../components/TextView';
import Colors from '../../../theme/Colors';
import NetInfo from '@react-native-community/netinfo';

import MessageSenderMe from '../../../components/messages/MessageSenderMe';
import MessageSenderSomeone from '../../../components/messages/MessageSenderSomeone';
import { createGuid } from '../../../helpers/Calculate';
import styles from '../../../theme/Style';

var { width, height } = Dimensions.get('window')

class MessageDetail extends React.Component {


    static navigationOptions = ({ navigation }) => {
        const { params = {} } = navigation.state;

        return {
            title: params.message.groupName,
            headerTitleStyle: {
                alignSelf: 'center'
            },
            headerLeft: (
                <View style={{ flex: 1, flexDirection: 'row', alignItems: 'center' }}>
                    <Ionicons style={{ paddingLeft: 20, paddingRight: 20, color: 'black' }} name="ios-arrow-back" size={25} onPress={() => {
                        const backActions = NavigationActions.back({
                            key: null
                        });
                        navigation.dispatch(backActions)
                    }} />
                </View>
            ),
        }
    }

    constructor(props) {
        super(props)

        this.state = {
            refreshing: false,
            scrolled: false,
            messageBody: "",
            isNewMessage: false,
        }
        this.messageDetailApiRequestBody = {
            remote: true,
            almsPlusApiUrl: this.props.main.selectedOrganization.almsPlusApiUrl,
            accessToken: this.props.main.authData.access_token,
            messageId: this.props.navigation.getParam('message', null).messageId,
            take: Constants.MessageApiResponseQuantity,
            skip: 0,
        }

        this.sendMessageApiRequestBody = {
            almsPlusApiUrl: this.props.main.selectedOrganization.almsPlusApiUrl,
            accessToken: this.props.main.authData.access_token,
            body: null,
            subject: null,
            originMessageId: this.props.navigation.getParam('message', null).messageId,
            parentMessageId: null,
            contextList: {
                targetType: Constants.MessagesSendTargetTypes.User,
                contextId: this.state.firstItemUserId,
            },
            fileUploadId: createGuid(),
        }

        this.getMessagesGroupApiRequestBody = {
            remote: true,
            accessToken: this.props.main.authData.access_token,
            almsPlusApiUrl: this.props.main.selectedOrganization.almsPlusApiUrl,
            take: Constants.ApiResponseQuantity,
            skip: 0
        }
    }


    componentDidMount() {
        this.messageDetailApiCall();
    }
    messageDetailApiCall() {
        this.setState({ scrolled: false })

        NetInfo.fetch().then(statu => {
            if (statu.isConnected) this.messageDetailApiRequestBody.remote = true; else this.messageDetailApiRequestBody.remote = false
            this.props.getMessageDetail(this.messageDetailApiRequestBody);
        });
    }
    sendMessage() {
        this.props.sendMessage(this.sendMessageApiRequestBody)
    }
    componentWillReceiveProps(props) {

        if (this.props.messages.getMessageDetailData !== props.messages.getMessageDetailData) {
        }

        if (this.props.messages.sendMessageData !== props.messages.sendMessageData) {
            console.log("message send successfully");
            this.setState({ messageBody: "", isNewMessage: true })
            this._onRefresh();

        }
    }


    componentWillUnmount() {
        this.props.clearMessageDetailState();

        if (this.state.isNewMessage) {
            this.messageGroupApiCall();
        }
    }

    messageGroupApiCall() {
        this.setState({ scrolled: false })
        NetInfo.fetch().then(statu => {
            if (statu.isConnected) this.getMessagesGroupApiRequestBody.remote = true; else this.getMessagesGroupApiRequestBody.remote = false
            this.props.clearMessageGroups()
            this.props.getMessagesGroups(this.getMessagesGroupApiRequestBody);
        })
    }

    keyExtractor = (item, index) => item.messageId

    _onRefresh() {
        this.setState({ refreshing: false })
        this.props.getMessageDetail(this.messageDetailApiRequestBody);
    }

    _renderMoreItem() {
        console.log("_renderMoreItem");
        if (this.state.scrolled && (this.props.messages.getMessageDetailData.length % Constants.MessageApiResponseQuantity === 0)) {
            console.log("Load more item")
            this.messageDetailApiRequestBody.skip = this.messageDetailApiRequestBody.skip + Constants.MessageApiResponseQuantity;
            this.messageDetailApiCall()
        }
    }
    _onMessageItemClick(item) {
    }

    _renderMessages() {
        var firstItem = null;
        return (
            <FlatList
                // onContentSizeChange={() => {
                //     this.flatListRef.scrollToEnd({ animated: true })
                // }}
                keyExtractor={this.keyExtractor}
                //ref={ref => (this.flatListRef = ref)}
                onEndReachedThreshold={0.3}
                contentContainerStyle={{ flex: 1, flexDirection: 'column',  justifyContent: 'flex-end' }}
                onEndReached={this._renderMoreItem()}
                initialNumToRender={Constants.MessageApiResponseQuantity}
                data={this.props.messages.getMessageDetailData}
                refreshControl={<RefreshControl refreshing={this.state.refreshing} onRefresh={() => this._onRefresh()} />}
                renderItem={({ item }) => {
                    if (firstItem === null) {
                        firstItem = item
                        this.sendMessageApiRequestBody.contextList.contextId = item.userId
                    }
                    return (
                        <TouchableOpacity activeOpacity={0.7} style={{ flexDirection: 'column' }}
                            onPress={() => this._onMessageItemClick(item)}>
                            {item.userId === this.props.main.userIdentity.userId
                                ? <MessageSenderMe message={item} />
                                : <MessageSenderSomeone message={item} />}
                        </TouchableOpacity>
                    )
                }}
                onMomentumScrollBegin={() => {
                    this.setState({
                        scrolled: true
                    })
                }}
            />
        )
    }
    _renderTypeContent() {
        return (
            <View style={{ flexDirection: 'row' }}>
                <TextInput multiline={true} placeholder={this.props.main.languageResource.r_message_send_message_input || strings('r_message_send_message_input')}
                    style={{ flex: 0.8, paddingLeft: 10 }}
                    onChangeText={(text) => {
                        this.setState({ messageBody: text })
                        this.sendMessageApiRequestBody.body = text
                    }}>
                    {this.state.messageBody}
                </TextInput>
                <TouchableOpacity onPress={() => this.sendMessage()}
                    style={{ flex: 0.2, alignItems: 'center', justifyContent: 'center', height: '100%' }} >
                    {this.props.messages.sendMessageSending
                        ? <View style={style.activityIndicatorWrapper}><ActivityIndicator animating={this.props.messages.sendMessageSending} /></View>
                        : <Ionicons name="md-send" size={25} />}
                </TouchableOpacity>

            </View>
        )
    }
    render() {
        return (
            <KeyboardAvoidingView style={{ flex: 1 }} behavior={Platform.OS === 'ios' ? 'padding' : null} keyboardVerticalOffset={Platform.OS === 'ios' ? 90 : 0}>
                <SafeAreaView style={{ flex: 1 }}>
                    <View style={{ flex: 1 }}>
                        <View style={{ flex: 1 }}>
                            {this._renderMessages()}
                        </View>
                        <View style={style.bottomView}>
                            {this._renderTypeContent()}
                        </View>
                    </View>
                </SafeAreaView>
            </KeyboardAvoidingView>

        )
    }
}

const style = StyleSheet.create({

    bottomView: {
        width: '100%',
        height: styles.typeBoxHeight,
        // position: 'absolute',
        // bottom: 0,
        backgroundColor: Colors.background,
    },
    activityIndicatorWrapper: {
        backgroundColor: Colors.background,
        height: 25,
        width: 25,
        borderRadius: 10,
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'space-around'
    }
});


const mapDispatchToProps = (dispatch) => {
    return {
        getMessageDetail: (...args) => dispatch(MessagesActions.getMessageDetailRequest(...args)),
        clearMessageDetailState: (...args) => dispatch(MessagesActions.clearMessageDetail(...args)),
        sendMessage: (...args) => dispatch(MessagesActions.sendMessageRequest(...args)),
        clearMessageGroups: (...args) => dispatch(MessagesActions.clearMessageGroups(...args)),
        getMessagesGroups: (...args) => dispatch(MessagesActions.getMessagesGroupsRequest(...args)),
    }
}

const mapStateToProps = (state) => {
    return {
        messages: state.messages,
        main: state.main
    }
}
export default connect(mapStateToProps, mapDispatchToProps)(MessageDetail);