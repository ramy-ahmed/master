import React from 'react';

import { View, StyleSheet, SafeAreaView, TouchableOpacity } from 'react-native';
import Ionicons from 'react-native-vector-icons/Ionicons';
import MaterialIcons from 'react-native-vector-icons/MaterialIcons';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
import Colors from '../../../theme/Colors';
import TextView from '../../../components/TextView';

import { NavigationActions } from 'react-navigation';
import Constants from '../../../services/Constants';


import { connect } from 'react-redux';
import { strings } from '../../../locales/i18n';

class NewActivitySelector extends React.Component {
    constructor(props) {
        super(props)
    }

    static navigationOptions = ({ navigation }) => {
        const { params = {} } = navigation.state;

        return {
            headerLeft: (
                <View style={{ flex: 1, flexDirection: 'row', alignItems: 'center' }}>
                    <Ionicons style={{ paddingLeft: 20, paddingRight: 20, }} name="ios-arrow-back" color="black" size={25} onPress={() => {
                        const backActions = NavigationActions.back({
                            key: null
                        });
                        navigation.dispatch(backActions)
                        //navigation.goBack()
                    }} />
                </View>
            ),
            title: params.languageResource !== undefined ? params.languageResource.r_activity_add_activity_type || strings('r_activity_add_activity_type') : strings('r_activity_add_activity_type'),
        }
    }

    componentDidMount() {
        this.props.navigation.setParams({
            languageResource: this.props.main.languageResource
        })
    }

    typeItemClick(activityType) {
        const backActions = NavigationActions.back({
            key: null
        });
        this.props.navigation.dispatch(backActions)


        // this.props.navigation.navigate('NewActivityClassSelector', { activityType: activityType })

        const AddingActivityData = {
            activityType: activityType,
            isCommon: Constants.NewActivityConstant.ClassActivity,
            courseId: this.props.courseDetail.course.courseId,
            selectedClass: [this.props.courseDetail.course.classId]
        }
        this.props.navigation.navigate('NewActivityContents', { addingActivityData: AddingActivityData })

    }
    render() {
        return (
            <SafeAreaView style={{ flex: 1, padding: 5, backgroundColor: Colors.background }}>
                <View style={{ flexDirection: 'row' }}>
                    <TouchableOpacity style={{ flex: 1, margin: 5, padding: 10, backgroundColor: 'white', flexDirection: 'column', alignItems: 'center', }}
                        onPress={() => this.typeItemClick(Constants.ActivityType.Assignment)}>
                        <MaterialCommunityIcons style={{}} color="black" name="file-document-outline" size={30} />
                        <TextView weight="bold" style={{ color: 'black', marginTop: 5, fontSize: 16 }}>
                            {this.props.main.languageResource.r_activity_type_assignment || strings('r_activity_type_assignment')}
                        </TextView>
                    </TouchableOpacity>


                    <TouchableOpacity style={{ flex: 1, margin: 5, padding: 10, backgroundColor: 'white', flexDirection: 'column', alignItems: 'center' }}
                        onPress={() => this.typeItemClick(Constants.ActivityType.Video)}>
                        <MaterialCommunityIcons style={{}} color="black" name="play-circle-outline" size={30} />
                        <TextView weight="bold" style={{ color: 'black', marginTop: 5, fontSize: 16 }}>
                            {this.props.main.languageResource.r_activity_type_video || strings('r_activity_type_video')}
                        </TextView>
                    </TouchableOpacity>

                </View>
                <View style={{ flexDirection: 'row' }}>
                    <TouchableOpacity style={{ flex: 1, margin: 5, padding: 10, backgroundColor: 'white', flexDirection: 'column', alignItems: 'center' }}
                        onPress={() => this.typeItemClick(Constants.ActivityType.Document)}>
                        <Ionicons style={{}} color="black" name="ios-document" size={30} />
                        <TextView weight="bold" style={{ color: 'black', marginTop: 5, fontSize: 16 }}>
                            {this.props.main.languageResource.r_activity_type_document || strings('r_activity_type_document')}
                        </TextView>
                    </TouchableOpacity>
                    <TouchableOpacity style={{ flex: 1, margin: 5, padding: 10, backgroundColor: 'white', flexDirection: 'column', alignItems: 'center' }}
                        onPress={() => this.typeItemClick(Constants.ActivityType.LinkActivity)}>
                        <MaterialCommunityIcons style={{}} color="black" name="link-variant" size={30} />
                        <TextView weight="bold" style={{ color: 'black', marginTop: 5, fontSize: 16 }}>
                            {this.props.main.languageResource.r_activity_type_link || strings('r_activity_type_link')}
                        </TextView>
                    </TouchableOpacity>
                </View>
                <View style={{ flexDirection: 'row' }}>
                    <TouchableOpacity disabled={true} style={{ opacity: 0.5, flex: 1, margin: 5, padding: 10, backgroundColor: 'white', flexDirection: 'column', alignItems: 'center' }}
                        onPress={() => this.typeItemClick(Constants.ActivityType.Assignment)}>
                        <MaterialCommunityIcons style={{}} color="black" name="format-text" size={30} />
                        <TextView weight="bold" style={{ color: 'black', marginTop: 5, fontSize: 16 }}>
                            {this.props.main.languageResource.r_activity_type_plain_text || strings('r_activity_type_plain_text')}
                        </TextView>
                    </TouchableOpacity>

                    <TouchableOpacity disabled={true} style={{ opacity: 0.5, flex: 1, margin: 5, padding: 10, backgroundColor: 'white', flexDirection: 'column', alignItems: 'center' }}
                        onPress={() => this.typeItemClick(Constants.ActivityType.Dictionary)}>
                        <Ionicons style={{}} color="black" name="ios-list" size={30} />
                        <TextView weight="bold" style={{ color: 'black', marginTop: 5, fontSize: 16 }}>
                            {this.props.main.languageResource.r_activity_type_dictionary || strings('r_activity_type_dictionary')}
                        </TextView>
                    </TouchableOpacity>
                </View>
            </SafeAreaView>
        )
    }
}

const mapDispatchToProps = (dispatch) => {
    return {}
}

const mapStateToProps = (state) => {
    return {
        main: state.main,
        courseDetail: state.courseDetail,
    }
}
export default connect(mapStateToProps, mapDispatchToProps)(NewActivitySelector)