import React from 'react';
import { View, SafeAreaView, Text, Image } from 'react-native';
import Ionicons from 'react-native-vector-icons/Ionicons';
import { NavigationActions } from 'react-navigation';
import AsyncStorage from '@react-native-community/async-storage';

import CourseDetailTabMenu from '../../../navigation/CourseDetailTabMenu';
//redux
import CourseDetailActions from '../../../redux/CourseDetailRedux';
import { connect } from 'react-redux';

import Constants from '../../../services/Constants';



class CourseDetail extends React.Component {
    constructor(props) {
        super(props);

        this.props.setCourse(this.props.navigation.getParam('Course'));
    }
    static navigationOptions = ({ navigation }) => {

        return {
            // headerRight: (
            //     <View style={{ flex: 1, flexDirection: 'row', justifyContent: 'center', alignItems: 'center' }}>
            //         <Ionicons style={{ paddingLeft: 20, paddingRight: 20 }} color="black" name="ios-add" size={30}
            //             onPress={() => navigation.navigate('NewActivitySelector')}
            //         />
            //     </View>
            // ),
            headerTitle: navigation.getParam('Course').name,
        };
    }

    componentWillMount() { //clear state cache
        // if (this.props.main.userIdentity.userType === Constants.UserTypes.Instructor) {
        //     this.props.navigation.setParams({
        //         instructor: true,
        //         addNewClick: this.addNewActivity
        //     })
        // } else {
        //     this.props.navigation.setParams({
        //         instructor: false
        //     })
        // }
        this.props.setActivityList()
    }
    addNewActivity() {
        this.props.navigation.navigate('NewActivitySelector')
    }

    static router = CourseDetailTabMenu.router;
    _renderCourseTabMenu() {
        return <CourseDetailTabMenu navigation={this.props.navigation} screenProps={{ languageResource: this.props.main.languageResource }} />
    }

    render() {
        return (
            <View style={{ flex: 1 }}>
                {this._renderCourseTabMenu()}
            </View>
        );
    }
}


const mapDispatchToProps = (dispatch) => {
    return {
        setCourse: (...args) => dispatch(CourseDetailActions.setCourse(...args)),
        setActivityList: () => dispatch(CourseDetailActions.setActivityList())
    }
}

const mapStateToProps = (state) => {
    return {
        courseDetail: state.courseDetail,
        main: state.main,
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(CourseDetail)