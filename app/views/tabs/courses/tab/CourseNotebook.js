import React from 'react';
import { View, Text } from 'react-native';
import PageEmpty from '../../../../components/courses/PageEmpty'


class CourseNotebook extends React.Component {
    constructor(props) {
        super(props)
    }
    render() {
        return (
            <View style={{ flex: 1 }}>
                <PageEmpty text={"Course notebook is empty"} />
            </View>
        );
    }
}
export default CourseNotebook;