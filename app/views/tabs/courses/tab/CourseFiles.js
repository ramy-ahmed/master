import React from 'react';
import { View, Text, SafeAreaView, FlatList, RefreshControl, ScrollView, Image, Platform } from 'react-native';
import PageEmpty from '../../../../components/courses/PageEmpty'
import AsyncStorage from '@react-native-community/async-storage';
import LocalStorageConstants from '../../../../local/LocalStorageConstants';
import DownloadedFile from '../../../../components/courses/DownloadedFile';

import empty_activity from '../../../../assets/images/empty_activity.png';
import Constants from '../../../../services/Constants';

import RNFetchBlob from 'rn-fetch-blob'

import { connect } from 'react-redux';
import { strings } from '../../../../locales/i18n';

class CourseFiles extends React.Component {
    constructor(props) {
        super(props)

        this.state = {
            downloadDatas: [],
            refreshing: false
        }
    }

    async componentDidMount() {
        this.getDownloadedFiles()
    }

    //TODO: Farklı formattaki dosyalar ? 
    async _onRemove(removedData) {
        var filePath = null;
        if (removedData.file.extension === Constants.DocumentExtenstion.Video) {
            filePath = removedData.downloadedVideoPath
        } else if (removedData.file.extension === Constants.DocumentExtenstion.Pdf) {
            filePath = removedData.downloadedFilePaths
        }

        this.clearCacheData(filePath)

        var copyArray = Object.assign([], this.state.downloadDatas)
        let filteredArray = copyArray.filter(item => item.activityId !== removedData.activityId)
        AsyncStorage.setItem(LocalStorageConstants.DownloadedFilePaths, JSON.stringify(filteredArray))

        this.setState({ downloadDatas: filteredArray })
    }
    async getDownloadedFiles() {
        const downloadDatas = await AsyncStorage.getItem(LocalStorageConstants.DownloadedFilePaths)
        if (downloadDatas !== null) {
            //only get downloaded data of selected course
            let filteredData = JSON.parse(downloadDatas).filter(item => item.courseId === this.props.courseDetail.course.courseId)
            console.log("downloadDatas: ", filteredData);
            this.setState({ downloadDatas: filteredData })
        }
        this.setState({ refreshing: false })

    }
    async _onRefresh() {
        this.setState({ refreshing: true })
        this.getDownloadedFiles()
    }

    clearCacheData(filePath) {
        RNFetchBlob.fs.unlink(filePath).then(() => {
            console.log("cache data removed");
        }).catch((err) => {
            console.log("cache data remove error : ", err);
        })
    }

    keyExtractor = (item, index) => item.activityId

    _renderDownloadedFiles() {
        if (this.state.downloadDatas !== null && this.state.downloadDatas.length !== 0) {
            return (
                <FlatList
                    refreshControl={<RefreshControl refreshing={this.state.refreshing} onRefresh={() => this._onRefresh()} />}
                    keyExtractor={this.keyExtractor}
                    data={this.state.downloadDatas}
                    renderItem={({ item }) => (
                        <DownloadedFile activity={item}
                            navigation={this.props.navigation}
                            onRemove={(item) => this._onRemove(item)} />
                    )}
                />
            )
        } else {
            return (
                <ScrollView contentContainerStyle={{ flexGrow: 1, justifyContent: 'center' }}
                    refreshControl={<RefreshControl refreshing={this.state.refreshing} onRefresh={() => this._onRefresh()} />}>
                    <PageEmpty text={this.props.main.languageResource.r_activity_downloaded_files_empty || strings('r_activity_downloaded_files_empty')} />
                </ScrollView>
            )
        }
    }
    render() {
        return (
            <SafeAreaView style={{ flex: 1 }}>
                <View style={{ flex: 1 }}>
                    {this._renderDownloadedFiles()}
                </View>
            </SafeAreaView>
        );
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
    }
}

const mapStateToProps = (state) => {
    return {
        courseDetail: state.courseDetail,
        main: state.main,
    }
}
export default connect(mapStateToProps, mapDispatchToProps)(CourseFiles);