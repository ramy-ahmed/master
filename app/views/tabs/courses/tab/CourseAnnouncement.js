import React from 'react';
import { View, Text, SafeAreaView, FlatList, RefreshControl, TouchableOpacity } from 'react-native';
import PageEmpty from '../../../../components/courses/PageEmpty'
import Loader from '../../../../components/Loader';
import Colors from '../../../../theme/Colors';
import NetInfo from '@react-native-community/netinfo';

//redux
import { connect } from 'react-redux';
import CourseDetailActions from '../../../../redux/CourseDetailRedux';
import Constants from '../../../../services/Constants';


import AnnouncementListItem from '../../../../components/announcement/AnnouncementListItem';
import AnnouncementEmpty from '../../../../components/announcement/AnnouncementEmpty';

class CourseAnnouncement extends React.Component {
    constructor(props) {
        super(props)

        this.state = {
            refreshing: false,
            scrolled: false,
        }

        this.classAnnouncementApiRequestBody = {
            remote: true,
            classId: this.props.courseDetail.course.classId,
            almsPlusApiUrl: this.props.main.selectedOrganization.almsPlusApiUrl,
            address: this.props.main.selectedOrganization.apiAddress,
            accessToken: this.props.main.authData.access_token,
            contextType: 16,
            port: Constants.ApiPort,
            take: Constants.ApiResponseQuantity,
            skip: 0
        }
    }

    componentWillUnmount() {
        this.props.setCourseAnnouncementList();
    }

    componentDidMount() {
        NetInfo.fetch().then(statu => {
            if (statu.isConnected) this.classAnnouncementApiRequestBody.remote = true; else this.classAnnouncementApiRequestBody.remote = false;
            this.props.getCourseAnnouncements(this.classAnnouncementApiRequestBody)
        });
    }

    keyExtractor = (item, index) => index.toString()

    _onRefresh() {

    }

    _onItemClick(item) {
        this.props.navigation.navigate('CourseAnnouncementDetail', { Announcement: item })
    }


    _renderMoreItem = () => {
        if (this.state.scrolled && (this.props.courseDetail.courseAnnouncementData.length % Constants.ApiResponseQuantity === 0)) {
            console.log("Load more item")
            this.classAnnouncementApiRequestBody.skip = this.classAnnouncementApiRequestBody.skip + Constants.ApiResponseQuantity;
            this.props.getCourseAnnouncements(this.classAnnouncementApiRequestBody)
        }
    }

    _renderCourseAnnouncements() {
        if (!this.props.courseDetail.couseAnnouncementsError && this.props.courseDetail.courseAnnouncementData.length !== 0) {
            return (
                <FlatList
                    keyExtractor={this.keyExtractor}
                    data={this.props.courseDetail.courseAnnouncementData}
                    refreshControl={<RefreshControl refreshing={this.state.refreshing} onRefresh={() => this._onRefresh()} />}
                    onEndReached={this._renderMoreItem}
                    onEndReachedThreshold={0.2}
                    initialNumToRender={Constants.ApiResponseQuantity}
                    onTouchStart={() => {
                        this.setState({
                            scrolled: true
                        })
                    }}
                    renderItem={({ item }) => (
                        <TouchableOpacity activeOpacity={0.7} style={{ flexDirection: 'column' }} onPress={() => this._onItemClick(item)}>
                            <AnnouncementListItem announcement={item} />
                        </TouchableOpacity>
                    )}
                />
            )
        }
        else if (!this.props.courseDetail.couseAnnouncementsfetching && this.props.courseDetail.courseAnnouncementData.length === 0) {
            return (
                <AnnouncementEmpty />
            )
        } else return null
    }
    render() {
        return (
            <SafeAreaView style={{ flex: 1 }}>
                <Loader loading={this.props.courseDetail.couseAnnouncementsfetching} />
                <View style={{ flex: 1, flexDirection: 'column', backgroundColor: Colors.background }}>
                    <View style={{ flex: 1 }}>
                        {this._renderCourseAnnouncements()}
                    </View>
                </View>
            </SafeAreaView>
        );
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        getCourseAnnouncements: (...args) => dispatch(CourseDetailActions.getCourseAnnouncementRequest(...args)),
        setCourseAnnouncementList: (...args) => dispatch(CourseDetailActions.setCourseAnnouncementList(...args)),
    }
}

const mapStateToProps = (state) => {
    return {
        courseDetail: state.courseDetail,
        main: state.main
    }
}
export default connect(mapStateToProps, mapDispatchToProps)(CourseAnnouncement)