import React from 'react';
import { SafeAreaView, View, Platform, FlatList, TouchableOpacity, TextInput, ScrollView, Alert } from 'react-native';
import Ionicons from 'react-native-vector-icons/Ionicons';
import { NavigationActions } from 'react-navigation';
import { CheckBox, Input, Button, ThemeProvider } from 'react-native-elements';
import Constants from '../../../../services/Constants';
import Colors from '../../../../theme/Colors';
import Icon from 'react-native-vector-icons/AntDesign'

import AnnouncementActions from '../../../../redux/AnnouncementRedux';
import AddActivityActions from '../../../../redux/AddActivityRedux';
import { connect } from 'react-redux'
import TextView from '../../../../components/TextView';
import AsyncStorage from '@react-native-community/async-storage';
import LocalStorageConstants from '../../../../local/LocalStorageConstants';

import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view'
import { strings } from '../../../../locales/i18n';

class NewActivityClassSelector extends React.Component {
    constructor(props) {
        super(props)

        this.state = {
            receivers: [],
            receiverSearhText: '',
            nextButtonDisable: true,
            isCommon: Constants.NewActivityConstant.ClassActivity,
            activityType: this.props.navigation.getParam('activityType', null)
        }

        this.searchReceiverApiRequestBody = {
            accessToken: this.props.main.authData.access_token,
            almsPlusApiUrl: this.props.main.selectedOrganization.almsPlusApiUrl,
            searchKey: '',
            getTypes: 'Class',
            take: Constants.ApiResponseQuantity,
            skip: 0
        }

        this.getClassInformationApiRequestBody = {
            accessToken: this.props.main.authData.access_token,
            almsPlusApiUrl: this.props.main.selectedOrganization.almsPlusApiUrl,
            courseId: this.props.courseDetail.course.courseId,
            searchedText: '',
            take: 1,
            skip: 0
        }

    }

    static navigationOptions = ({ navigation }) => {
        const { params = {} } = navigation.state;

        return {
            headerLeft: (
                <View style={{ flex: 1, flexDirection: 'row', alignItems: 'center' }}>
                    <Ionicons style={{ paddingLeft: 20, paddingRight: 20, }} name="ios-arrow-back" color="black" size={25} onPress={() => {
                        const backActions = NavigationActions.back({
                            key: null
                        });
                        navigation.dispatch(backActions)
                    }} />
                </View>
            ),
            title: params.languageResource !== undefined ? params.languageResource.r_activity_add_class_selection_title || strings('r_activity_add_class_selection_title') : strings('r_activity_add_class_selection_title'),
        }
    }
    componentDidMount() {
        this.props.navigation.setParams({
            languageResource: this.props.main.languageResource
        })
        this.props.getClassInformation(this.getClassInformationApiRequestBody)
    }

    componentWillReceiveProps(props) {
        if (!props.addActivity.classInformationFetching && props.addActivity.classInformation !== this.props.addActivity.classInformation) {
            //apiden sadece tek data alınıyor.
            var mClass = {
                id: props.addActivity.classInformation[0].value,
                value: props.addActivity.classInformation[0].label
            }
            this.setState({
                receivers: [...this.state.receivers, mClass]
            })
        }
    }

    _renderActivityType() {
        return (
            <View style={{ flexDirection: 'row', justifyContent: 'space-around' }}>
                <View style={{ flex: 1 }}>
                    <CheckBox title={this.props.main.languageResource.r_activity_add_class_activity_type || strings('r_activity_add_class_activity_type')}
                        checkedIcon='dot-circle-o'
                        uncheckedIcon='circle-o'
                        checked={this.state.isCommon === Constants.NewActivityConstant.ClassActivity}
                        textStyle={{ color: 'black' }}
                        onPress={() => this.setState({ isCommon: Constants.NewActivityConstant.ClassActivity })}
                        checkedColor={Colors.primary} containerStyle={{ borderWidth: 0, backgroundColor: '#eeede9', margin: 0 }}
                    />
                </View>
                <View style={{ flex: 1 }}>
                    <CheckBox title={this.props.main.languageResource.r_activity_add_general_activity_type || strings('r_activity_add_general_activity_type')}
                        checkedIcon='dot-circle-o'
                        uncheckedIcon='circle-o'
                        checked={this.state.isCommon === Constants.NewActivityConstant.CommonActivity}
                        textStyle={{ color: 'black' }}
                        onPress={() => this.setState({ isCommon: Constants.NewActivityConstant.CommonActivity })}
                        checkedColor={Colors.primary} containerStyle={{ borderWidth: 0, backgroundColor: '#eeede9', margin: 0 }}
                    />
                </View>
            </View>
        )
    }
    searchText(item) {
        this.searchReceiverApiRequestBody.searchKey = item
        if (item.length > 2) {
            this.props.searchReceiver(this.searchReceiverApiRequestBody)
        } else {
            this.props.clearCacheSearchData()
        }
    }
    addReceiver(item) {
        let checkList = this.state.receivers.filter(receiver => receiver.id === item.id) //checklist for already existing
        if (checkList.length === 0) {
            this.setState({
                receiverSearhText: '',
                receivers: [...this.state.receivers, item],
            })
        } else {
            Alert.alert('', this.props.main.languageResource.r_activity_add_error_already_selected_class || strings('r_activity_add_error_already_selected_class'))
        }
        this.props.clearCacheSearchData()
    }

    removeReceiver(e) {
        let filteredArray = this.state.receivers.filter(item => item.id !== e.id)
        this.setState({ receivers: filteredArray })
    }

    keyExtractor = (item, index) => item.id
    _renderAddReceivers() {
        return (
            <View style={{ marginTop: 20 }}>
                <View style={{ flexDirection: 'row' }}>
                    <TextView weight="bold" style={{ color: 'black', fontSize: 19, flex: 0.7, alignSelf: 'center' }}>
                        {this.props.main.languageResource.r_activity_add_class_add || strings('r_activity_add_class_add')}
                    </TextView>
                    {/* <CheckBox title="Tüm şubeler"
                        checked={this.state.isCommon === Constants.NewActivityConstant.CommonActivity}
                        textStyle={{ color: 'black', fontSize: 12 }}
                        onPress={() => this.setState({ isCommon: Constants.NewActivityConstant.CommonActivity })}
                        checkedColor={Colors.primary} containerStyle={{ backgroundColor:'white',borderWidth: 0, margin: 0, padding: 0, flex: 0.3, alignItems: 'flex-end' }}
                    /> */}
                </View>
                <View style={{ flexDirection: 'row', flexWrap: 'wrap', backgroundColor: Colors.background, marginTop: 5 }}>
                    {
                        this.state.receivers.map(item => (
                            <TouchableOpacity key={item.id} style={{ margin: 5, padding: 5, backgroundColor: 'white', flexDirection: 'row', alignItems: 'center' }}
                                onPress={() => this.removeReceiver(item)}>
                                <TextView weight="bold" style={{ color: 'black', fontSize: 12 }}>
                                    {item.value}
                                </TextView>
                                <Ionicons name="ios-close" size={20} color="black" style={{ marginStart: 10 }} />
                            </TouchableOpacity>
                        ))
                    }
                </View>
                <TextInput style={{ fontSize: 16, height: 40, borderColor: Colors.background, borderWidth: 1, marginTop: 5 }}
                    placeholder={this.props.main.languageResource.r_activity_add_search_class || strings('r_activity_add_search_class')}
                    onChangeText={text => {
                        this.searchText(text)
                        this.setState({
                            receiverSearhText: text
                        })
                    }}>{this.state.receiverSearhText}</TextInput>

                <FlatList
                    keyboardShouldPersistTaps="handled"
                    keyExtractor={this.keyExtractor}
                    data={this.props.announcements.searchAnnouncementReceiverData}
                    renderItem={({ item }) => (
                        <TouchableOpacity style={{ padding: 10 }} onPress={() => this.addReceiver(item)}>
                            <TextView weight="regular" style={{ color: 'black' }}>{item.value}</TextView>
                        </TouchableOpacity>
                    )}
                />
            </View>
        )
    }
    _renderActionButtons() {
        return (
            <View style={{ alignSelf: 'flex-end', bottom: 0 }}>
                <Button
                    disabled={this.state.receivers.length === 0 ? true : false}
                    title={this.props.main.languageResource.r_activity_add_next_button_text || strings('r_activity_add_next_button_text')}
                    onPress={() => this.routeToActivityContent()}
                    icon={<Icon name="arrowright" size={20} color="white" />}
                    iconRight
                    containerStyle={{ margin: 10 }}
                    buttonStyle={{ backgroundColor: Colors.primary }}
                    titleStyle={{ marginEnd: 8, fontFamily: Platform.OS === 'ios' ? 'SFProText-Medium' : 'Roboto-Medium' }} />
            </View>
        )
    }
    async routeToActivityContent() {
        const AddingActivityData = {
            activityType: this.state.activityType,
            isCommon: this.state.isCommon,
            courseId: this.props.courseDetail.course.courseId
        }
        var selectedClass = [];
        this.state.receivers.map((receiver) => {
            selectedClass.push(receiver.id);
        })

        AddingActivityData.selectedClass = selectedClass;
        await AsyncStorage.setItem(LocalStorageConstants.AddingActivityData, JSON.stringify(AddingActivityData))
        this.props.navigation.navigate('NewActivityContents', { addingActivityData: AddingActivityData })
    }
    render() {
        return (
            <SafeAreaView style={{ flex: 1 }}>
                <ScrollView contentContainerStyle={{ padding: 10 }}>
                    <KeyboardAwareScrollView>
                        {this._renderActivityType()}
                        {this._renderAddReceivers()}
                    </KeyboardAwareScrollView>
                    {this._renderActionButtons()}
                </ScrollView>
            </SafeAreaView>
        )
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        searchReceiver: (...args) => dispatch(AnnouncementActions.searchAnnouncementReceiverRequest(...args)),
        clearCacheSearchData: (...args) => dispatch(AnnouncementActions.clearAnnouncementReceiver(...args)),
        getClassInformation: (...args) => dispatch(AddActivityActions.getClassInformationRequest(...args)),
    }
}

const mapStateToProps = (state) => {
    return {
        main: state.main,
        announcements: state.announcement,
        addActivity: state.addActivity,
        courseDetail: state.courseDetail,
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(NewActivityClassSelector);