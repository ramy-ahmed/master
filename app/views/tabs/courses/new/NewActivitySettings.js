import React from 'react';
import { ScrollView, SafeAreaView, View, Image, TouchableOpacity, TextInput, FlatList, Switch, Platform, Alert } from 'react-native';
import { NavigationActions } from 'react-navigation';
import { Button } from 'react-native-elements';
import TextView from '../../../../components/TextView';
import Ionicons from 'react-native-vector-icons/Ionicons';
import Colors from '../../../../theme/Colors';
import ImagePicker from 'react-native-image-picker';
import Icon from 'react-native-vector-icons/AntDesign'
import { showMessage, hideMessage } from 'react-native-flash-message';

import { connect } from 'react-redux';
import AddActivityActions from '../../../../redux/AddActivityRedux';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view'
import Constants from '../../../../services/Constants';
import { strings } from '../../../../locales/i18n';
import { collectAssignmentData, collectDocumentData, collectLinkData, collectVideoData } from '../../../../helpers/DataCollector';
import Loader from '../../../../components/Loader';


class NewActivitySettings extends React.Component {
    constructor(props) {
        super(props)

        this.state = {
            addingActivityData: this.props.navigation.getParam('addingActivityData', null),
            imageSelected: false,
            unitSearchText: '',
            creditSearchText: '',
            unitListVisibility: false,
            creditListVisibility: false,
            selectedActivityUnits: [],
            selectedActivityCredits: [],
            activityAlwaysOnTop: false,
            selectedConditionalActivity: null,
            isConditionalActivitiesVisibility: false,
            conditionalActivities: this.props.addActivity.conditionalActivities,
            allUnits: false,
            loader: false,
        }

        this.getTagsUnitApiRequestBody = {
            almsPlusApiUrl: this.props.main.selectedOrganization.almsPlusApiUrl,
            accessToken: this.props.main.authData.access_token,
            type: Constants.ActivityTags.etcActivityUnit,
            searchedText: this.state.unitSearchText,
            skip: 0,
            take: 100,
        }
        this.getTagsCreditsRequestBody = {
            almsPlusApiUrl: this.props.main.selectedOrganization.almsPlusApiUrl,
            accessToken: this.props.main.authData.access_token,
            type: Constants.ActivityTags.Credits,
            searchedText: this.state.creditSearchText,
            skip: 0,
            take: 100,
        }

        this.addActivityApiRequesyBody = {
            almsPlusApiUrl: this.props.main.selectedOrganization.almsPlusApiUrl,
            accessToken: this.props.main.authData.access_token,
            apiEndPoint: null,
            data: null,
        }

        this.fileUplaodRequestBody = {
            almsPlusApiUrl: this.props.main.selectedOrganization.almsPlusApiUrl,
            accessToken: this.props.main.authData.access_token,
            formData: null,
        }
    }

    static navigationOptions = ({ navigation }) => {
        const { params = {} } = navigation.state;

        return {
            headerLeft: (
                <View style={{ flex: 1, flexDirection: 'row', alignItems: 'center' }}>
                    <Ionicons style={{ paddingLeft: 20, paddingRight: 20, }} name="ios-arrow-back" color="black" size={25} onPress={() => {
                        const backActions = NavigationActions.back({
                            key: null
                        });
                        navigation.dispatch(backActions)
                    }} />
                </View>
            ),
            title: params.languageResource !== undefined ? params.languageResource.r_activity_add_settings_title || strings('r_activity_add_settings_title') : strings('r_activity_add_settings_title'),
        }
    }
    //TODO: api çağrıları başarısız olması durumunu kontrol et!
    componentDidMount() {
        this.props.navigation.setParams({
            languageResource: this.props.main.languageResource
        })
        this.props.getTagsUnit(this.getTagsUnitApiRequestBody)
        this.props.getTagsCredits(this.getTagsCreditsRequestBody)
    }

    componentWillReceiveProps(props) {
        if (props.addActivity.addActivityError) {
            Alert.alert('', this.props.main.languageResource.r_activity_add_error || strings('r_activity_add_error'))
            this.props.clearAddActivityData()
        }


        if (this.props.addActivity.addActivityData !== props.addActivity.addActivityData && !props.addActivity.addActivityError) {
            showMessage({
                message: this.props.main.languageResource.r_activity_add_success || strings('r_activity_add_success'),
                description: this.props.main.languageResource.r_activity_add_success_description || strings('r_activity_add_success_description'),
                type: 'success',
                duration: 5000,
            })
            this.props.clearAddActivityData()
            this.props.navigation.navigate('CourseDetail')
        }

    }
    imagePicker() {
        const options = {
            title: '',
            storageOptions: {
                skipBackup: true,
                path: 'images',
            },
        };

        ImagePicker.launchImageLibrary(options, (response) => {
            if (response.didCancel) {
                console.log('User cancelled image picker');
            } else if (response.error) {
                console.log('ImagePicker Error: ', response.error);
            } else if (response.customButton) {
                console.log('User tapped custom button: ', response.customButton);
            } else {
                const source = { uri: response.uri };
                console.log("uri :", source);
                this.setState({
                    imageSelected: true,
                    activityCardImg: { uri: response.uri },
                });
            }
        })
    }


    _renderImageComponent() {
        if (this.state.imageSelected) {
            return (
                <TouchableOpacity style={{}} onPress={() => this.imagePicker()}>
                    <Image style={{ aspectRatio: 16 / 9 }} source={this.state.activityCardImg} />
                </TouchableOpacity>
            )
        } else {
            return (
                <TouchableOpacity style={{ height: 200, alignItems: 'center', justifyContent: 'center', backgroundColor: Colors.background }} onPress={() => this.imagePicker()}>
                    <Image style={{ width: 70, height: 70 }} source={require('../../../../assets/images/image_not_found.png')} />
                </TouchableOpacity>
            )
        }
    }
    _renderActivityPicture() {
        return (
            <View style={{ marginTop: 10 }}>
                <TextView weight="bold" style={{ color: "black", fontSize: 18, marginBottom: 5 }}>
                    {this.props.main.languageResource.r_activity_add_settings_activity_picture || strings('r_activity_add_settings_activity_picture')}
                </TextView>
                {this._renderImageComponent()}
            </View>
        )
    }

    unitKeyExtractor = (item, index) => item.value
    _renderActivityUnit() {
        return (
            <View style={{ marginTop: 10 }}>
                <TextView weight="bold" style={{ color: "black", fontSize: 18, marginBottom: 5 }}>
                    {this.props.main.languageResource.r_activity_add_settings_activity_display_units || strings('r_activity_add_settings_activity_display_units')}
                </TextView>
                <View style={{ flexDirection: 'row', flexWrap: 'wrap', backgroundColor: Colors.background, marginTop: 5 }}>
                    {
                        this.state.selectedActivityUnits.map(item => (
                            <TouchableOpacity key={item.value} style={{ margin: 5, padding: 5, backgroundColor: 'white', flexDirection: 'row', alignItems: 'center' }}
                                onPress={() => this.removeActivityUnit(item)}>
                                <TextView weight="bold" style={{ color: 'black', fontSize: 12 }}>{item.label}</TextView>
                                <Ionicons name="ios-close" size={20} color="black" style={{ marginStart: 10 }} />
                            </TouchableOpacity>
                        ))
                    }
                </View>

                <TextInput placeholder={this.props.main.languageResource.r_activity_add_settings_search_unit || strings('r_activity_add_settings_search_unit')} style={{ borderColor: Colors.background, borderWidth: 1, padding: 5 }}
                    onTouchEnd={() => {
                        this.setState({ unitListVisibility: !this.state.unitListVisibility })
                    }} />
                {this._renderUnitFlatList()}
            </View>
        )
    }

    removeActivityUnit(e) {
        let filteredArray = this.state.selectedActivityUnits.filter(item => item.value !== e.value)
        this.setState({ selectedActivityUnits: filteredArray })
    }
    addActivityUnit(u) {
        let checkList = this.state.selectedActivityUnits.filter(unit => unit.value === u.value) //checklist for already existing
        if (checkList.length === 0) {
            this.setState({
                unitListVisibility: false,
                selectedActivityUnits: [...this.state.selectedActivityUnits, u]
            })
        }
    }
    _renderUnitFlatList() {
        if (this.state.unitListVisibility) {
            return (
                <FlatList
                    keyboardShouldPersistTaps="always"
                    keyExtractor={this.unitKeyExtractor}
                    data={this.props.addActivity.tagsUnits}
                    renderItem={({ item }) => (
                        <TouchableOpacity style={{ padding: 10 }}
                            onPress={() => {
                                this.addActivityUnit(item)
                            }}>
                            <TextView weight="regular" style={{ color: 'black' }}>{item.label}</TextView>
                        </TouchableOpacity>
                    )}
                />
            )
        } else return null
    }
    _renderSelectedActiviy() {
        if (this.state.selectedConditionalActivity !== null) {
            return (
                <View style={{ flexDirection: 'row', flexWrap: 'wrap', backgroundColor: Colors.background, marginTop: 5 }}>
                    <TouchableOpacity key={this.state.selectedConditionalActivity.id} style={{ margin: 5, padding: 5, backgroundColor: 'white', flexDirection: 'row', alignItems: 'center' }}
                        onPress={() => this.removeConditionalActivity(this.state.selectedConditionalActivity)}>
                        <TextView weight="bold" style={{ color: 'black', fontSize: 12 }}>{this.state.selectedConditionalActivity.activityName}</TextView>
                        <Ionicons name="ios-close" size={20} color="black" style={{ marginStart: 10 }} />
                    </TouchableOpacity>
                </View>
            )
        } else return null;
    }
    removeConditionalActivity(activity) {
        this.setState({ selectedConditionalActivity: null })
    }
    _renderActivityPreCondition() {
        return (
            <View style={{ marginTop: 10 }}>
                <TextView weight="bold" style={{ color: "black", fontSize: 18, marginBottom: 5 }}>
                    {this.props.main.languageResource.r_activity_add_settings_prerequisite || strings('r_activity_add_settings_prerequisite')}
                </TextView>

                <View style={{ marginTop: 10 }}>
                    {this._renderSelectedActiviy()}
                    <TextInput
                        placeholder={this.props.main.languageResource.r_activity_add_content_search_activity || strings('r_activity_add_content_search_activity')}
                        style={{ borderColor: Colors.background, borderWidth: 1, padding: 5 }}
                        onChangeText={(text) =>
                            this.setState({
                                conditionalActivities: this.props.addActivity.conditionalActivities.filter((activity) =>
                                    activity.activityName.startsWith(text)).slice(0, 5)
                            })
                        }
                        onTouchEnd={() => {
                            this.setState({ isConditionalActivitiesVisibility: !this.isConditionalActivitiesVisibility })
                        }} />
                    {this._renderConditionalActivitesFlatList()}
                </View>
            </View>
        )
    }

    conditionalActivitiesKeyExtractor = (item, index) => item.activityId
    _renderConditionalActivitesFlatList() {
        if (this.state.isConditionalActivitiesVisibility) {
            return (
                <FlatList
                    keyboardShouldPersistTaps="always"
                    keyExtractor={this.conditionalActivitiesKeyExtractor}
                    data={this.state.conditionalActivities.slice(0, 5)}
                    renderItem={({ item }) => (
                        <TouchableOpacity style={{ padding: 10 }}
                            onPress={() => {
                                this.addConditionalActivity(item)
                            }}>
                            <TextView weight="regular" style={{ color: 'black' }}>{item.activityName}</TextView>
                        </TouchableOpacity>
                    )}
                />
            )
        } else return null
    }

    addConditionalActivity(activity) {
        this.setState({
            isConditionalActivitiesVisibility: false,
            selectedConditionalActivity: activity
        })
    }

    _renderActivityAlwaysOnTop() {
        return (
            <View style={{ marginTop: 10 }}>
                <TextView weight="bold" style={{ color: "black", fontSize: 18, marginBottom: 5 }}>
                    {this.props.main.languageResource.r_activity_add_settings_always_on_top || strings('r_activity_add_settings_always_on_top')}
                </TextView>
                <Switch style={{ alignSelf: 'flex-start' }} value={this.state.activityAlwaysOnTop} onValueChange={() => this.setState({
                    activityAlwaysOnTop: !this.state.activityAlwaysOnTop
                })} />
            </View>
        )
    }
    _renderActivityOwners() {
        return (
            <View style={{ marginTop: 10 }}>
                <TextView weight="bold" style={{ color: "black", fontSize: 18, marginBottom: 5 }}>
                    {this.props.main.languageResource.r_activity_add_settings_activity_owner || strings('r_activity_add_settings_activity_owner')}
                </TextView>

                <View style={{ flexDirection: 'row', flexWrap: 'wrap', backgroundColor: Colors.background, marginTop: 5 }}>
                    {
                        this.state.selectedActivityCredits.map(item => (
                            <TouchableOpacity key={item.value} style={{ margin: 5, padding: 5, backgroundColor: 'white', flexDirection: 'row', alignItems: 'center' }}
                                onPress={() => this.removeActivityCredit(item)}>
                                <TextView weight="bold" style={{ color: 'black', fontSize: 12 }}>{item.label}</TextView>
                                <Ionicons name="ios-close" size={20} color="black" style={{ marginStart: 10 }} />
                            </TouchableOpacity>
                        ))
                    }
                </View>
                <TextInput placeholder={this.props.main.languageResource.r_activity_add_settings_activity_owner_input || strings('r_activity_add_settings_activity_owner_input')} style={{ borderColor: Colors.background, borderWidth: 1, padding: 5 }} onTouchEnd={() => {
                    this.setState({
                        creditListVisibility: !this.state.creditListVisibility
                    })
                }} />

                {this._renderActivityOwnerFlatList()}
            </View>
        )
    }

    removeActivityCredit(e) {
        let filteredArray = this.state.selectedActivityCredits.filter(item => item.value !== e.value)
        this.setState({ selectedActivityCredits: filteredArray })
    }
    addActivityCredit(unit) {

        let checkList = this.state.selectedActivityCredits.filter(owner => owner.value === unit.value) //checklist for already existing
        if (checkList.length === 0) {
            this.setState({
                creditListVisibility: false,
                selectedActivityCredits: [...this.state.selectedActivityCredits, unit]
            })
        }

    }

    ownerKeyExtractor = (item, index) => item.value
    _renderActivityOwnerFlatList() {
        if (this.state.creditListVisibility) {
            return (
                <FlatList
                    keyboardShouldPersistTaps="always"
                    keyExtractor={this.ownerKeyExtractor}
                    data={this.props.addActivity.tagsCredits}
                    renderItem={({ item }) => (
                        <TouchableOpacity style={{ padding: 10 }}
                            onPress={() => {

                                this.addActivityCredit(item)
                            }}>
                            <TextView weight="regular" style={{ color: 'black' }}>{item.label}</TextView>
                        </TouchableOpacity>
                    )}
                />
            )
        } else return null;
    }

    _renderActionButtons() {
        return (
            <View style={{ alignSelf: 'flex-end', bottom: 0 }}>
                <Button disabled={false} title={this.props.main.languageResource.r_activity_add_settings_save_button || strings('r_activity_add_settings_save_button')}
                    onPress={() => this.saveAndPublish()}
                    icon={<Icon name="arrowright" size={20} color="white" />}
                    iconRight
                    containerStyle={{ margin: 10 }}
                    buttonStyle={{ backgroundColor: Colors.primary }}
                    titleStyle={{ marginEnd: 8, fontFamily: Platform.OS === 'ios' ? 'SFProText-Medium' : 'Roboto-Medium' }} />
            </View>
        )
    }
    settingsData() {
        var ownerIds = [];
        this.state.selectedActivityCredits.map((owner) => {
            ownerIds.push(owner.value);
        })
        var unitIds = [];
        this.state.selectedActivityUnits.map((tag) => {
            unitIds.push(tag.value);
        })

        this.state.addingActivityData.allUnits = this.state.allUnits;
        this.state.addingActivityData.alwaysOnTop = this.state.activityAlwaysOnTop;
        this.state.addingActivityData.selectedActivityOwnersTagIds = ownerIds
        this.state.addingActivityData.selectedActivityUnitsTagIds = unitIds

    }
    //TODO: Fill all types
    saveAndPublish() {
        this.settingsData()
        var apiBody = null;
        switch (this.state.addingActivityData.activityType) {
            case Constants.ActivityType.Assignment:
                apiBody = collectAssignmentData(this.state.addingActivityData)
                this.addActivityApiRequesyBody.apiEndPoint = Constants.AssignmentAdd;
                this.addActivityApiRequesyBody.data = apiBody;
                break;
            case Constants.ActivityType.Document:
                apiBody = collectDocumentData(this.state.addingActivityData)
                this.addActivityApiRequesyBody.apiEndPoint = Constants.DocumentAdd;
                this.addActivityApiRequesyBody.data = apiBody;
                break;
            case Constants.ActivityType.LinkActivity:
                apiBody = collectLinkData(this.state.addingActivityData)
                this.addActivityApiRequesyBody.apiEndPoint = Constants.LinkActivityAdd;
                this.addActivityApiRequesyBody.data = apiBody;
                break;
            case Constants.ActivityType.Video:
                apiBody = collectVideoData(this.state.addingActivityData)
                this.addActivityApiRequesyBody.apiEndPoint = Constants.VideoActivityAdd;
                this.addActivityApiRequesyBody.data = apiBody;
                break;
        }
        console.log("apiBody", apiBody);

        this.props.addActivityType(this.addActivityApiRequesyBody)
    }


    render() {
        return (
            <SafeAreaView style={{ flex: 1 }}>
                <Loader loading={this.props.addActivity.uploadFileProcessing || this.props.addActivity.addActivityProcessing} />
                <ScrollView contentContainerStyle={{ padding: 10 }}>
                    <KeyboardAwareScrollView>
                        {this._renderActivityPicture()}
                        {this._renderActivityUnit()}
                        {this._renderActivityPreCondition()}
                        {this._renderActivityAlwaysOnTop()}
                        {this._renderActivityOwners()}
                    </KeyboardAwareScrollView>
                    {this._renderActionButtons()}
                </ScrollView>
            </SafeAreaView>
        )
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        getTagsUnit: (...args) => dispatch(AddActivityActions.getTagsUnitRequest(...args)),
        getTagsCredits: (...args) => dispatch(AddActivityActions.getTagsCreditsRequest(...args)),
        addActivityType: (...args) => dispatch(AddActivityActions.addActivityTypeRequest(...args)),
        clearAddActivityData: () => dispatch(AddActivityActions.clearAddActivityData()),
    }
}

const mapStateToProps = (state) => {
    return {
        addActivity: state.addActivity,
        main: state.main,
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(NewActivitySettings)