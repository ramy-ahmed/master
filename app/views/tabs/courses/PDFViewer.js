import React from 'react';
import { View, Dimensions, StyleSheet } from 'react-native';
import Pdf from 'react-native-pdf';
import AsyncStorage from '@react-native-community/async-storage';
import LocalStorageConstants from '../../../local/LocalStorageConstants';
import { NavigationActions } from 'react-navigation';
import Ionicons from 'react-native-vector-icons/Ionicons';
import { connect } from 'react-redux';
import { strings } from '../../../locales/i18n';
class PDFViewer extends React.Component {

    static navigationOptions = ({ navigation }) => {
        const { params = {} } = navigation.state;

        return {
            headerLeft: (
                <View style={{ flex: 1, flexDirection: 'row', alignItems: 'center' }}>
                    <Ionicons style={{ paddingLeft: 20, paddingRight: 20, }} name="ios-arrow-back" color="black" size={25} onPress={() => {
                        const backActions = NavigationActions.back({
                            key: null
                        });
                        navigation.dispatch(backActions)
                    }} />
                </View>
            ),
            title: params.languageResource !== undefined ? params.languageResource.r_document_title || strings('r_document_title') : strings('r_document_title'),
        }
    }

    constructor(props) {
        super(props);

        this.state = {
            activity: this.props.navigation.getParam('activity', null),
            totalSpentSecond: 0,
            fileUri: null,
            pdfIsReady: false
        }
    }

    async componentWillMount() {
        const paths = await AsyncStorage.getItem(LocalStorageConstants.DownloadedFilePaths);
        if (paths !== null) {
            let downloadedData = JSON.parse(paths);
            for (var i = 0; i < downloadedData.length; i++) {
                if (this.state.activity.activityId === downloadedData[i].activityId) {
                    await this.setState({ fileUri: downloadedData[i].downloadedFilePaths, pdfIsReady: true })
                    break;
                }
            }
        }
    }
    componentDidMount() {
        this.props.navigation.setParams({
            languageResource: this.props.main.languageResource
        })

        this.interval = setInterval(() => this.tick(), 1000);
    }


    tick() {
        this.setState({
            totalSpentSecond: this.state.totalSpentSecond + 1
        });
    }

    componentWillUnmount() {
        clearInterval(this.interval)
        console.log("totalSpentSecond: ", this.state.totalSpentSecond);
    }

    _renderPdfViewer() {
        if (this.state.pdfIsReady) {
            const source = { uri: this.state.fileUri, cache: true };

            return (
                <Pdf source={source}
                    onLoadComplete={(numberOfPages, filePath) => {
                        console.log(`number of pages: ${numberOfPages}`);
                    }}
                    onPageChanged={(page, numberOfPages) => {
                        console.log(`current page: ${page}`);
                    }}
                    onError={(error) => {
                        console.log(error);
                    }}
                    style={styles.pdf} />
            )

        }
        else return null
    }
    render() {
        return (
            <View style={styles.container}>
                {this._renderPdfViewer()}
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'flex-start',
        alignItems: 'center',
    },
    pdf: {
        flex: 1,
        width: Dimensions.get('window').width,
    }
});

const mapDispatchToProps = (dispatch) => {
    return {}
}

const mapStateToProps = (state) => {
    return {
        main: state.main,
    }
}
export default connect(mapStateToProps, mapDispatchToProps)(PDFViewer)