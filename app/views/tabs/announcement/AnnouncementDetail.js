import React from 'react';
import { View, Text, ScrollView, Platform, TouchableOpacity, Alert, PermissionsAndroid } from 'react-native';
import Ionicons from 'react-native-vector-icons/Ionicons';
import { NavigationActions } from 'react-navigation';
import TextView from '../../../components/TextView';
import { ErrorAlert } from '../../../components/ErrorAlert';
import HTML from 'react-native-render-html';
import HTMLView from 'react-native-htmlview';

import NetInfo from '@react-native-community/netinfo';
import { Image } from 'react-native-elements';
import Moment from 'moment';
import FileViewer from 'react-native-file-viewer';
import RNFetchBlob from 'rn-fetch-blob'

//redux
import { connect } from 'react-redux';
import AnnouncementActions from '../../../redux/AnnouncementRedux';

import Loader from '../../../components/Loader';
import Colors from '../../../theme/Colors';
import { bytesToSize } from '../../../helpers/Calculate';
import DownloadAlert from '../../../components/DownloadAlert';
import PercentageCircle from 'react-native-percentage-circle';
import { strings } from '../../../locales/i18n';


const { config, fs, android } = RNFetchBlob
//let DownloadDir = fs.dirs.DownloadDir
let dirs = RNFetchBlob.fs.dirs


class AnnouncementDetail extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            ready: false,
            announcement: this.props.navigation.getParam('Announcement'),
            fromAvatarPath: { uri: this.props.announcements.announcementDetail.fromAvatarPath },
            downloadPercentage: 0,
            downloaderVisibility: false,
            canceled: false,
            onBackground: false,
        }

        this.announcementDetailApiRequestBody = {
            remote: true,
            almsPlusApiUrl: this.props.main.selectedOrganization.almsPlusApiUrl,
            announcementId: this.props.navigation.getParam('Announcement').announcementId,
            accessToken: this.props.main.authData.access_token
        }
    }

    static navigationOptions = ({ navigation }) => {
        const { params = {} } = navigation.state;
        return {
            headerLeft: (
                <View style={{ flex: 1, flexDirection: 'row', justifyContent: 'center', alignItems: 'center' }}>
                    <Ionicons style={{ paddingLeft: 20, paddingRight: 20 }} name="ios-arrow-back" size={25} onPress={() => {
                        const backActions = NavigationActions.back({
                            key: null
                        });
                        navigation.dispatch(backActions)
                    }} />
                </View>
            ),
            // headerRight: (
            //     <View style={{ flex: 1, flexDirection: 'row', justifyContent: 'center', alignItems: 'center' }}>
            //         <Ionicons style={{ paddingLeft: 20, paddingRight: 20 }} name="ios-more" size={25} onPress={() => {
            //             navigation.navigate('announcementTabs')
            //         }} />
            //     </View>
            // ),
            headerTitle: params.announcementSubject,
            headerTitleStyle: {
                textAlign: 'center',
                alignSelf: 'center',
                fontFamily: Platform.OS === 'ios' ? 'SFProText-Bold' : 'Roboto-Bold',
                fontSize: 15
            }
        };
    }

    componentDidMount() {
        this.props.navigation.setParams({
            announcementSubject: this.state.announcement.subject
        });

        NetInfo.fetch().then(statu => {
            if (statu.isConnected) this.announcementDetailApiRequestBody.remote = true; else this.announcementDetailApiRequestBody.remote = false;
            this.props.getAnnouncementDetail(this.announcementDetailApiRequestBody);

        })
    }
    componentWillReceiveProps(props) {
        if (!props.announcements.announcementDetailFetching && !props.announcements.announcementDetailError) {
            this.setState({ ready: true })
        }

        if (props.announcements.announcementDetailError) {
            const backActions = NavigationActions.back({
                key: null
            });
            this.props.navigation.dispatch(backActions)
        }

    }
    componentWillUnmount() {
        this.props.setAnnouncementDetail()
    }
    //FIXME: default avatar ne olmalı?
    onAvatarPathError() {
        console.log("onAvatarPathError")
        this.setState({
            fromAvatarPath: { uri: 'https://icon-library.net/images/avatar-icon-images/avatar-icon-images-4.jpg' }
        })
    }
    _renderSender() {
        return (
            <View style={{ flexDirection: 'column' }}>
                <View style={{ flexDirection: 'column', padding: 10 }}>
                    <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                        <Image source={this.state.fromAvatarPath}
                            style={{ width: 40, height: 40, borderRadius: 40 / 2 }}
                            onError={() => this.onAvatarPathError()} />
                        <TextView weight="bold" style={{ color: 'black', fontSize: 16, marginStart: 5 }}>{this.props.announcements.announcementDetail.fromDisplayName}</TextView>
                    </View>

                    {this._renderTargets()}
                </View>

                {/* <View style={{ flexDirection: 'row' }}>
                    <TextView style={{ color: 'black', backgroundColor: '#6f9999', padding: 5, borderRadius: 10, margin: 5, fontSize: 10 }}>E-POSTA</TextView>
                    <TextView style={{ color: 'black', backgroundColor: '#f3d8d6', padding: 5, borderRadius: 10, margin: 5, fontSize: 10 }}>WEB</TextView>
                    <TextView style={{ color: 'black', backgroundColor: '#fbf1df', padding: 5, borderRadius: 10, margin: 5, fontSize: 10 }}>SMS</TextView>
                </View> */}
                <View style={{ marginTop: 10, height: 1, backgroundColor: Colors.lineColor }} />
            </View>
        )
    }
    _renderTargets() {
        return (
            <View style={{ flexDirection: 'row', flexWrap: 'wrap', marginTop: 10, }}>
                {
                    this.props.announcements.announcementDetail.targets.map(target => (
                        <TextView key={target.announcementTargetId} style={{ color: 'black', backgroundColor: Colors.background, padding: 5, borderRadius: 15, margin: 5 }}>{target.contextHeader}</TextView>
                    ))
                }
            </View>
        )
    }

    _renderContent() {
        return (
            <View style={{ marginTop: 10 }}>
                <View style={{ flexDirection: 'column', padding: 10 }}>
                    <TextView weight="regular" style={{ color: 'black', fontSize: 13 }}>
                        {Moment(this.props.announcements.announcementDetail.startDate).format('DD MMMM YYYY / HH:MM')}
                    </TextView>
                    <View style={{ marginTop: 20 }}>
                        {/* <HTML html={this.props.announcements.announcementDetail.body} tagsStyles={{ strong: { fontFamily: Platform.OS === 'ios' ? 'SFProText-Bold' : 'Roboto-Bold' } }} /> */}
                        <HTMLView value={this.props.announcements.announcementDetail.body} addLineBreaks={false} />
                    </View>
                </View>
                <View style={{ marginTop: 10, height: 1, backgroundColor: Colors.lineColor }} />
            </View>
        );
    }

    _attachClick(file) {
        if (Platform.OS === 'android') {
            this.requestStoragePermission(file)
        } else {
            this._downloadAttachmentFile(file)
        }
    }

    requestStoragePermission(file) {
        try {
            PermissionsAndroid.requestMultiple([PermissionsAndroid.PERMISSIONS.READ_EXTERNAL_STORAGE, PermissionsAndroid.PERMISSIONS.WRITE_EXTERNAL_STORAGE]).then((result) => {
                if (result['android.permission.READ_EXTERNAL_STORAGE'] && result['android.permission.WRITE_EXTERNAL_STORAGE'] === 'granted') {
                    this._downloadAttachmentFile(file);
                } else {
                    console.log('Permissions denied');
                }
            })
        } catch (error) {
        }
    }
    _downloadAttachmentFile(file) {
        const downloadAttachmentBody = {
            announcementId: this.state.announcement.announcementId,
            path: file.path,
            fileName: file.fileName,
        }


        RNFetchBlob.fs.exists(dirs.DocumentDir + '/' + downloadAttachmentBody.announcementId + downloadAttachmentBody.fileName).then((exist) => {
            console.log("document downlaoded ? :", exist);
            if (exist) {
                FileViewer.open(dirs.DocumentDir + '/' + downloadAttachmentBody.announcementId + downloadAttachmentBody.fileName).then(() => {
                    console.log("file opened successfully");
                }).catch(error => {
                    Alert.alert('', this.props.main.languageResource.r_activity_attach_file_open_error || strings('r_activity_attach_file_open_error'))
                })
            } else {
                //this.props.downloadAnnouncementAttachment(downloadAttachmentBody)
                this._callDownloadApi(downloadAttachmentBody)
            }
        })
    }

    _callDownloadApi(body) {
        this.setState({
            downloaderVisibility: true,
            canceled: false
        })
        let dirs = RNFetchBlob.fs.dirs

        let options = {
            fileCache: true,
            //path: DownloadDir + "/alms/announcements/" + body.announcementId + body.fileName,
            path: dirs.DocumentDir + '/' + body.announcementId + body.fileName
        }

        this.task = RNFetchBlob.config(options).fetch('GET', body.path);
        this.task.progress((received, total) => {
            console.log("announcement down. progress:", ((received / total) * 100).toFixed(1))
            this.setState({
                downloadPercentage: ((received / total) * 100).toFixed(1)
            })
        }).then((res) => {
            if (!this.state.canceled) {
                this.setState({ downloaderVisibility: false, onBackground: false })
                console.log('file path:', res.path());
                FileViewer.open(res.path()).then(() => {
                    console.log("file opened successfully");
                }).catch(error => {
                    Alert.alert('', this.props.main.languageResource.r_activity_attach_file_open_error || strings('r_activity_attach_file_open_error'))
                })
            }
        }).catch((err) => {
            console.log("Error", err);
            this.setState({
                downloadPercentage: 0,
                downloaderVisibility: false,
                onBackground: false,
            })
        })


    }
    _renderAttachments() {
        if (this.props.announcements.announcementDetail.files.length !== 0) {
            return (
                this.props.announcements.announcementDetail.files.map((file, index) => {
                    return (
                        <View style={{ marginTop: 10 }} key={index}>
                            <TouchableOpacity onPress={() => this._attachClick(file)} activeOpacity={0.7} style={{ borderRadius: 5, borderWidth: 0.3, margin: 10, flex: 1, flexDirection: 'row', padding: 10, alignItems: 'center' }} >
                                <Ionicons style={{ flex: 0.05 }} name="ios-attach" size={20} />
                                <TextView style={{ flex: 0.75, marginStart: 5, color: 'black' }} weight="regular" numberOfLines={1} ellipsizeMode={'middle'}>{file.fileName}</TextView>
                                <View style={{ flex: 0.2, alignItems: 'flex-end' }}>
                                    {
                                        this.state.onBackground ?
                                            <View style={{ flex: 0.5, justifyContent: 'center', marginStart: 5 }}>
                                                <PercentageCircle
                                                    radius={17}
                                                    borderWidth={3}
                                                    percent={this.state.downloadPercentage}
                                                    color={Colors.primary}>
                                                    <TextView weight="medium" style={{ fontSize: 11 }}>
                                                        {this.state.downloadPercentage}
                                                    </TextView>
                                                </PercentageCircle>
                                            </View> :
                                            <TextView style={{ fontSize: 12, color: 'black' }} weight="bold">
                                                {bytesToSize(file.size)}
                                            </TextView>}


                                </View>
                            </TouchableOpacity>
                        </View>
                    )
                })
            );
        } else return null;
    }

    _renderContainer() {
        if (this.state.ready) {
            return (
                <View>
                    {this._renderSender()}
                    {this._renderContent()}
                    {this._renderAttachments()}
                </View>
            )
        }
    }
    downloadOnBackground() {
        this.setState({ onBackground: true, downloaderVisibility: false })
    }
    downloadOnCancel() {
        let dirs = RNFetchBlob.fs.dirs

        RNFetchBlob.fs.unlink(dirs.DocumentDir + '/' + this.state.announcement.announcementId + this.props.announcements.announcementDetail.files[0].fileName);
        this.setState({ downloaderVisibility: false, canceled: true, downloadPercentage: 0 })
        this.task.cancel();
    }

    render() {
        return (
            <ScrollView style={{ flex: 1 }}>
                <Loader loading={this.props.announcements.announcementDetailFetching} />
                <DownloadAlert downloading={this.state.downloaderVisibility} percent={this.state.downloadPercentage} onBackground={() => this.downloadOnBackground()} onCancel={() => this.downloadOnCancel()} />
                {this._renderContainer()}
            </ScrollView>
        );
    }
}


const mapDispatchToProps = (dispatch) => {
    return {
        getAnnouncementDetail: (...args) => dispatch(AnnouncementActions.getAnnouncementDetailRequest(...args)),
        setAnnouncementDetail: () => dispatch(AnnouncementActions.setAnnouncementDetail()),
        downloadAnnouncementAttachment: (...args) => dispatch(AnnouncementActions.downloadAnnouncementAttachmentRequest(...args)),
    }
}

const mapStateToProps = (state) => {
    return {
        announcements: state.announcement,
        main: state.main,
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(AnnouncementDetail)
