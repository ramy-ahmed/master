import React from 'react';
import { View, Text, FlatList, TouchableOpacity, Alert, ScrollView, RefreshControl } from 'react-native';
import NetInfo from '@react-native-community/netinfo';


//redux
import { connect } from 'react-redux';
import AnnouncementActions from '../../../redux/AnnouncementRedux';

import Constants from '../../../services/Constants';
import AsyncStorage from '@react-native-community/async-storage';

import AnnouncementListItem from '../../../components/announcement/AnnouncementListItem'
import AnnouncementEmpty from '../../../components/announcement/AnnouncementEmpty';
import { strings } from '../../../locales/i18n';
import Loader from '../../../components/Loader';
class InboxAnnouncement extends React.Component {

    constructor(props) {
        super(props)

        this.state = {
            refreshing: false,
            scrolled: false,
        }

        this.announcementApiRequestBody = {
            remote: true,
            accessToken: this.props.main.authData.access_token,
            almsPlusApiUrl: this.props.main.selectedOrganization.almsPlusApiUrl,
            state: Constants.AnnouncementTypes.Inbox,
            take: Constants.ApiResponseQuantity,
            skip: 0
        }
    }
    componentDidMount() {
        this._getInboxAnnouncements()
    }

    async _getInboxAnnouncements() {
        await this.setState({ scrolled: false })

        NetInfo.fetch().then(statu => {
            if (statu.isConnected) this.announcementApiRequestBody.remote = true; else this.announcementApiRequestBody.remote = false;
            this.props.getInboxAnnouncements(this.announcementApiRequestBody)

        })
    }
    componentWillReceiveProps(props) {
        if (props.announcements.announcementsInboxError) {
            Alert.alert(strings('error.error_title'), props.announcements.announcementsInboxErrorMessage)
        }

        if (!props.announcements.announcementsInboxFetching) {
            this.setState({ refreshing: false })
        }
    }
    keyExtractor = (item, index) => index.toString()

    _onItemClick(item) {
        this.props.navigation.navigate('AnnouncementDetail', { Announcement: item })
    }


    _onRefresh() {
        this.props.setInboxAnnouncement()
        this.announcementApiRequestBody.skip = 0
        this.setState({ refreshing: true })
        this._getInboxAnnouncements();
    }

    _renderMoreItem = () => {
        if (this.state.scrolled && (this.props.announcements.announcementsInboxData.length % Constants.ApiResponseQuantity === 0)) {
            this.announcementApiRequestBody.skip = this.announcementApiRequestBody.skip + Constants.ApiResponseQuantity;
            this._getInboxAnnouncements()
        }
    }


    _renderData() {
        if (this.props.announcements.announcementsInboxData.length == 0 && !this.props.announcements.announcementsInboxFetching) {
            return (<AnnouncementEmpty />);
        } else if (!this.props.announcements.announcementsInboxError && this.props.announcements.announcementsInboxData.length != 0) {
            return (
                <FlatList
                    keyExtractor={this.keyExtractor}
                    data={this.props.announcements.announcementsInboxData}
                    refreshControl={<RefreshControl refreshing={this.state.refreshing} onRefresh={() => this._onRefresh()} />}
                    onEndReached={this._renderMoreItem}
                    onEndReachedThreshold={0.5}
                    initialNumToRender={Constants.ApiResponseQuantity}
                    onTouchStart={() => {
                        this.setState({
                            scrolled: true
                        })
                    }}
                    renderItem={({ item }) => (
                        <TouchableOpacity activeOpacity={0.7} style={{ flexDirection: 'column' }} onPress={() => this._onItemClick(item)}>
                            <AnnouncementListItem announcement={item} />
                        </TouchableOpacity>
                    )} />

            )
        } else {
            return null;
        }
    }

    render() {
        return (
            <View style={{ flex: 1 }}>
                <Loader loading={this.props.announcements.announcementsInboxFetching && !this.state.refreshing} />
                <View style={{ flex: 1 }}>
                    {this._renderData()}
                </View>

            </View>
        );
    }
}



const mapDispatchToProps = (dispatch) => {
    return {
        getInboxAnnouncements: (...args) => dispatch(AnnouncementActions.getAnnouncementInboxRequest(...args)),
        setInboxAnnouncement: (...args) => dispatch(AnnouncementActions.setInboxAnnouncement(...args)),
    }
}

const mapStateToProps = (state) => {
    return {
        announcements: state.announcement,
        main: state.main,
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(InboxAnnouncement);